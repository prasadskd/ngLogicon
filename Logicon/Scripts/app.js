﻿
var app = angular.module('Logicon', ['ngAnimate', 'ngRoute', 'ui.bootstrap', 'angular-growl', 'ui.grid',
                         'ngMaterial', 'treeControl', 'mwl.calendar', 'ngTagsInput', 'moment-picker', 'oc.lazyLoad', 'ngSanitize', 'angular.morris', 'ui.bootstrap.datetimepicker', 'ui.dateTimeInput']);//<!-- 'ngSlimScroll', , 'logicon.fileupload'-->

app.run(function ($rootScope, $location) {    

    $rootScope.$on('$routeChangeSuccess', function (e, current, pre) {
        var currentRoute = current.$$route.originalPath;
        var IS_AUTHENTICATE = sessionStorage.getItem('IS_AUTHENTICATE') == 'true' ? true : false;
        var companyType = sessionStorage.getItem('COMPANYTYPE');
        if (IS_AUTHENTICATE) {
            if (companyType == 'OWNER' && currentRoute == '/')
                $location.path('/owner');
            else
                return;
        }
        else {            
            if (currentRoute != '/registration' && currentRoute != '/registration/:userid' && currentRoute != '/registeredcompany' && currentRoute != '/emailverification/:uniqueid' && currentRoute != '/forgotpassword') {
                //$location.path('/login');
                location.href = 'default.html';
            }
            else
                return;
        }
    });
});

app.factory('HttpRequestInterceptor', ['$rootScope', function ($rootScope) {
    return {
        request: function ($config) {
            var IS_AUTHENTICATE = sessionStorage.getItem('IS_AUTHENTICATE') == 'true' ? true : false;
            var AUTH_TOKEN = '';
            var USER_ID = '';
            var COMPANY_ID = '';

            if (IS_AUTHENTICATE) {
                COMPANY_ID = sessionStorage.getItem('COMPANYID');
                USER_ID = sessionStorage.getItem('USERID');
                BRANCH_ID = sessionStorage.getItem('BRANCHID');

                $config.headers['COMPANY_ID'] = COMPANY_ID;
                $config.headers['USERID'] = USER_ID;
                $config.headers['BRANCH_ID'] = BRANCH_ID;
            }

            /*
            var IS_AUTHENTICATE = sessionStorage.getItem('IS_AUTHENTICATE') == 'true' ? true : false;
            if (IS_AUTHENTICATE) {
                $config.headers['AUTH_TOKEN'] = AUTH_TOKEN;
                $config.headers['USER_ID'] = USER_ID;
            }*/

            return $config;
        }
    }
}]);

app.filter('LogiconcurrencyFilter', ['$filter', function ($filter) {
    return function (input) {
        input = parseFloat(input);
        if (input % 1 === 0) {
            input = input.toFixed(2);
        }

        return (input).replace(/(\d)(?=(\d{3})+($|\.))/g, "$1cfs");
    };
}]);

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) && angular.isUndefined(key))
            return 0;
        var sum = 0;
        angular.forEach(data, function (value) {            
            if (!angular.isUndefined(value[key]) && value[key] != null)
                sum = sum + parseFloat(value[key]);
        });
        return sum.toFixed(4);
    }
});

app.directive('compareTo', function () {

    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function (scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function (modelValue) {                
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function () {
                ngModel.$validate();
            });
        }
    };
});

app.directive('decimalNumber', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }

                var clean = val.replace(/[^-0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');
                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = negativeCheck[0] + '-' + negativeCheck[1];
                    if (negativeCheck[0].length > 0) {
                        clean = negativeCheck[0];
                    }

                }

                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 4);
                    clean = decimalCheck[0] + '.' + decimalCheck[1];
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});

app.directive('logiconControlCurrency', function ($filter, $locale) {

    var decimalSep = $locale.NUMBER_FORMATS.DECIMAL_SEP;
    var toNumberRegex = new RegExp('[^0-9\\' + decimalSep + ']', 'g');
    var trailingZerosRegex = new RegExp('\\' + decimalSep + '0+$');
    var filterFunc = function (input, attrs) {
        if (angular.isUndefined(input)) {
            return false;
        }

        //return $filter('currency')(value);
        input = parseFloat(input);
        debugger;
        if (input % 1 === 0) {
            input = input.toFixed(2);
        }

        return input;
        //return (input).replace(/(\d)(?=(\d{3})+($|\.))/g, "$1,");
    };

    function getCaretPosition(input) {
        if (!input) return 0;
        if (input.selectionStart !== undefined) {
            return input.selectionStart;
        } else if (document.selection) {
            // Curse you IE
            input.focus();
            var selection = document.selection.createRange();
            selection.moveStart('character', input.value ? -input.value.length : 0);
            return selection.text.length;
        }
        return 0;
    }

    function setCaretPosition(input, pos) {

        if (!input) return 0;
        if (input.offsetWidth === 0 || input.offsetHeight === 0) {
            return; // Input's hidden
        }
        if (input.setSelectionRange) {
            input.focus();
            input.setSelectionRange(pos, pos);
        }
        else if (input.createTextRange) {
            // Curse you IE
            var range = input.createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    }

    function toNumber(currencyStr) {
        return parseFloat(currencyStr.replace(toNumberRegex, ''), 10);
    }

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function postLink(scope, elem, attrs, modelCtrl) {
            modelCtrl.$formatters.push(function (value) {
                if (angular.isUndefined(value)) {
                    input = parseFloat('0.00');
                    return input.toFixed(attrs.logiconControlCurrency)
                } else {
                    input = parseFloat(value);
                    return input.toFixed(attrs.logiconControlCurrency);
                }
            });
            //modelCtrl.$formatters.push(filterFunc);
            //modelCtrl.$parsers.push(function (newViewValue) {                
            //    var oldModelValue = modelCtrl.$modelValue;
            //    var newModelValue = toNumber(newViewValue);
            //    modelCtrl.$viewValue = filterFunc(newModelValue);
            //    var pos = getCaretPosition(elem[0]);
            //    elem.val(modelCtrl.$viewValue);
            //    var newPos = pos + modelCtrl.$viewValue.length -
            //                       newViewValue.length;
            //    if ((oldModelValue === undefined) || isNaN(oldModelValue)) {
            //        newPos -= 3;
            //    }
            //    setCaretPosition(elem[0], newPos);
            //    return newModelValue;
            //});
        }
    };
});

app.directive('logiconDecimal', function () {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) return '';
                var transformedInput = inputValue.replace(/[^0-9\.]/g, '');
                if (transformedInput !== inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});

app.directive('logiconNumber', function () {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) return '';
                if (typeof inputValue == 'number') {
                    inputValue = inputValue.toString();
                }

                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                if (transformedInput !== inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});

app.factory('UtilityFunc', ['$filter', function ($filter) {
    var obj = {};
    obj.removeArrayElementByKey = function (array, key, value) {
        if (array.length == 0)
            return array;
        else {

            var filterObj = {};
            filterObj[key] = value;

            var removableList = $filter('filter')(array, filterObj);

            angular.forEach(removableList, function (obj, key) {
                var index = array.indexOf(obj);
                array.splice(index, 1);
            });

            return array;
        }
    };

    obj.UserID = function () {
        return sessionStorage.getItem('USERID');
    };

    obj.UserName = function () {
        return sessionStorage.getItem('USERNAME');
    };

    obj.UserDesignation = function () {
        return sessionStorage.getItem('USERDESIGNATION');
    };

    obj.CompanyID = function () {
        return parseInt(sessionStorage.getItem('COMPANYID'));
    };

    obj.BranchID = function () {
        return parseInt(sessionStorage.getItem('BRANCHID'));
    };

    obj.CompanyType = function () {
        return sessionStorage.getItem('COMPANYTYPE');
    };

    obj.Logo = function () {
        return sessionStorage.getItem('LOGO');
    };

    obj.IsAuthenticate = function () {
        return sessionStorage.getItem('IS_AUTHENTICATE') == 'true' ? true : false;
    };

    obj.CompanySubscriptionList = function () {
        return JSON.parse(sessionStorage.getItem('COMPANYSUBSCRIPTIONLIST'))
    };

    obj.DateFormat = function () {
        return 'DD/MM/YYYY';
    };

    obj.TimeFormat = function () {
        return 'hh:mm';
    };

    obj.DateTimeFormat = function () {
        return 'DD/MM/YYYY hh:mm';
    };

    obj.DateTimeFormat12 = function () {
        return 'DD/MM/YYYY hh:mm a';
    };

    obj.DefaultCurrency = function () {
        return 'MYR';
    };

    obj.DefaultCountry = function () {
        return 'MY';
    };

    obj.FirstDateOfMonth = function () {        
        var date = new Date(), y = date.getFullYear(), m = date.getMonth();
        var firstDay = new Date(y, m, 1);        
        return moment(firstDay);
    };

    obj.StandardQuotationkey = function () {
        return 'STANDARD_1000001';
    };

    obj.DataGridNorecords = function () {
        return 'No records found.';
    };

    return obj;
}]);

app.factory('mySharedService', function ($rootScope) {
    var sharedService = {};
    sharedService.path = '';

    sharedService.prepForBroadcast = function (path) {
        this.path = path;
        this.broadcastItem(path);
    };

    sharedService.broadcastItem = function (path) {
        $rootScope.$broadcast('handleBroadcast');
    };

    sharedService.LoginBroadcast = function () {
        this.broadcastLogin();
    };

    sharedService.broadcastLogin = function () {
        $rootScope.$broadcast('USERLOGGEDIN');
    };

    sharedService.LogOutBroadcast = function () {
        this.broadcastLogOut();
    };

    sharedService.broadcastLogOut = function () {
        $rootScope.$broadcast('USERLOGGEDOUT');
    };

    sharedService.RegUserLoggedIn = function () {
        this.broadcastRegLoggedIn();
    };

    sharedService.broadcastRegLoggedIn = function () {
        $rootScope.$broadcast('REGUSERLOGGEDIN');
    };

    sharedService.LookUpSearchItem = function (type) {
        $rootScope.$broadcast('LOOKUPSEARCH', type);
    };

    return sharedService;
});

app.directive('hasPermission', function ($filter) {
    return {
        link: function (scope, element, attrs) {
            var userRights = JSON.parse(sessionStorage.getItem('SsnUserRights'));
            var linkID = attrs.linkid;
            var securableItem = attrs.securable;

            var obj = $filter('filter')(userRights, { LinkID: linkID });
            var flag = false;
            angular.forEach(obj, function (item, index) {
                if (item.SecurableItemDescription.toLowerCase() == securableItem.toLowerCase())
                    flag = true;
            });

            if (flag)
                element.show();
            else
                element.hide();
        }
    };
});

app.directive('hasModule', function ($filter, UtilityFunc) {
    return {
        link: function (scope, element, attrs) {
            var companySubscriptionList = UtilityFunc.CompanySubscriptionList();            
            var moduleArray = attrs.module.split(',');
            var flag = false;
            for (var i = 0; i < moduleArray.length; i++) {
                var obj = $filter('filter')(companySubscriptionList, { ModuleDescription: moduleArray[i].trim().toLowerCase() }, true)[0];
                if (!angular.isUndefined(obj)) {                    
                    if (obj.ModuleDescription.toLowerCase() == moduleArray[i].trim().toLowerCase()) {
                        flag = true;
                        break;
                    }
                }
            }

            if (flag)
                element.show();
            else
                element.hide();

        }
    };
});

app.filter('trustAsResourceUrl', ['$sce', function ($sce) {
    return function (val) {
        return $sce.trustAsResourceUrl(val);
    };
}]);

app.directive('httpSrc', ['$http', function ($http) {
    var directive = {
        link: link,
        restrict: 'A'
    };
    return directive;

    function link(scope, element, attrs) {
        var requestConfig = {
            method: 'Get',
            url: attrs.httpSrc,
            responseType: 'arraybuffer',
            cache: 'true'
        };

        $http(requestConfig)
            .success(function (data) {
                var arr = new Uint8Array(data);
                var raw = '';
                var i, j, subArray, chunk = 5000;
                for (i = 0, j = arr.length; i < j; i += chunk) {
                    subArray = arr.subarray(i, i + chunk);
                    raw += String.fromCharCode.apply(null, subArray);
                }
                var b64 = btoa(raw);
                debugger;
                //attrs.$set('src', "data:image/jpeg;base64," + b64);
                download('data:image/jpeg;base64,' + b64, 'edi.txt', attrs.mime)
            });
    }

}]);

app.service('DownloadService', ['$http', '$q', 'Utility', function ($http, $q, Utility) {
    this.Download = function (fileName, type, mimetype) {

        var requestConfig = {
            method: 'Get',
            url: Utility.ServiceUrl + '/download/file/' + type + '/' + fileName + '/',
            responseType: 'arraybuffer',
            cache: 'false'
        };

        $http(requestConfig)
            .success(function (data) {
                var arr = new Uint8Array(data);
                var raw = '';
                var i, j, subArray, chunk = 5000;
                for (i = 0, j = arr.length; i < j; i += chunk) {
                    subArray = arr.subarray(i, i + chunk);
                    raw += String.fromCharCode.apply(null, subArray);
                }
                var b64 = btoa(raw);
                download('data:image/jpeg;base64,' + b64, fileName, mimetype)
            });
    };

    this.DownloadRegistrationFiles = function (fileName, type, mimetype, companyID) {

        var requestConfig = {
            method: 'Get',
            url: Utility.ServiceUrl + '/download/file/' + companyID + '/' + type + '/' + fileName + '/',
            responseType: 'arraybuffer',
            cache: 'false'
        };

        $http(requestConfig)
            .success(function (data) {
                var arr = new Uint8Array(data);
                var raw = '';
                var i, j, subArray, chunk = 5000;
                for (i = 0, j = arr.length; i < j; i += chunk) {
                    subArray = arr.subarray(i, i + chunk);
                    raw += String.fromCharCode.apply(null, subArray);
                }
                var b64 = btoa(raw);
                debugger;
                download('data:application/octet-stream;base64,' + b64, fileName, mimetype)
                //download('data:image/jpeg;base64,' + b64, fileName, mimetype)
                //window.open("data:application/pdf;base64," + Base64.encode(out));
            });
    };
}]);

app.directive('logiconLimit', [function () {
    return {
        restrict: 'A',
        link: function (scope, elem, attrs) {
            var limit = parseInt(attrs.ngMaxlength);
            angular.element(elem).on('keypress', function (e) {
                if (this.value.length == limit) e.preventDefault();
            });
        }
    }
}]);

app.constant('Messages', {
    DataGridNorecords:'No records found'
});

app.directive('hcChart', function () {
    return {
        restrict: 'E',
        template: '<div></div>',
        scope: {
            options: '='
        },
        link: function (scope, element) {
            Highcharts.chart(element[0], scope.options);
        }
        //,
        //controller: ['$scope', '$q', 'VisualReportsService', '$routeParams',
        //    function ($scope, $q, VisualReportsService, $routeParams) {
        //        chart = this;
        //        VisualReportsService.GetHSCodePortOfLoading().then(function (d) {
        //            debugger;
        //            $scope.chartOptions = d.data;
        //        }, function (err) { });

        //    }]
    };
})

app.directive('hcPieChart', function () {
    return {
        restrict: 'E',
        template: '<div></div>',
        scope: {
            title: '@',
            data: '='
        },
        link: function (scope, element) {
            Highcharts.chart(element[0], {
                chart: {
                    type: 'pie'
                },
                title: {
                    text: scope.title
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                },
                series: [{
                    data: scope.data
                }]
            });
        }
        
    };
});
var chart;
app.service('DataTransferService', ['$http', function ($http) {
    this.Data = [];

    this.SetData = function (data) {
        this.Data = data;
    };

    this.GetData = function () {
        return this.Data;
    }
}]);

//app.service('VisualReportsService', ['$http', '$q', 'Utility', function ($http, $q, Utility) {
//    this.GetHSCodePortOfLoading = function () {
//        var deferred = $q.defer();
//        $http.get(Utility.ServiceUrl + '/Reports/VisualReports/hscodeportofloading').then(function (res) {
//            deferred.resolve(res);
//        }, function (err) {
//            deferred.reject(err);
//        });
//        return deferred.promise;
//    };
//}]);






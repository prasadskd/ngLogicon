﻿app.config(function ($routeProvider, $httpProvider, growlProvider, $mdThemingProvider, $uibTooltipProvider, Utility) {//momentPickerProvider
    $routeProvider
        /* masterdata module starts */
        .when('/', {
            templateUrl: 'Js/Home/Views/dashboard.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Home/controller/HomeController.js?v=' + Utility.Version,
                                    'js/Operation/Services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/Operation/Services/VesselScheduleService.js?v=' + Utility.Version,
                                    'Js/MasterData/Services/PortAreaService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/owner', {
            templateUrl: 'Js/Home/Views/owner-dashboard.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Home/controller/OwnerDashboardController.js?v=' + Utility.Version,
                                    'js/Home/Services/OwnerService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/registration', {
            templateUrl: 'Js/Home/Views/Registration.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Home/controller/RegistrationController.js?v=' + Utility.Version,
                                    'js/Home/Services/RegistrationService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CountryService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/registration/:userID', {
            templateUrl: 'Js/Home/Views/Registration.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Home/controller/RegistrationController.js?v=' + Utility.Version,
                                    'js/Home/Services/RegistrationService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CountryService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/forgotpassword', {
            templateUrl: 'Js/Home/Views/forgotpassword.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Home/controller/RegistrationController.js?v=' + Utility.Version,
                                    'js/Home/Services/RegistrationService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CountryService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/emailverification/:uniqueid', {
            templateUrl: 'Js/Home/Views/Emailverification.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Home/controller/EmailVerificationController.js?v=' + Utility.Version,
                                    'js/Home/Services/VerificationService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/login', {
            templateUrl: 'Js/Home/Views/login.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Home/controller/LoginController.js?v=' + Utility.Version,
                                    'js/Home/Services/RegistrationService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/registeredcompany', {
            templateUrl: 'Js/Home/Views/registeredcompany.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: [
                                //'Css/FileUpload/style.css',
                                //'Css/FileUpload/jquery.fileupload.css',
                                //'Css/FileUpload/jquery.fileupload-ui.css',
                                //'Css/FileUpload/jquery.fileupload-noscript.css',
                                //'Css/FileUpload/jquery.fileupload-ui-noscript.css',
                                //'Scripts/FileUpload/jquery.ui.widget.js',
                                //'//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js',
                                //'//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js',
                                //'Scripts/FileUpload/jquery.iframe-transport.js',
                                //'Scripts/FileUpload/jquery.fileupload.js',
                                //'Scripts/FileUpload/jquery.fileupload-process.js',
                                //'Scripts/FileUpload/jquery.fileupload-image.js',
                                //'Scripts/FileUpload/jquery.fileupload-validate.js',
                                //'Scripts/FileUpload/jquery.fileupload-angular.js',
                                'js/Home/controller/RegisteredCompanyController.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/home', {
            templateUrl: 'Js/Home/Views/dashboard.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Home/controller/HomeController.js?v=' + Utility.Version,
                                    'js/Operation/Services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/Operation/Services/VesselScheduleService.js?v=' + Utility.Version,
                                    'Js/MasterData/Services/PortAreaService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })



        .when('/company/:companyID', {
            templateUrl: 'Js/Home/Views/companydetails.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Home/controller/CompanyDetailsController.js?v=' + Utility.Version,
                                    'js/Home/Services/CompanyDetailsService.js?v=' + Utility.Version,
                                    'js/Home/Services/RegisteredCompanyService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/map', {
            templateUrl: 'Js/Home/Views/map.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: []
                        }
                    );
                }]
            }
        })

        .when('/masterdata/company', {
            templateUrl: 'Js/MasterData/Views/Company/company.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/Company.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CompanyService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CountryService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/masterdata/imocode', {
            templateUrl: 'Js/MasterData/Views/IMOCode/Imocode.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/IMOCodeCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/EditIMOCodeCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/IMOCodeService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/masterdata/holdstatus', {
            templateUrl: 'Js/MasterData/Views/HoldStatus/holdstatus.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/HoldStatusCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/EditHoldStatusCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/HoldStatusService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/masterdata/processmaster', {
            templateUrl: 'Js/MasterData/Views/ProcessMaster/processmaster.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/ProcessMaster.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/AddProcessMasterCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/ProcessMasterService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CompanyService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/masterdata/merchant/:code/:taxID/:regNo', {
            templateUrl: 'Js/MasterData/Views/MerchantProfile/merchant.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/MerchantProfile.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/addEditAddressCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CountryService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/masterdata/merchantlist', {
            templateUrl: 'Js/MasterData/Views/MerchantProfile/merchantprofilelist.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/MerchantProfile.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/addEditAddressCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/masterdata/chargecodelist', {
            templateUrl: 'Js/MasterData/Views/ChargeCode/chargecodelist.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/ChargeCode.js?v=' + Utility.Version,
                                    'js/MasterData/Services/ChargeCodeService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/GstRateService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/masterdata/chargecodemaster/:chargecode', {
            templateUrl: 'Js/MasterData/Views/ChargeCode/chargecodemaster.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/ChargeCode.js?v=' + Utility.Version,
                                    'js/MasterData/Services/ChargeCodeService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/GstRateService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/masterdata/currencyrate', {
            templateUrl: 'Js/MasterData/Views/CurrencyRate/currencyrate.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/CurrencyRateCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/EditExpiryExchangeCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CurrencyService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CurrencyRateService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/masterdata/drivermasterlist', {
            templateUrl: 'Js/MasterData/Views/DriverMaster/drivermasterlist.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/DriverMasterCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/DriverMasterService.js?v=' + Utility.Version,
                                    'Js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version,
                            'js/MasterData/Services/CountryService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/masterdata/drivermaster/:driverId', {
            templateUrl: 'Js/MasterData/Views/DriverMaster/drivermaster.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/DriverMasterCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/DriverMasterService.js?v=' + Utility.Version,
                                    'Js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CountryService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/masterdata/truckmaster', {
            templateUrl: 'Js/MasterData/Views/TruckMaster/truckmaster.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/TruckMasterController.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/EditTruckController.js?v=' + Utility.Version,
                                    'js/MasterData/Services/TruckMasterService.js?v=' + Utility.Version,
                                    'js/Operation/Services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/DriverMasterService.js?v=' + Utility.Version,
                                    'Js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/masterdata/trailermaster', {
            templateUrl: 'Js/MasterData/Views/TrailerMaster/trailermaster.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/TrailerMasterController.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/EditTrailerController.js?v=' + Utility.Version,
                                    'Js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version,
                                    'Js/MasterData/Services/TrailerMasterService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/masterdata/portareacode', {
            templateUrl: 'Js/MasterData/Views/PortAreaCode/portareacode.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/PortAreaCodeCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/EditPortAreaCntrl.js?v=' + Utility.Version,
                                    'Js/MasterData/Services/CountryService.js?v=' + Utility.Version,
                                    'Js/MasterData/Services/PortAreaService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/masterdata/uom', {
            templateUrl: 'Js/MasterData/Views/Uom/uom.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/UomCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/EditUomCntrl.js?v=' + Utility.Version,
                                    'Js/MasterData/Services/UomService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/masterdata/warehouse', { templateUrl: 'Js/MasterData/WareHouse/Views/warehouse.html?v=' + Utility.Version })
        .when('/masterdata/lookupconfig', { templateUrl: 'Js/MasterData/LookupConfig/Views/lookupconfig.html?v=' + Utility.Version })


        .when('/masterdata/productmaster', {
            templateUrl: 'Js/MasterData/Views/ProductMaster/productmaster.html?v=' + Utility.Version, resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/ProductMasterCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/ProductMasterService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version,
                            ]
                        }
                    );
                }]
            }
        })


        .when('/masterdata/jobcategory', {
            templateUrl: 'Js/MasterData/Views/JobCategory/jobcategory.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/JobCategory.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/addEditJobCategoryCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/EditChargesCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/JobCategoryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/ProcessMasterService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CompanyService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/masterdata/jobcategory/add/:code', {
            templateUrl: 'Js/MasterData/Views/JobCategory/addjobcategory.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/JobCategory.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/addEditJobCategoryCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/EditChargesCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/JobCategoryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/ProcessMasterService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/JobCategoryChargesService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CompanyService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/masterdata/zonemaster', { templateUrl: 'Js/MasterData/ZoneMaster/Views/zonemaster.html?v=' + Utility.Version })
        .when('/masterdata/productlocation', { templateUrl: 'Js/MasterData/ProductLocation/Views/productlocation.html?v=' + Utility.Version })
        .when('/masterdata/cfs', { templateUrl: 'Js/MasterData/CFS/Views/cfs.html?v=' + Utility.Version })
        .when('/masterdata/driverassignment', { templateUrl: 'Js/MasterData/DriverAssignment/Views/driverassignment.html?v=' + Utility.Version })
        .when('/masterdata/driverallocation', { templateUrl: 'Js/MasterData/DriverAllocation/Views/driverallocation.html?v=' + Utility.Version })
        .when('/masterdata/railterminal', { templateUrl: 'Js/MasterData/RailTerminal/Views/railterminal.html?v=' + Utility.Version })
        //.when('/masterdata/holiday', { templateUrl: 'Js/MasterData/Holiday/Views/holiday.html' })
        .when('/masterdata/gstrate', {
            templateUrl: 'Js/MasterData/Views/GSTRate/gstrate.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/GSTRateCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/AddEditGSTRateCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/GstRateService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

          .when('/masterdata/vesselmaster', {
              templateUrl: 'Js/MasterData/Views/VesselMaster/vesselmaster.html?v=' + Utility.Version,
              resolve: {
                  lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                      return $ocLazyLoad.load(
                          {
                              name: 'Logicon',
                              files: ['js/MasterData/Controllers/VesselMasterCntrl.js?v=' + Utility.Version,
                                      'js/MasterData/Controllers/AddEditVesselMasterCntrl.js?v=' + Utility.Version,
                                      'js/MasterData/Services/VesselMasterService.js?v=' + Utility.Version]
                          }
                      );
                  }]
              }
          })
          .when('/masterdata/holiday', {
              templateUrl: 'Js/MasterData/Views/Holiday/holidaylist.html?v=' + Utility.Version,
              resolve: {
                  lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                      return $ocLazyLoad.load(
                          {
                              name: 'Logicon',
                              files: ['js/MasterData/Controllers/HolidayCntrl.js?v=' + Utility.Version,
                                      'js/MasterData/Controllers/AddEditHolidayCntrl.js?v=' + Utility.Version,
                                      'js/MasterData/Services/HolidayService.js?v=' + Utility.Version]
                          }
                      );
                  }]
              }
          })
         .when('/masterdata/pudomaster', {
             templateUrl: 'Js/MasterData/Views/PUDOMaster/pudomaster.html?v=' + Utility.Version,
             resolve: {
                 lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(
                         {
                             name: 'Logicon',
                             files: ['js/MasterData/controllers/PUDOMasterCntrl.js?v=' + Utility.Version,
                                     'js/MasterData/controllers/AddEditPUDOMasterCntrl.js?v=' + Utility.Version,
                                     'js/MasterData/Services/PUDOMasterService.js?v=' + Utility.Version]
                         }
                     );
                 }]
             }
         })

         .when('/masterdata/commoditycode', {
             templateUrl: 'Js/MasterData/Views/CommodityCode/commoditycodelist.html?v=' + Utility.Version,
             resolve: {
                 lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(
                         {
                             name: 'Logicon',
                             files: ['js/MasterData/controllers/CommodityCodeCntrl.js?v=' + Utility.Version,
                                     'js/MasterData/controllers/AddEditCommodityCodeCntrl.js?v=' + Utility.Version,
                                     'js/MasterData/Services/CommodityCodeService.js?v=' + Utility.Version]
                         }
                     );
                 }]
             }
         })

         .when('/masterdata/equipmentsizetype', {
             templateUrl: 'Js/MasterData/Views/EquipmentSizeType/equipmentsizetype.html?v=' + Utility.Version,
             resolve: {
                 lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(
                         {
                             name: 'Logicon',
                             files: ['js/MasterData/controllers/EquipmentSizeTypeCntrl.js?v=' + Utility.Version,
                                     'js/MasterData/controllers/AddEditEquipmentSizeTypeCntrl.js?v=' + Utility.Version,
                                     'js/MasterData/Services/EquipmentSizeTypeService.js?v=' + Utility.Version]
                         }
                     );
                 }]
             }
         })
         .when('/masterdata/containerstatus', {
             templateUrl: 'Js/MasterData/Views/ContainerStatus/containerstatus.html?v=' + Utility.Version,
             resolve: {
                 lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(
                         {
                             name: 'Logicon',
                             files: ['js/MasterData/controllers/ContainerStatusCntrl.js?v=' + Utility.Version,
                                     'js/MasterData/controllers/AddEditContainerStatusCntrl.js?v=' + Utility.Version,
                                     'js/MasterData/Services/ContainerStatusService.js?v=' + Utility.Version]
                         }
                     );
                 }]
             }
         })
        .when('/masterdata/countries', { templateUrl: 'Js/MasterData/Views/Country/countries.html?v=' + Utility.Version })

        .when('/masterdata/countries', {
            templateUrl: 'Js/MasterData/Views/Country/countries.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/CountryCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/EditCountryCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CountryService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/masterdata/currencylist', {
            templateUrl: 'Js/MasterData/Views/Currency/currencies.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/CurrencyCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/EditCurrencyCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CurrencyService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        /* masterdata module ends */


        /* adminstration module starts */
        .when('/admin/companyprofile', {
            templateUrl: 'Js/admin/CompanyProfile/Views/companyprofile.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['Js/admin/Controllers/UserProfileController.js?v=' + Utility.Version,
                                    'Js/admin/Services/UserProfileService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        /* adminstration module ends */

        /* port module starts */

        .when('/Port/ordertype', { templateUrl: 'Js/Port/Views/OrderType/ordertype.html?v=' + Utility.Version })
        .when('/Port/lookup', { templateUrl: 'Js/Port/Views/Lookup/lookup.html?v=' + Utility.Version })

        .when('/Port/bookingentrylist', {
            templateUrl: 'Js/Port/Views/BookingEntry/bookingentrylist.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['Js/Port/Controllers/BookingEntryListController.js?v=' + Utility.Version,
                                    'Js/Port/Services/BookingEntryListService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/Port/bookingentry/:orderNo', {
            templateUrl: 'Js/Port/Views/BookingEntry/bookingentry.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['Js/port/Controllers/BookingEntry.js?v=' + Utility.Version,
                                    'Js/port/Controllers/VASChargesCntrl.js?v=' + Utility.Version,
                                    'Js/port/Controllers/BookingContainerInfoCntrl.js?v=' + Utility.Version,
                                    'js/port/Services/BookingEntryService.js?v=' + Utility.Version,
                                    'js/Operation/Services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/JobCategoryChargesService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/VesselMasterService.js?v=' + Utility.Version,
                                    'js/Operation/Services/VesselScheduleService.js?v=' + Utility.Version,
                                    'Js/MasterData/Services/PortAreaService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/Port/gatein', {
            templateUrl: 'Js/Port/Views/GateIn/gatein.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Port/controllers/GateIn.js?v=' + Utility.Version,
                                    'js/Port/Services/GateInService.js?v=' + Utility.Version,
                                    'js/Port/Services/BookingEntryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version,
                                    'js/Operation/Services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CountryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/VesselMasterService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
               .when('/Port/gateout', {
                   templateUrl: 'Js/Port/Views/GateOut/gateout.html?v=' + Utility.Version,
                   resolve: {
                       lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                           return $ocLazyLoad.load(
                               {
                                   name: 'Logicon',
                                   files: ['js/Port/controllers/GateOut.js?v=' + Utility.Version,
                                           'js/Port/Services/GateInService.js?v=' + Utility.Version,
                                           'js/Port/Services/BookingEntryService.js?v=' + Utility.Version,
                                           'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version,
                                           'js/Operation/Services/OrderEntryService.js?v=' + Utility.Version,
                                           'js/MasterData/Services/CountryService.js?v=' + Utility.Version,
                                           'js/MasterData/Services/VesselMasterService.js?v=' + Utility.Version]
                               }
                           );
                       }]
                   }
               })
        .when('/Port/containermaster', {
            templateUrl: 'Js/Port/Views/ContainerMaster/containermaster.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Port/controllers/ContainerMaster.js?v=' + Utility.Version,
                                    'js/Port/Services/ContainerMasterService.js?v=' + Utility.Version,
                                    'js/Port/Services/GateInService.js?v=' + Utility.Version,
                                    'js/Operation/Services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version
                                    //'js/MasterData/Services/MerchantProfileService.js',
                                    //'js/Operation/Services/OrderEntryService.js'
                            ]
                        }
                    );
                }]
            }
        })
        /* port module end */
        .when('/Port/vesselschedule', { templateUrl: 'Js/Tms/Views/VesselSchedule/vesselschedule.html?v=' + Utility.Version })
        .when('/MasterData/standardquotation', {
            templateUrl: 'Js/MasterData/Views/StandardQuotation/standardquotation.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/StandardQuotation.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CustomerQuotationService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/ChargeCodeService.js?v=' + Utility.Version,
                                    'js/Operation/Services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/MasterData/Directives/QuotationDetail.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/QuotationDetail.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/MasterData/customerquotation/list', {
            templateUrl: 'Js/MasterData/Views/CustomerQuotation/customerquotationlist.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/CustomerQuotation.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/addCustomerQuotationCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CustomerQuotationService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/ChargeCodeService.js?v=' + Utility.Version,
                                    'js/Operation/Services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/MasterData/Directives/QuotationDetail.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/QuotationDetail.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/MasterData/customerquotation/:quotation', {
            templateUrl: 'Js/MasterData/Views/CustomerQuotation/customerquotation.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/CustomerQuotation.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/addCustomerQuotationCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CustomerQuotationService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/ChargeCodeService.js?v=' + Utility.Version,
                                    'js/Operation/Services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/AddressService.js?v=' + Utility.Version,
                                    'js/MasterData/Directives/customer-address.js?v=' + Utility.Version,
                                    'js/MasterData/Directives/QuotationDetail.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/QuotationDetail.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/MasterData/vendorquotation/list', {
            templateUrl: 'Js/MasterData/Views/VendorQuotation/vendorquotationlist.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/VendorQuotation.js?v=' + Utility.Version,
                                    'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CustomerQuotationService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/ChargeCodeService.js?v=' + Utility.Version,
                                    'js/Operation/Services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/AddressService.js?v=' + Utility.Version,
                                    'js/MasterData/Directives/QuotationDetail.js?v=' + Utility.Version,
                                    'js/MasterData/Directives/customer-address.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/QuotationDetail.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/MasterData/vendorquotation/:quotation', {
            templateUrl: 'Js/MasterData/Views/VendorQuotation/vendorquotation.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/VendorQuotation.js?v=' + Utility.Version,
                                    'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CustomerQuotationService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/ChargeCodeService.js?v=' + Utility.Version,
                                    'js/Operation/Services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/AddressService.js?v=' + Utility.Version,
                                    'js/MasterData/Directives/QuotationDetail.js?v=' + Utility.Version,
                                    'js/MasterData/Directives/customer-address.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/QuotationDetail.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/MasterData/Associationdiscountlist', {
            templateUrl: 'Js/MasterData/Views/AssociationTariff/AssociationTarifflist.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/AssociationTariffCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/TariffService.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/TariffDetailCntrl.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/MasterData/Associationtariff/:tariffno', {
            templateUrl: 'Js/MasterData/Views/AssociationTariff/AssociationTariff.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/AssociationTariffCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/TariffService.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/TariffDetailCntrl.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/MasterData/Associationtariff', {
            templateUrl: 'Js/MasterData/Views/DiscountTariff/AssociationTariff.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/AssociationTariffCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/TariffService.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/TariffDetailCntrl.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/MasterData/rebatetariff/:tariffno', {
            templateUrl: 'Js/MasterData/Views/RebateTariff/RebateTariff.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/RebateTariffCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/TariffService.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/TariffDetailCntrl.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/MasterData/rebatetariff', {
            templateUrl: 'Js/MasterData/Views/RebateTariff/RebateTariff.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/RebateTariffCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/TariffService.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/TariffDetailCntrl.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/MasterData/rebatelist', {
            templateUrl: 'Js/MasterData/Views/RebateTariff/RebateTariffList.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/RebateTariffCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/TariffService.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/TariffDetailCntrl.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/MasterData/standardtariff', {
            templateUrl: 'Js/MasterData/Views/StandardTariff/StandardTariff.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/MasterData/controllers/StandardTariffCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/TariffService.js?v=' + Utility.Version,
                                    'js/MasterData/controllers/TariffDetailCntrl.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })


        /* Net tms module ends */

        /* operation module starts */
        .when('/operation/registrationlist', {
            templateUrl: 'Js/Operation/Views/Registration/registrationlist.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Operation/controllers/RegistrationList.js?v=' + Utility.Version,
                                    'js/Home/Services/RegisteredCompanyService.js?v=' + Utility.Version,
                                    'js/Home/Services/RegistrationService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/operation/subscriberslist', {
            templateUrl: 'Js/Operation/Views/Subscribers/subscriberslist.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Operation/controllers/Subscribers.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CompanyService.js?v=' + Utility.Version,
                                    'js/Billing/Services/StatementService.js?v=' + Utility.Version,
                                    'js/Reports/services/operationalservice.js?v=' + Utility.Version,
                                    'js/Reports/controllers/ReportController.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/operation/subscribers/:companycode', {
            templateUrl: 'Js/Operation/Views/Subscribers/subscriber.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Operation/controllers/Subscribers.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CompanyService.js?v=' + Utility.Version,
                                    'js/Billing/Services/StatementService.js?v=' + Utility.Version,
                                    'js/Reports/services/operationalservice.js?v=' + Utility.Version,
                                    'js/Reports/controllers/ReportController.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/operation/vesselschedule', {
            templateUrl: 'Js/Operation/Views/VesselSchedule/vesselschedule.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Operation/controllers/VesselSchedule.js?v=' + Utility.Version,
                                    'js/Operation/controllers/addEditVesselScheduleCntrl.js?v=' + Utility.Version,
                                    'js/Operation/Services/VesselScheduleService.js?v=' + Utility.Version,
                                    'Js/MasterData/Services/PortAreaService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/operation/manifest/vesselprofilelist', {
            templateUrl: 'Js/Operation/Views/VesselProfile/vesselprofilelist.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Operation/controllers/VesselProfile.js?v=' + Utility.Version,
                                    'js/Operation/Services/VesselProfileService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/operation/manifest/vesselprofile/:vesselID', {
            templateUrl: 'Js/Operation/Views/VesselProfile/vesselprofile.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Operation/controllers/VesselProfile.js?v=' + Utility.Version,
                                    'js/Operation/Services/VesselProfileService.js?v=' + Utility.Version,
                                    'js/Operation/controllers/addEditVesselScheduleCntrl.js?v=' + Utility.Version,
                                    'js/Operation/Services/VesselScheduleService.js?v=' + Utility.Version,
                                    'Js/MasterData/Services/PortAreaService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/operation/manifest/oceanbl', {
            templateUrl: 'Js/Operation/Views/VesselProfile/oceanbl.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Operation/controllers/VesselProfile.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/operation/manifest/test', {
            templateUrl: 'Js/Operation/Views/VesselProfile/test.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Operation/controllers/VesselProfile.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/edi/cusrep/inquiry', {
            templateUrl: 'Js/Manifest/Views/CUSREPInquiry.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Manifest/controllers/CUSREPInquiry.js?v=' + Utility.Version,
                                    'js/Manifest/Services/CUSREPInquiryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/VesselMasterService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/PortAreaService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/edi/cuscar/inquiry', {
            templateUrl: 'Js/Manifest/Views/CUSCARInquiry.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Manifest/controllers/CUSCARInquiry.js?v=' + Utility.Version,
                                    'js/Manifest/Services/CUSCARInquiryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/VesselMasterService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/PortAreaService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/declaration/k1inquiry', {
            templateUrl: 'Js/declaration/Views/k1inquiry/k1inquiry.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/declaration/controllers/k1Inquiry.js?v=' + Utility.Version,
                                    'js/declaration/services/k1Service.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/declaration/k2inquiry', {
            templateUrl: 'Js/declaration/Views/k2inquiry/k2inquiry.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/declaration/controllers/k2Inquiry.js?v=' + Utility.Version,
                                    'js/declaration/services/k2Service.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/declaration/k3inquiry', {
            templateUrl: 'Js/declaration/Views/k3inquiry/k3inquiry.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/declaration/controllers/k3Inquiry.js?v=' + Utility.Version,
                                    'js/declaration/services/k3Service.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/declaration/k8inquiry', {
            templateUrl: 'Js/declaration/Views/k8inquiry/k8inquiry.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/declaration/controllers/k8Inquiry.js?v=' + Utility.Version,
                                    'js/declaration/services/k8Service.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/declaration/k9inquiry', {
            templateUrl: 'Js/declaration/Views/k9inquiry/k9inquiry.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/declaration/controllers/k9Inquiry.js?v=' + Utility.Version,
                                    'js/declaration/services/k9Service.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/declaration/k1declaration/:declarationNo', {
            templateUrl: 'Js/declaration/Views/k1/k1declaration.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/declaration/controllers/k1Controller.js?v=' + Utility.Version,
                                    'js/declaration/controllers/AddEditItemEntry.js?v=' + Utility.Version,
                                    'js/declaration/controllers/AddDownload.js?v=' + Utility.Version,
                                    'js/declaration/services/k1Service.js?v=' + Utility.Version,
                                    'js/declaration/services/ItemEntryService.js?v=' + Utility.Version,
                                    'js/MasterData/services/MerchantProfileService.js?v=' + Utility.Version,
                                    'js/MasterData/services/PortAreaService.js?v=' + Utility.Version,
                                    'js/MasterData/services/CurrencyRateService.js?v=' + Utility.Version,
                                    'js/Operation/services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/Operation/services/VesselScheduleService.js?v=' + Utility.Version,
                                    'js/MasterData/services/VesselMasterService.js?v=' + Utility.Version,
                                    'js/declaration/controllers/k1Clause.js?v=' + Utility.Version,
                                    'js/declaration/controllers/k1Document.js?v=' + Utility.Version,
                                    'js/MasterData/Services/AddressService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/HSCodeService.js?v=' + Utility.Version,
                                    'js/Reports/controllers/ReportController.js?v=' + Utility.Versions]
                        }
                    );
                }]
            }
        })

        .when('/declaration/k2declaration/:declarationNo', {
            templateUrl: 'Js/declaration/Views/k2/k2declaration.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/declaration/controllers/k2Controller.js?v=' + Utility.Version,
                                    'js/declaration/controllers/k2ItemEntry.js?v=' + Utility.Version,
                                    'js/declaration/controllers/AddDownload.js?v=' + Utility.Version,
                                    'js/declaration/services/k2Service.js?v=' + Utility.Version,
                                    'js/declaration/services/ItemEntryService.js?v=' + Utility.Version,
                                    'js/MasterData/services/MerchantProfileService.js?v=' + Utility.Version,
                                    'js/MasterData/services/PortAreaService.js?v=' + Utility.Version,
                                    'js/MasterData/services/CurrencyRateService.js?v=' + Utility.Version,
                                    'js/Operation/services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/Operation/services/VesselScheduleService.js?v=' + Utility.Version,
                                    'js/MasterData/services/VesselMasterService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/AddressService.js?v=' + Utility.Version,
                                    'js/declaration/controllers/k2Document.js?v=' + Utility.Version,
                                    'js/declaration/controllers/k2Clause.js?v=' + Utility.Version,
                                    'js/Reports/controllers/ReportController.js?v=' + Utility.Version,
                                    'js/MasterData/Services/HSCodeService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/declaration/k3declaration/:declarationNo', {
            templateUrl: 'Js/declaration/Views/k3/k3declaration.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/declaration/controllers/k3Controller.js?v=' + Utility.Version,
                                    'js/declaration/controllers/k3ItemEntry.js?v=' + Utility.Version,
                                    'js/declaration/controllers/AddDownload.js?v=' + Utility.Version,
                                    'js/declaration/services/k3Service.js?v=' + Utility.Version,
                                    'js/declaration/services/ItemEntryService.js?v=' + Utility.Version,
                                    'js/MasterData/services/MerchantProfileService.js?v=' + Utility.Version,
                                    'js/MasterData/services/PortAreaService.js?v=' + Utility.Version,
                                    'js/MasterData/services/CurrencyRateService.js?v=' + Utility.Version,
                                    'js/Operation/services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/Operation/services/VesselScheduleService.js?v=' + Utility.Version,
                                    'js/MasterData/services/VesselMasterService.js?v=' + Utility.Version,
                                    //'js/declaration/controllers/k3Clause.js?v=' + Utility.Version,
                                    'js/declaration/controllers/k3Document.js?v=' + Utility.Version,
                                    'js/MasterData/Services/AddressService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/declaration/k8declaration/:declarationNo', {
            templateUrl: 'Js/declaration/Views/k8/k8declaration.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/declaration/controllers/k8Controller.js?v=' + Utility.Version,
                                    'js/declaration/controllers/k8ItemEntry.js?v=' + Utility.Version,
                                    'js/declaration/controllers/AddDownload.js?v=' + Utility.Version,
                                    'js/declaration/services/k8Service.js?v=' + Utility.Version,
                                    'js/declaration/services/ItemEntryService.js?v=' + Utility.Version,
                                    'js/MasterData/services/MerchantProfileService.js?v=' + Utility.Version,
                                    'js/MasterData/services/PortAreaService.js?v=' + Utility.Version,
                                    'js/MasterData/services/CurrencyRateService.js?v=' + Utility.Version,
                                    'js/Operation/services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/Operation/services/VesselScheduleService.js?v=' + Utility.Version,
                                    'js/MasterData/services/VesselMasterService.js?v=' + Utility.Version,
                                    'js/declaration/controllers/k8Clause.js?v=' + Utility.Version,
                                    'js/declaration/controllers/k8Document.js?v=' + Utility.Version,
                                    'js/MasterData/Services/AddressService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/declaration/k9declaration/:declarationNo', {
            templateUrl: 'Js/declaration/Views/k9/k9declaration.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/declaration/controllers/k9Controller.js?v=' + Utility.Version,
                                    'js/declaration/controllers/k9ItemEntry.js?v=' + Utility.Version,
                                    'js/declaration/controllers/AddDownload.js?v=' + Utility.Version,
                                    'js/declaration/services/k9Service.js?v=' + Utility.Version,
                                    'js/declaration/services/ItemEntryService.js?v=' + Utility.Version,
                                    'js/MasterData/services/MerchantProfileService.js?v=' + Utility.Version,
                                    'js/MasterData/services/PortAreaService.js?v=' + Utility.Version,
                                    'js/MasterData/services/CurrencyRateService.js?v=' + Utility.Version,
                                    'js/Operation/services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/Operation/services/VesselScheduleService.js?v=' + Utility.Version,
                                    'js/MasterData/services/VesselMasterService.js?v=' + Utility.Version,
                                    'js/declaration/controllers/k9Clause.js?v=' + Utility.Version,
                                    'js/declaration/controllers/k9Document.js?v=' + Utility.Version,
                                    'js/MasterData/Services/AddressService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/operation/orderentry', {
            templateUrl: 'Js/Operation/Views/OrderEntry/orderentry.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Operation/controllers/OrderEntryController.js?v=' + Utility.Version,
                                    'js/Operation/controllers/AddOrderEntryContainer.js?v=' + Utility.Version,
                                    'js/Operation/controllers/AddOrderEntryTransport.js?v=' + Utility.Version,
                                    'js/Operation/controllers/AddOrderEntryCargo.js?v=' + Utility.Version,
                                    'js/Operation/Services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/Operation/Services/VesselScheduleService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/JobCategoryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/AddressService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CountryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/HSCodeService.js?v=' + Utility.Version,
                                    'Js/MasterData/Services/PortAreaService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version
                            ]
                        }
                    );
                }]
            }
        })
        .when('/operation/orderentry/:orderno', {
            templateUrl: 'Js/Operation/Views/OrderEntry/orderentry.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Operation/controllers/OrderEntryController.js?v=' + Utility.Version,
                                    'js/Operation/controllers/AddOrderEntryContainer.js?v=' + Utility.Version,
                                    'js/Operation/controllers/AddOrderEntryTransport.js?v=' + Utility.Version,
                                    'js/Operation/controllers/AddOrderEntryCargo.js?v=' + Utility.Version,
                                    'js/Operation/Services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/Operation/Services/VesselScheduleService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/JobCategoryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/AddressService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CountryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/HSCodeService.js?v=' + Utility.Version,
                                    'Js/MasterData/Services/PortAreaService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version
                            ]
                        }
                    );
                }]
            }
        })
        .when('/Port/orderentry', {
            templateUrl: 'Js/Port/Views/OrderEntry/orderentry.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Operation/controllers/OrderEntryController.js?v=' + Utility.Version,
                                    'js/Operation/controllers/AddOrderEntryContainer.js?v=' + Utility.Version,
                                    'js/Operation/controllers/AddOrderEntryTransport.js?v=' + Utility.Version,
                                    'js/Operation/controllers/AddOrderEntryCargo.js?v=' + Utility.Version,
                                    'js/Operation/Services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/Operation/Services/VesselScheduleService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/JobCategoryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/AddressService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CountryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/HSCodeService.js?v=' + Utility.Version,
                                    'Js/MasterData/Services/PortAreaService.js?v=' + Utility.Version
                            ]
                        }
                    );
                }]
            }
        })
        .when('/operation/orderentrylist', {
            templateUrl: 'Js/Operation/Views/OrderEntry/orderentrylist.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Operation/controllers/OrderEntryListController.js?v=' + Utility.Version,
                                    'js/Operation/Services/OrderEntryListService.js?v=' + Utility.Version
                            ]
                        }
                    );
                }]
            }
        })
        .when('/operation/orderentry/:orderno/:branchid', {
            templateUrl: 'Js/Operation/Views/OrderEntry/orderentry.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Operation/controllers/OrderEntryController.js?v=' + Utility.Version,
                                    'js/Operation/controllers/AddOrderEntryContainer.js?v=' + Utility.Version,
                                    'js/Operation/controllers/AddOrderEntryTransport.js?v=' + Utility.Version,
                                    'js/Operation/controllers/AddOrderEntryCargo.js?v=' + Utility.Version,
                                    'js/Operation/Services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/JobCategoryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/AddressService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CountryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/HSCodeService.js?v=' + Utility.Version,
                                    'Js/MasterData/Services/PortAreaService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        /* operation module ends */

        /* billing module starts */
        .when('/billing/costsheet', {
            templateUrl: 'Js/Billing/Views/CostSheet/costsheet.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Billing/controllers/CostSheet.js?v=' + Utility.Version,
                                    'js/Billing/controllers/addEditCostSheetCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/ChargeCodeService.js?v=' + Utility.Version,
                                    'js/Billing/Services/CostSheetService.js?v=' + Utility.Version,
                                    'js/Billing/Services/CustomerInvoiceService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/billing/costsheet/:orderno', {
            templateUrl: 'Js/Billing/Views/CostSheet/costsheet.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Billing/controllers/CostSheet.js?v=' + Utility.Version,
                                    'js/Billing/controllers/addEditCostSheetCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/ChargeCodeService.js?v=' + Utility.Version,
                                    'js/Billing/Services/CostSheetService.js?v=' + Utility.Version,
                                     'js/Billing/Services/CustomerInvoiceService.js?v=' + Utility.Version
                            ]
                        }
                    );
                }]
            }
        })
        .when('/billing/customerinvoicelist', {
            templateUrl: 'Js/Billing/Views/CustomerInvoice/customerinvoicelist.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Billing/controllers/CustomerInvoiceList.js?v=' + Utility.Version,
                                    'js/Billing/controllers/CustomerInvoice.js?v=' + Utility.Version,
                                    'js/Billing/controllers/addEditCustomerInvoice.js?v=' + Utility.Version,
                                    'js/MasterData/Services/ChargeCodeService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version,
                                    'js/Billing/Services/CostSheetService.js?v=' + Utility.Version,
                                    'js/Billing/Services/CustomerInvoiceService.js?v=' + Utility.Version
                            ]
                        }
                    );
                }]
            }
        })
         .when('/billing/customerinvoice', {
             templateUrl: 'Js/Billing/Views/CustomerInvoice/customerinvoice.html?v=' + Utility.Version,
             resolve: {
                 lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(
                         {
                             name: 'Logicon',
                             files: ['js/Billing/controllers/CustomerInvoice.js?v=' + Utility.Version,
                                     'js/Billing/controllers/addEditCustomerInvoice.js?v=' + Utility.Version,
                                     'js/MasterData/Services/ChargeCodeService.js?v=' + Utility.Version,
                                     'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version,
                                     'js/Billing/Services/CostSheetService.js?v=' + Utility.Version,
                                     'js/Billing/Services/CustomerInvoiceService.js?v=' + Utility.Version
                             ]
                         }
                     );
                 }]
             }
         })
         .when('/billing/customercashbill', {
             templateUrl: 'Js/Billing/Views/CustomerCashBill/customercashbill.html?v=' + Utility.Version,
             resolve: {
                 lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(
                         {
                             name: 'Logicon',
                             files: ['js/Billing/controllers/CustomerCashBill.js?v=' + Utility.Version,
                                     'js/Billing/controllers/addEditCustomerCashBill.js?v=' + Utility.Version,
                                      'js/Billing/Services/CustomerCashBillService.js?v=' + Utility.Version,
                                       'js/MasterData/Services/ChargeCodeService.js?v=' + Utility.Version,
                                      'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version,
                                      'js/Billing/Services/CostSheetService.js?v=' + Utility.Version,
                                     'js/Billing/Services/CustomerInvoiceService.js?v=' + Utility.Version
                             ]
                         }
                     );
                 }]
             }
         })

        //.when('/billing/costsheet', { templateUrl: 'Js/Billing/Views/CostSheet/costsheet.html?v=' + Utility.Version })
        //.when('/billing/costsheet/:orderno', { templateUrl: 'Js/Billing/Views/CostSheet/costsheet.html?v=' + Utility.Version })
        .when('/billing/unbilledorders', { templateUrl: 'Js/Billing/Views/UnBilledOrders/unbilledorders.html?v=' + Utility.Version })
        //.when('/billing/customerinvoice', { templateUrl: 'Js/Billing/Views/CustomerInvoice/customerinvoice.html?v=' + Utility.Version })
        //.when('/billing/customercashbill', { templateUrl: 'Js/Billing/Views/CustomerCashBill/customercashbill.html?v=' + Utility.Version })
        .when('/billing/customercashbill/:type', { templateUrl: 'Js/Billing/Views/CustomerCashBill/customercashbill.html?v=' + Utility.Version })
        .when('/billing/customercreditnote', { templateUrl: 'Js/Billing/Views/CustomerCreditNote/customercreditnote.html?v=' + Utility.Version })
        .when('/billing/cnsubmission', { templateUrl: 'Js/Billing/Views/CNSubmission/cnsubmission.html?v=' + Utility.Version })
        .when('/billing/whcustomerinvoice', { templateUrl: 'Js/Billing/Views/WHCustomerInvoice/whcustomerinvoice.html?v=' + Utility.Version })
        .when('/billing/vendorinvoice', { templateUrl: 'Js/Billing/Views/VendorInvoice/vendorinvoice.html?v=' + Utility.Version })
        .when('/billing/vendorreversebilling', { templateUrl: 'Js/Billing/Views/VendorReverseBilling/vendorreversebilling.html?v=' + Utility.Version })
        .when('/billing/vendorcreditnote', { templateUrl: 'Js/Billing/Views/VendorCreditNote/vendorcreditnote.html?v=' + Utility.Version })
        .when('/billing/paymentrequest', { templateUrl: 'Js/Billing/Views/PaymentRequest/paymentrequest.html?v=' + Utility.Version })
        .when('/billing/advanceentry', { templateUrl: 'Js/Billing/Views/AdvanceEntry/advanceentry.html?v=' + Utility.Version })
        .when('/billing/advancesettlement', { templateUrl: 'Js/Billing/Views/AdvanceSettlement/advancesettlement.html?v=' + Utility.Version })
        .when('/billing/billcostapproval', { templateUrl: 'Js/Billing/Views/BillCostApproval/billcostapproval.html?v=' + Utility.Version })

        .when('/billing/pendingbilling', {
            templateUrl: 'Js/Billing/Views/PendingBilling/PendingBilling.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Billing/controllers/PendingBilling.js?v=' + Utility.Version,
                                    'js/Billing/services/PendingBillingService.js?v=' + Utility.Version,
                                    'js/MasterData/services/CompanyService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/billing/paymenthistory', {
            templateUrl: 'Js/Billing/Views/paymenthistory/paymenthistory.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Billing/controllers/PaymentHistory.js?v=' + Utility.Version,
                                    'js/Billing/services/PendingBillingService.js?v=' + Utility.Version,
                                    'js/MasterData/services/CompanyService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/billing/generateinvoice/:statementno', {
            templateUrl: 'Js/Billing/Views/GenerateInvoice/GenerateInvoice.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Billing/controllers/GenerateInvoice.js?v=' + Utility.Version,
                                    'js/Billing/controllers/addEditGenerateInvoiceCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/ChargeCodeService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version,
                                    'js/Billing/Services/CostSheetService.js?v=' + Utility.Version,
                                    'js/Billing/Services/CustomerInvoiceService.js?v=' + Utility.Version,
                                    'js/Billing/Services/PaymentService.js?v=' + Utility.Version,
                                    'js/Billing/Services/StatementService.js?v=' + Utility.Version
                            ]
                        }
                    );
                }]
            }
        })
        .when('/billing/generateinvoice', {
            templateUrl: 'Js/Billing/Views/GenerateInvoice/GenerateInvoice.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Billing/controllers/GenerateInvoice.js?v=' + Utility.Version,
                                    'js/Billing/controllers/addEditGenerateInvoiceCntrl.js?v=' + Utility.Version,
                                    'js/MasterData/Services/ChargeCodeService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version,
                                    'js/Billing/Services/CostSheetService.js?v=' + Utility.Version,
                                    'js/Billing/Services/CustomerInvoiceService.js?v=' + Utility.Version
                            ]
                        }
                    );
                }]
            }
        })
        .when('/billing/generatereceipt/:paymentno/:statementno', {
            templateUrl: 'Js/Billing/Views/GenerateReceipt/GenerateReceipt.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Billing/controllers/GenerateReceipt.js?v=' + Utility.Version,
                                    'js/Billing/Services/PaymentService.js?v=' + Utility.Version,
                                    'js/Billing/Services/CustomerInvoiceService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        /* adminstration module ends */

        /* freight module starts */
        .when('/freight/houseairwaybill', {
            templateUrl: 'Js/Freight/Views/HouseAirWaybill/houseairwaybill.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Freight/controllers/HouseAirWaybill.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/freight/containerrequest', {
            templateUrl: 'Js/Freight/Views/ContainerRequest/ContainerRequest.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Freight/controllers/ContainerRequest.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/freight/containermonitor', {
            templateUrl: 'Js/Freight/Views/ContainerMonitor/ContainerMonitor.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Freight/controllers/ContainerMonitor.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/freight/billoflad', {
            templateUrl: 'Js/Freight/Views/BillOfLad/billoflad.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Freight/controllers/BillOfLad.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/freight/shippinginstruction', {
            templateUrl: 'Js/Freight/Views/ShippingInstruction/shippinginstruction.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Freight/controllers/ShippingInstruction.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/freight/shipperinvoice', {
            templateUrl: 'Js/Freight/Views/ShipperInvoice/shipperinvoice.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Freight/controllers/ShipperInvoice.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/freight/documentation', {
            templateUrl: 'Js/Freight/Views/Documentation/documentation.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Freight/controllers/documentation.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/freight/transportmonitor', {
            templateUrl: 'Js/Freight/Views/TransportMonitor/transportmonitor.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Freight/controllers/TransportMonitor.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/freight/collectionadvice', {
            templateUrl: 'Js/Freight/Views/CollectionAdvice/collectionadvice.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Freight/controllers/CollectionAdvice.js?v=' + Utility.Version,
                                    'js/Freight/controllers/CollectionAdviceFilter.js?v=' + Utility.Version,
                                    'js/Freight/Services/CollectionAdviceService.js?v=' + Utility.Version,
                                    'Js/MasterData/Services/PortAreaService.js?v=' + Utility.Version
                            ]
                        }
                    );
                }]
            }
        })

        .when('/freight/transportrequest', {
            templateUrl: 'Js/Freight/Views/TransportRequest/transportrequest.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Freight/controllers/TransportRequest.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        /* freight module ends */

        /* billing module starts */
        .when('/transport/loadplanning', { templateUrl: 'Js/Transport/Views/LoadPlanning/loadplanning.html?v=' + Utility.Version })
        .when('/transport/equipmentplanning', { templateUrl: 'Js/Transport/Views/EquipmentPlanning/equipmentplanning.html?v=' + Utility.Version })
        .when('/transport/driverpaymentvendor', { templateUrl: 'Js/Transport/Views/DriverPaymentVendor/driverpaymentvendor.html?v=' + Utility.Version })
        .when('/transport/driverpaymentbreakdown', { templateUrl: 'Js/Transport/Views/DriverPaymentBreakdown/driverpaymentbreakdown.html?v=' + Utility.Version })
        .when('/transport/equipmentcostentry', { templateUrl: 'Js/Transport/Views/EquipmentCostEntry/equipmentcostentry.html?v=' + Utility.Version })
        .when('/transport/fuelusage', { templateUrl: 'Js/Transport/Views/FuelUsage/fuelusage.html?v=' + Utility.Version })
        .when('/transport/varianceackmonitor', { templateUrl: 'Js/Transport/Views/VarianceAckMonitor/varianceackmonitor.html?v=' + Utility.Version })
        .when('/transport/driverratetbl', { templateUrl: 'Js/Transport/Views/DriverRateTbl/driverratetbl.html?v=' + Utility.Version })

        /* adminstration module ends */

        /* inquiry start */
        .when('/inquires/orderinquiry', {
            templateUrl: 'Js/Inquiry/Views/OrderInquiry/orderinquiry.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Inquiry/controllers/OrderInquiryCntrl.js?v=' + Utility.Version,
                                    'js/Inquiry/services/OrderInquiryService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/inquires/outstandingmvtsinquiry', {
            templateUrl: 'Js/Inquiry/Views/OutstandingMvts/OutstandingMvts.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Inquiry/controllers/OutstandingMvts.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/inquires/containercargostatus', {
            templateUrl: 'Js/Inquiry/Views/containercargo/containercargo.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Inquiry/controllers/OutstandingMvts.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/inquires/bookinginquiry', {
            templateUrl: 'Js/Inquiry/Views/bookinginquiry/bookinginquiry.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Inquiry/controllers/BookingInquiry.js?v=' + Utility.Version,
                            'js/Inquiry/Services/BookingInquiryService.js?v=' + Utility.Version,
                            'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/inquires/truckmvmtinquiry', {
            templateUrl: 'Js/Inquiry/Views/truckmvmtinquiry/truckmvmtinquiry.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Inquiry/controllers/truckmvmtinquiry.js?v=' + Utility.Version,
                            'js/Inquiry/Services/TruckMovementService.js?v=' + Utility.Version,
                            'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/inquiry/subscriber', {
            templateUrl: 'Js/inquiry/Views/Subscriberinquiry/Subscriberinquiry.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/inquiry/controllers/Subscriberinquiry.js?v=' + Utility.Version,
                                    'js/Billing/services/PendingBillingService.js?v=' + Utility.Version,
                                    'js/MasterData/services/CompanyService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/inquiry/payment', {
            templateUrl: 'Js/inquiry/Views/paymentinquiry/paymentinquiry.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/inquiry/controllers/paymentinquiry.js?v=' + Utility.Version,
                                    'js/Billing/services/PendingBillingService.js?v=' + Utility.Version,
                                    'js/MasterData/services/CompanyService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/inquiry/invoice', {
            templateUrl: 'Js/inquiry/Views/invoiceinquiry/invoiceinquiry.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/inquiry/controllers/invoiceinquiry.js?v=' + Utility.Version,
                                    'js/Billing/services/PendingBillingService.js?v=' + Utility.Version,
                                    'js/MasterData/services/CompanyService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/inquiry/receipt', {
            templateUrl: 'Js/inquiry/Views/receiptinquiry/receiptinquiry.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/inquiry/controllers/receiptinquiry.js?v=' + Utility.Version,
                                    'js/Billing/services/PendingBillingService.js?v=' + Utility.Version,
                                    'js/MasterData/services/CompanyService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        /* inquiry end */
        .when('/reports/operational', {
            templateUrl: 'Js/Reports/Views/Operational/operational.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Reports/controllers/OperationalReports.js?v=' + Utility.Version,
                                    'js/Reports/services/operationalservice.js?v=' + Utility.Version,
                                    'js/Reports/controllers/ReportController.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/reports/management', {
            templateUrl: 'Js/Reports/Views/management/management.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/Reports/controllers/ManagementReports.js?v=' + Utility.Version,
                                    'js/Reports/services/operationalservice.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        /*Haulage module starts*/
        .when('/haulage/driverincentive', {
            templateUrl: 'Js/haulage/Views/driverincentive/driverincentive.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/haulage/controllers/driverincentivecontroller.js?v=' + Utility.Version,
                                    'js/haulage/controllers/editdriverincentivecontroller.js?v=' + Utility.Version,
                                    'js/haulage/services/driverincentiveService.js?v=' + Utility.Version,
                                    'js/Operation/Services/OrderEntryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/MerchantProfileService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/JobCategoryService.js?v=' + Utility.Version,
                                    'js/MasterData/Services/ChargeCodeService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/haulage/equipmentplanning', {
            templateUrl: 'Js/haulage/Views/equipmentplanning/equipmentplanning.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/haulage/controllers/equipmentplanning.js?v=' + Utility.Version,
                                    'js/haulage/controllers/editequipmentcontroller.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/haulage/operationmonitor', {
            templateUrl: 'Js/haulage/Views/operationmonitor/operationmonitor.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/haulage/controllers/operationmonitor.js?v=' + Utility.Version,
                                    'js/haulage/controllers/editoperationmonitor.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/haulage/collectionadvice', {
            templateUrl: 'Js/haulage/Views/collectionadvice/collectionadvice.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/haulage/controllers/collectionadvice.js?v=' + Utility.Version,
                                    'js/haulage/controllers/editcollectionadvice.js?v=' + Utility.Version,
                                    'Js/MasterData/Services/PortAreaService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/haulage/driverpayment', {
            templateUrl: 'Js/haulage/Views/driverpaymentbyvendor/driverpaymentbyvendor.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/haulage/controllers/driverpaymentbyvendor.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/haulage/driverpaymentbreakdown', {
            templateUrl: 'Js/haulage/Views/driverpaymentbreakdown/driverpaymentbreakdown.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/haulage/controllers/driverpaymentbreakdown.js?v=' + Utility.Version,
                            'js/haulage/controllers/editdriverpaymentbreakdown.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/haulage/driverpaymentapproval', {
            templateUrl: 'Js/haulage/Views/driverpaymentapproval/driverpaymentapproval.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/haulage/controllers/driverpaymentapproval.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/haulage/equipmentcostentry', {
            templateUrl: 'Js/haulage/Views/equipmentcostentry/equipmentcostentry.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/haulage/controllers/equipmentcostentry.js?v=' + Utility.Version,
                                    'js/haulage/controllers/editequipmentcostentry.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/haulage/fuelusage', {
            templateUrl: 'Js/haulage/Views/fuelusage/fuelusage.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/haulage/controllers/fuelusage.js?v=' + Utility.Version,
                                    'js/haulage/controllers/editfuelusage.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/haulage/porttimeslot', {
            templateUrl: 'Js/haulage/Views/porttimeslot/porttimeslot.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/haulage/controllers/porttimeslot.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        //
        /*Haulage module ends*/

        /*Administration module starts*/
        .when('/admin/userlist', {
            templateUrl: 'Js/admin/Views/userprofile/userlist.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/admin/controllers/userslistcontroller.js?v=' + Utility.Version,
                                    'js/admin/services/userprofileservice.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/admin/AssociationList', {
            templateUrl: 'Js/admin/Views/AssociationList/associationlist.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/admin/controllers/AssociationListController.js?v=' + Utility.Version,
                                'js/MasterData/Services/CountryService.js?v=' + Utility.Version,
                            'js/admin/Services/AssociationListService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/admin/snnupdate', {
            templateUrl: 'js/admin/CompanyProfile/Views/snnlist.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/admin/CompanyProfile/Controller/snnupdates.js?v=' + Utility.Version,
                            'js/admin/Services/SnnService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        .when('/admin/Association/:associationID', {
            templateUrl: 'Js/admin/Views/AssociationList/associationheader.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/admin/controllers/AssociationHeaderController.js?v=' + Utility.Version,
                                'js/MasterData/Services/CountryService.js?v=' + Utility.Version,
                                'js/admin/Services/AssociationListService.js?v=' + Utility.Version,
                                'js/MasterData/Services/CompanyService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
         .when('/admin/CompanyBilling', {
             templateUrl: 'Js/admin/Views/CompanyBilling/CompanyBilling.html?v=' + Utility.Version,
             resolve: {
                 lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(
                         {
                             name: 'Logicon',
                             files: ['js/admin/controllers/CompanyBillingController.js?v=' + Utility.Version,
                                    'js/MasterData/Services/CompanyService.js?v=' + Utility.Version,
                                    'js/Billing/Services/StatementService.js?v=' + Utility.Version,
                                    'js/Reports/services/operationalservice.js?v=' + Utility.Version,
                                    'js/Reports/controllers/ReportController.js?v=' + Utility.Version]
                         }
                     );
                 }]
             }
         })
        .when('/admin/userprofile', {
            templateUrl: 'Js/admin/Views/userprofile/userprofile.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/admin/controllers/userprofilecontroller.js?v=' + Utility.Version,
                                    'js/admin/services/userprofileservice.js?v=' + Utility.Version,
                                    'js/Security/Services/SecurableService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/admin/userprofile/:userID', {
            templateUrl: 'Js/admin/Views/userprofile/userprofile.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/admin/controllers/userprofilecontroller.js?v=' + Utility.Version,
                                    'js/admin/services/userprofileservice.js?v=' + Utility.Version,
                                    'js/Security/Services/SecurableService.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })

        .when('/powerbi/report', {
            templateUrl: 'Js/PowerBI/Views/report.html?v=' + Utility.Version,
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'Logicon',
                            files: ['js/PowerBI/controllers/PowerBICntrl.js?v=' + Utility.Version]
                        }
                    );
                }]
            }
        })
        /*Administration module end*/

        .otherwise({ templateUrl: 'Js/Error/Views/404.html?v=' + Utility.Version })

    growlProvider.onlyUniqueMessages(false);
    growlProvider.globalTimeToLive(4000);
    growlProvider.globalDisableIcons(false);
    growlProvider.globalDisableCountDown(true);
    $httpProvider.interceptors.push('HttpRequestInterceptor');

    $uibTooltipProvider.options({
        'popover-mode': 'single'
    });


    /*
    momentPickerProvider.options({
        autoclose: true,
        today: false,
        keyboard: false
    });*/

});
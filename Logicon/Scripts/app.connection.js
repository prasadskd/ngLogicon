﻿
app.constant('Utility', {
    BaseUrl: window.location.host.indexOf('localhost') != -1 ? 'http://localhost/LogiCon' : (window.location.host.indexOf('http://ragsarma-001-site10.htempurl.com') != -1 ? 'http://ragsarma-001-site12.htempurl.com' : 'http://ragsarma-001-site19.htempurl.com'),
    ServiceUrl: window.location.host.indexOf('localhost') != -1 ? 'http://localhost/LogiCon/api' : (window.location.host.indexOf('http://ragsarma-001-site10.htempurl.com') != -1 ? 'http://ragsarma-001-site12.htempurl.com/api' : 'http://ragsarma-001-site19.htempurl.com/api'),
    NetTmsUrl: 'http://localhost/NetTms.Api/api',
    CreatedBy: 'DEFAULTUSER',
    ModifiedBy: 'DEFAULTUSER',
    ReportPath: 'http://localhost/LogiCon/Report',
    Version: version,
    appVersion: '0.0.19'
});

//app.constant('Utility', {
//    BaseUrl: 'http://localhost:50766',
//    ServiceUrl: 'http://localhost:50766/api',
//    NetTmsUrl: 'http://localhost:50766/api',
//    ReportPath: 'http://localhost:50766/Report',
//    CreatedBy: 'DEFAULTUSER',
//    ModifiedBy: 'DEFAULTUSER',
//    Version: version,
//    appVersion: '0.0.20'
//});


//app.constant('Utility', {
//    BaseUrl: 'http://104.41.209.98',
//    ServiceUrl: 'http://104.41.209.98/api',
//    NetTmsUrl: 'http://localhost/NetTms.Api/api',
//    ReportPath: 'http://104.41.209.98/Report',
//    CreatedBy: 'DEFAULTUSER',
//    ModifiedBy: 'DEFAULTUSER',
//    Version: version,
//    appVersion: '0.0.2'
//});

//app.constant('Utility', {
//    BaseUrl: 'http://ragsarma-001-site12.htempurl.com',
//    ServiceUrl: 'http://ragsarma-001-site12.htempurl.com/api',
//    NetTmsUrl: 'http://ragsarma-001-site12.htempurl.com/api',
//    ReportPath: 'http://ragsarma-001-site12.htempurl.com/Report',
//    CreatedBy: 'DEFAULTUSER',
//    ModifiedBy: 'DEFAULTUSER',
//    Version: version,
//    appVersion: '0.0.21'
//});

//app.constant('Utility', {
//    BaseUrl: 'http://localhost',
//    ServiceUrl: 'http://localhost/api',
//    NetTmsUrl: 'http://localhost/api',
//    ReportPath: 'http://localhost/Report',
//    CreatedBy: 'DEFAULTUSER',
//    ModifiedBy: 'DEFAULTUSER',
//    Version: version,
//    appVersion: '0.0.12'
//});

//app.constant('Utility', {
//    BaseUrl: 'http://52.163.93.14',
//    ServiceUrl: 'http://52.163.93.14/api',
//    NetTmsUrl: 'http://localhost/NetTms.Api/api',
//    ReportPath: 'http://52.163.93.14/Report',
//    CreatedBy: 'DEFAULTUSER',
//    ModifiedBy: 'DEFAULTUSER',
//    Version: version,
//    appVersion: '0.0.19'
//});
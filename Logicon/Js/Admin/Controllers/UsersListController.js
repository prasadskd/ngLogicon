﻿app.controller('UsersListCntrl', ['$scope', 'UserProfileService', '$location', function ($scope, UserProfileService, $location) {
    $scope.showLoading = true;
    UserProfileService.getUsersList().then(function (d) {
        $scope.showLoading = false;
        $scope.usersList = d.data;
        debugger;
    }, function (err) { });

    $scope.userInfo = function (userID) {
        $location.path('/admin/userprofile/' + userID);
    };
}]);
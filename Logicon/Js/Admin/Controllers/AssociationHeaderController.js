﻿app.controller('AssociationHeaderController', ['$scope', '$uibModal', 'Utility', 'CountryService', 'AssociationListService', '$routeParams','$window','growl',
    function ($scope, $uibModal, Utility, CountryService, AssociationListService, $routeParams, $window, growl) {

        var associationId = $routeParams.associationID;
        $scope.association = {
            AssociationID: associationId,
            associationDetails: new Array()
        };


        $scope.GetCountriesList = function () {
            CountryService.GetCountriesList().then(function (d) {
                $scope.lookupData = d.data;
            }, function (err) {

            });
        };
        
        var associationDetailIndex = -1;
        $scope.AddDetail = function (index) {
            associationDetailIndex = index;
            $scope.modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/Admin/Templates/associationdetails.html?v=' + Utility.Version,
                size: 'md',
                controller: 'AssociationDetailsController',
                resolve: {
                    dataObj: function () {
                        return {

                            asDetail: (index == -1 ? {} : $scope.association.associationDetails[associationDetailIndex]),

                        };
                    }
                }
            });

            $scope.modalInstance.result.then(function (asDetail) {
                if (associationDetailIndex != -1) {
                    $scope.association.associationDetails[associationDetailIndex] = asDetail;
                }
                else {
                    if ($scope.association.associationDetails != null) {
                        $scope.association.associationDetails.push(asDetail);
                    }
                    else {
                        $scope.association.associationDetails = new Array();
                        $scope.association.associationDetails.push(asDetail);
                    }
                }
            }, function () {

            });

        };


        $scope.DeleteDetail = function (inx) {
            var r = $window.confirm('Are you sure you want to delete ?');
            if(r)
                $scope.association.associationDetails.splice(inx, 1);
        }

        $scope.SaveAssociation = function (association) {            

            if (association.associationDetails.length > 0) {
                AssociationListService.saveAssociation(association).then(function (d) {
                    growl.success('Saved Successfully..');
                }, function (err) {

                });
            } else {
                growl.error('Please add atleast one detail element');
            }
            
        }        

        var associationId = $routeParams.associationID;
        if (associationId != -1) {
            AssociationListService.GetAssociation(associationId).then(function (d) {
                $scope.association = d.data;
            }, function (err) { });
            $scope.GetCountriesList();
        } else {
            $scope.GetCountriesList();
        }

    }]);

app.controller('AssociationDetailsController', ['$scope', '$uibModalInstance', 'dataObj', 'growl', 'CompanyService', 'limitToFilter', function ($scope, $uibModalInstance, dataObj, growl, CompanyService, limitToFilter) {
    
    $scope.asDetail = dataObj.asDetail;
    $scope.SaveAssociationDetails = function (asDetail) {
        if ($scope.IsfrmAssociationDetailsValid) {
            $uibModalInstance.close(asDetail);
        } else {
            growl.error('Please enter all mandatory fields');
        }
        
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };


    $scope.IsfrmAssociationDetailsValid = false;
    $scope.$watch('frmAssociationDetails.$valid', function (isValid) {
        $scope.IsfrmAssociationDetailsValid = isValid;
    });

    $scope.CompanyNameResults = function (text) {        
        return CompanyService.GetCompanyAutoComplete(text).then(function (res) {
            return limitToFilter(res.data, 15);
        }, function (err) { });
    };

    $scope.CompanySelected = function (obj) {        
        $scope.asDetail.ROCNumber = obj.RegNo;
        $scope.asDetail.MerchantCode = obj.Value;
    };

    $scope.RocResults = function (text) {
        return CompanyService.GetRocNoAutoComplete(text).then(function (res) {
            return limitToFilter(res.data, 15);
        }, function (err) { });
    };

    $scope.RocSelected = function (obj) {
        $scope.asDetail.CompanyName = obj.Text;
    };
}]);
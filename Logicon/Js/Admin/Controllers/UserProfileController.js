﻿app.controller('UserProfileController', ['$scope', 'UserProfileService', 'SecurableService', '$routeParams', 'growl', '$location', '$q', '$filter', 'UtilityFunc', function ($scope, UserProfileService, SecurableService, $routeParams, growl, $location, $q, $filter, UtilityFunc) {
    $scope.showLoading = true;
    $scope.isFrmUserProfileValid = false;
    $scope.$watch('frmUserProfile.$valid', function (isValid) {
        $scope.isFrmUserProfileValid = isValid;
    });       

    $scope.SaveUser = function (user) {        
        if ($scope.isFrmUserProfileValid) {
            $scope.showLoading = true;
            user.UserID = user.EmailID;
            UserProfileService.SaveUserProfile(user).then(function (d) {
                $scope.showLoading = false;
                growl.success('User Saved Successfully..', {});
                $location.path('/admin/userlist');
            }, function (err) { });
        } else {
            growl.error('Please enter all mandatory fields', {});
        }        
    };

    $scope.checkUser = function () {
        $scope.showLoading = true;
        UserProfileService.getUser({
            EmailID: $scope.user.EmailID
        }).then(function (d) {
            $scope.showLoading = false;
            if (d.data != null) {                
                $scope.user = d.data;
            }
        }, function (err) { });
    };

    $scope.back = function () {
        $location.path('/admin/userlist')
    };

    var userID = $routeParams.userID;
    var lookupDataPromise = UserProfileService.getLookupData();
    var securableItemsPromise = SecurableService.GetSecurables(userID);

    $q.all([lookupDataPromise, securableItemsPromise]).then(function (d) {
        $scope.showLoading = false;
        $scope.lookUpData = d[0].data;
        $scope.securableItems = d[1].data;
    }, function (err) { });

    $scope.SaveRights = function () {
        $scope.showLoading = true;
        var rightsArray = $filter('filter')($scope.securableItems, { IsChecked: true });        
        var rightsArrDTO = new Array();
        angular.forEach(rightsArray, function (item, index) {
            var obj = {
                UserID: $scope.user.EmailID,
                SecurableItem: item.SecurableItem,
                LinkGroup: item.LinkGroup,
                LinkID: item.LinkID
            };

            rightsArrDTO.push(obj);
        });
        var saveUserRightsDTO = {
            userRightsList: rightsArrDTO,
            userID: $scope.user.EmailID
        };
        
        SecurableService.SaveUserRights(saveUserRightsDTO).then(function (d) {
            if (UtilityFunc.UserID() == $scope.user.EmailID) {                
                SecurableService.GetUserRights().then(function (d) {
                    $scope.showLoading = false;
                    growl.success('User Rights Saved Successfully..!', {});
                    sessionStorage.setItem('SsnUserRights', JSON.stringify(d.data));
                }, function (err) { });
            } else {
                $scope.showLoading = false;
                growl.success('User Rights Saved Successfully..!', {});
            }
        }, function (err) { });       
        
    };
    
    var userID = $routeParams.userID;    
    if (!angular.isUndefined(userID) && userID != 'NEW') {        
        UserProfileService.getUser({
            EmailID: userID
        }).then(function (d) {            
            $scope.user = d.data;
        }, function (err) { });
    }
}]);

//app.service('SecurableService', ['$http', '$q', 'Utility', function ($http, $q, Utility) {
//    this.GetSecurables = function (userID) {
//        var deferred = $q.defer();
//        $http.post(Utility.ServiceUrl + '/security/securables/list', JSON.stringify({ EmailID: userID })).then(function (res) {
//            deferred.resolve(res);
//        }, function (err) {
//            deferred.reject(err);
//        });
//        return deferred.promise;
//    };

//    this.SaveUserRights = function (obj) {        
//        var deferred = $q.defer();
//        $http.post(Utility.ServiceUrl + '/security/securables/saverights', JSON.stringify(obj)).then(function (res) {
//            deferred.resolve(res);
//        }, function (err) {
//            deferred.reject(err);
//        });
//        return deferred.promise;
//    };

//    this.GetUserRights = function () {
//        var deferred = $q.defer();
//        $http.get(Utility.ServiceUrl + '/security/securables/userrights').then(function (res) {
//            deferred.resolve(res);
//        }, function (err) {
//            deferred.reject(err);
//        });
//        return deferred.promise;
//    };
//}]);


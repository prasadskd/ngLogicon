﻿app.controller('CollectionAdvice', ['$scope', '$uibModal', 'growl', 'Utility', function ($scope, $uibModal, growl, Utility) {
    $scope.AddCollectionAdvice = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'Js/Haulage/Templates/collectionadvice/add-collectionadvice.html?v=' + Utility.Version,
            controller: 'editcollectionadvice',
            size: 'lg'

        });

        modalInstance.result.then(function () {

        }, function (err) {
            // growl.error(err.statusText, {});
        });
    };
}]);
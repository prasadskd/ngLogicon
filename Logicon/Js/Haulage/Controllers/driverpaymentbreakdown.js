﻿app.controller('driverpaymentbreakdown', ['$scope', '$uibModal', 'growl', 'Utility', function ($scope, $uibModal, growl, Utility) {
    $scope.AddBreakdown = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'Js/Haulage/Templates/Driverpaymentbreakdown/add-driverbreakdown.html?v=' + Utility.Version,
            controller: 'editdriverpaymentbreakdown',
            windowClass: 'app-modal-window2'

        });

        modalInstance.result.then(function () {

        }, function (err) {
            // growl.error(err.statusText, {});
        });
    };
}]);
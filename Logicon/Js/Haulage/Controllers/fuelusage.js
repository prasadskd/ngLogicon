﻿app.controller('fuelusage', ['$scope', '$uibModal', 'growl', 'Utility', function ($scope, $uibModal, growl, Utility) {
    $scope.AddFuelUsage = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'Js/Haulage/Templates/fuelusage/add-fuelusage.html?v=' + Utility.Version,
            controller: 'editfuelusage',
            windowClass: 'app-modal-window2'

        });

        modalInstance.result.then(function () {

        }, function (err) {
            // growl.error(err.statusText, {});
        });
    };
}]);
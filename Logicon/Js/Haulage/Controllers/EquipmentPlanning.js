﻿app.controller('EquipmentPlanning', ['$scope', '$uibModal', 'growl', 'Utility', function ($scope, $uibModal, growl, Utility ) {
    $scope.AddEquipment = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'Js/Haulage/Templates/EquipmentPlanning/add-equipment.html?v=' + Utility.Version,
            controller: 'EditEquipmentController',
            windowClass: 'app-modal-window'
            
        });

        modalInstance.result.then(function () {
            
        }, function (err) {
           // growl.error(err.statusText, {});
        });
    };
}]);
﻿app.controller('EquipmentCostEntry', ['$scope', '$uibModal', 'growl', 'Utility', function ($scope, $uibModal, growl, Utility) {
    $scope.AddEquipmentCostEntry = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'Js/Haulage/Templates/EquipmentCostEntry/add-equipmentcostentry.html?v=' + Utility.Version,
            controller: 'EditEquipmentCostEntry',
            size: 'lg'

        });

        modalInstance.result.then(function () {

        }, function (err) {
            // growl.error(err.statusText, {});
        });
    };
}]);
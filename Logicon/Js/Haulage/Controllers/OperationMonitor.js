﻿app.controller('OperationMonitor', ['$scope', '$uibModal', 'growl', 'Utility', function ($scope, $uibModal, growl, Utility) {
    $scope.AddOperationMonitor = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'Js/Haulage/Templates/OperationMonitor/add-operation-monitor.html?v=' + Utility.Version,
            controller: 'EditOperationMonitor',
            windowClass: 'app-modal-window'

        });

        modalInstance.result.then(function () {

        }, function (err) {
            // growl.error(err.statusText, {});
        });
    };
}]);
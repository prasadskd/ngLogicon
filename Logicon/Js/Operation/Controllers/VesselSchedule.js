﻿app.controller('VesselScheduleController', ['$scope', '$uibModal', 'alert', 'VesselScheduleService', 'Utility', 'UtilityFunc', function ($scope, $uibModal, alert, VesselScheduleService, Utility, UtilityFunc) {

    $scope.dateFormat = UtilityFunc.DateFormat();
    $scope.dateTimeFormat = UtilityFunc.DateTimeFormat();
    $scope.dateTimeFormat12 = UtilityFunc.DateTimeFormat12();

    $scope.addSchedule = function (vesselScheduleID) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'Js/Operation/Templates/VesselSchedule/vessel-schedule.html?v=' + Utility.Version,
            controller: 'addEditVesselScheduleCntrl',
            //size: 'lg',
            windowClass: 'app-modal-window2',
            resolve: {
                vesselScheduleID: function () {
                    return vesselScheduleID;
                }
            }
        });

        modalInstance.result.then(function () {
            var dt = moment(vm.viewDate).format('MM-01-YYYY');
            var m = vm.calendarView;
            $scope.GetVesselScheduleListByDate(dt);
        }, function () {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    };
    

    /* calendar events */
    var vm = this;

    //These variables MUST be set as a minimum for the calendar to work
    vm.calendarView = 'month';
    vm.viewDate = new Date();
    vm.viewChangeEnabled = false;
    /*
    vm.events = [
      {
          title: 'An event',
          type: 'warning',
          startsAt: moment().startOf('week').subtract(2, 'days').add(8, 'hours').toDate(),
          endsAt: moment().startOf('week').add(1, 'week').add(9, 'hours').toDate(),
          draggable: false,
          resizable: false
      }, 
      {
          title: '<i class="fa fa-asterisk" aria-hidden="true"></i><span class="text-primary">Another event</span>, with a <i>html</i> title',
          type: 'info',
          startsAt: moment().subtract(1, 'day').toDate(),
          endsAt: moment().add(5, 'days').toDate(),
          draggable: true,
          resizable: true
      }, {
          title: 'This is a really long event title that occurs on every year',
          type: 'important',
          startsAt: moment().startOf('day').add(7, 'hours').toDate(),
          endsAt: moment().startOf('day').add(19, 'hours').toDate(),
          recursOn: 'year',
          draggable: true,
          resizable: true
      }
    ];
    */
    vm.isCellOpen = false;

    vm.eventClicked = function (event) {
        debugger;
        //console.log('click');
        //alert.show('Clicked', event);
        //$scope.addSchedule();
    };

    vm.eventEdited = function (event) {
        debugger;
        //console.log('Edited');
        //alert.show('Edited', event);
        //$scope.addSchedule(event.vesselScheduleID);
    };

    vm.eventDeleted = function (event) {
        debugger;
        //console.log('Edited');
        //alert.show('Edited', event);
        //$scope.addSchedule();
    };

    vm.eventTimesChanged = function (event) {
        debugger;
        //console.log('Edited');
        //alert.show('Edited', event);
        //$scope.addSchedule();
    };



    vm.toggle = function ($event, field, event) {
        $event.preventDefault();
        $event.stopPropagation();
        event[field] = !event[field];
    };

    vm.DoubleClick = function (event) {
        //debugger;
        //$scope.addSchedule();
    }

    vm.MonthChanged = function () {
        var dt = moment(vm.viewDate).format('MM-01-YYYY');
        var m = vm.calendarView;
        $scope.GetVesselScheduleListByDate(dt);
    };

    vm.viewChangeClicked = function (nextView) {
        if (nextView != 'month') {
            return false;
        }
    };
    /* calendar events */



    $scope.GetVesselScheduleListByDate = function (dt) {
        VesselScheduleService.GetVesselScheduleList(moment(dt).format('MM-01-YYYY')).then(function (d) {
            for (var i = 0; i < d.data.length; i++) {
                d.data[i].startsAt = new Date(d.data[i].startsAt);
                d.data[i].endsAt = new Date(d.data[i].endsAt);
                d.data[i].type = 'info';
            }
            debugger;
            vm.events = d.data;
        }, function () { });
    };
    var dt = new Date();
    $scope.GetVesselScheduleListByDate(dt);
}]);



app.factory('alert', function ($uibModal) {

    function show(action, event) {
        return $uibModal.open({
            templateUrl: 'Js/Operation/VesselSchedule/Templates/vessel-schedule.html?v=' + Utility.Version,
            controller: 'addEditVesselScheduleCntrl'
        });
    }

    return {
        show: show
    };

});



//http://mattlewis92.github.io/angular-bootstrap-calendar/#?example=cell-is-open
﻿app.controller('OrderEntryController', ['$scope', '$uibModal', 'OrderEntryService', 'VesselScheduleService', 'JobCategoryService', 'Utility', 'limitToFilter', '$http', '$routeParams', 'growl', 'UtilityFunc', '$window', '$route', 'AddressService', 'PortAreaService', '$timeout', '$location', 'MerchantProfileService', 'CountryService',
 function ($scope, $uibModal, OrderEntryService, VesselScheduleService, JobCategoryService, Utility, limitToFilter, $http, $routeParams, growl, UtilityFunc, $window, $route, AddressService, PortAreaService, $timeout, $location, MerchantProfileService, CountryService) {

     var defaultCurrency = UtilityFunc.DefaultCurrency();
     $scope.dateFormat = UtilityFunc.DateFormat();
     $scope.dateTimeFormat = UtilityFunc.DateTimeFormat();

     $scope.isForwardingAgent = false;
     $scope.isShippingAgent = false;
     $scope.showLoading = true;
     $scope.isFrmOrderEntryValid = false;
     $scope.IsNew = false;
     $scope.docList = '';
     $scope.$watch('frmOrderEntry.$valid', function (isValid) {
         $scope.isFrmOrderEntryValid = isValid;
     });
     $scope.oe = {
         orderContainerList: new Array(),
         orderCargoList: new Array(),
         orderText: {},
         InvoiceCurrency: defaultCurrency,
         CargoCurrencyCode: defaultCurrency
     };
     var d = new Date();
     var month = d.getMonth() + 1;
     if (month < 10) {
         month = "0" + month;
     };

     $scope.oe.OrderDate = moment();
     $scope.tabs = [
         { title: 'General', content: 'Js/Operation/Templates/OrderEntry/general.html?v=' + Utility.Version, active: true, disabled: false, visible: true, eInsurance: false },
         { title: 'Transport', content: 'Js/Operation/Templates/OrderEntry/transport.html?v=' + Utility.Version, active: false, disabled: false, visible: false, eInsurance: false },
         { title: 'Container', content: 'Js/Operation/Templates/OrderEntry/container.html?v=' + Utility.Version, active: false, disabled: false, visible: false, eInsurance: false },
         { title: 'Cargo', content: 'Js/Operation/Templates/OrderEntry/cargo.html?v=' + Utility.Version, active: false, disabled: true, visible: true, eInsurance: false },
         //{ title: 'Marks & Numbers', content: 'Js/Operation/Templates/OrderEntry/marks.html?v=' + Utility.Version, active: false, disabled: false, visible: true, eInsurance: false },
         { title: 'Services', content: 'Js/Operation/Templates/OrderEntry/services.html?v=' + Utility.Version, active: false, disabled: false, visible: false, eInsurance: false },
         { title: 'Order Activity', content: 'Js/Operation/Templates/OrderEntry/orderactivity.html?v=' + Utility.Version, active: false, disabled: false, visible: false, eInsurance: false },
         { title: 'e Insurance', content: 'Js/Operation/Templates/OrderEntry/e-insurance.html?v=' + Utility.Version, active: false, disabled: false, visible: false, eInsurance: false },
         { title: 'Documents', content: 'Js/Operation/Templates/OrderEntry/documents.html?v=' + Utility.Version, active: false, disabled: false, visible: true, eInsurance: false }
     ];
     $scope.companyType = UtilityFunc.CompanyType();

     /*Container code*/
     var containerIndex = -1;
     $scope.AddContainer = function (index) {
         containerIndex = index;
         $scope.modalInstance = $uibModal.open({
             animation: true,
             templateUrl: 'Js/Operation/Templates/OrderEntry/add-container.html?v=' + Utility.Version,
             windowClass: 'app-modal-window',
             controller: 'AddOrderEntryContainer',
             resolve: {
                 dataObj: function () {
                     return {
                         containerItem: (containerIndex == -1 ? {} : $scope.oe.orderContainerList[containerIndex]),
                         JobType: $scope.oe.JobType
                     };
                 }
             }
         });

         $scope.modalInstance.result.then(function (con) {
             if (containerIndex != -1) {
                 $scope.oe.orderContainerList[containerIndex] = con;
             }
             else {
                 if ($scope.oe.orderContainerList != null) {
                     $scope.oe.orderContainerList.push(con);
                 }
                 else {
                     $scope.oe.orderContainerList = new Array();
                     $scope.oe.orderContainerList.push(con);
                 }
             }
             $scope.generate(true, false);
         }, function () {

         });
     };

     $scope.DeleteContainer = function (index) {
         $scope.oe.orderContainerList = UtilityFunc.removeArrayElementByKey($scope.oe.orderContainerList, 'Index', index);
     }

     $scope.CloneConatiner = function () {
         var tempArray = new Array();
         angular.forEach($scope.oe.orderContainerList, function (item, index) {
             if (item.chk) {
                 tempArray.push(index);
             }
         });

         angular.forEach(tempArray, function (item, index) {
             var tempObj = {};
             angular.copy($scope.oe.orderContainerList[item], tempObj);
             $scope.oe.orderContainerList.push(tempObj);
         });

         angular.forEach($scope.oe.orderContainerList, function (item, index) {
             item.Index = index;
             item.chk = false;
         });
     };

     $scope.RemoveContainers = function () {
         var tempArray = new Array();
         angular.forEach($scope.oe.orderContainerList, function (item, index) {
             if (item.chk) {
                 tempArray.push(index);
             }
         });

         angular.forEach(tempArray, function (item, index) {
             $scope.oe.orderContainerList = UtilityFunc.removeArrayElementByKey($scope.oe.orderContainerList, 'Index', item);
         });

         angular.forEach($scope.oe.orderContainerList, function (item, index) {
             item.Index = index;
             item.chk = false;
         });
     };
     $scope.hdrChkContainer = false;
     $scope.toggleChksContainer = function () {
         $scope.hdrChkContainer = !$scope.hdrChkContainer;
         angular.forEach($scope.oe.orderContainerList, function (item, index) {
             item.chk = $scope.hdrChkContainer;
         });
     };

     $scope.AddContainers = function () {
         var tempArray = new Array();

         angular.forEach($scope.oe.orderContainerList, function (item, index) {
             if (item.chk) {
                 tempArray.push(index);
             }
         });

         if (tempArray.length > 1) {
             growl.error('please select one container to proceed', {});
         } else {
             var count = parseInt($window.prompt('Please enter the count ?'));
             for (var i = 0; i < count; i++) {
                 var tempObj = {};
                 angular.copy($scope.oe.orderContainerList[tempArray[0]], tempObj);
                 $scope.oe.orderContainerList.push(tempObj);
             }
         }
     };

     $scope.ContainerRowClick = function (index) {
         angular.forEach($scope.oe.orderContainerList, function (item, index) {
             item.activeCls = false;
         });
         $scope.oe.orderContainerList[index].activeCls = true;
     };

     $scope.UpdateContainers = function () {
         var tempArray = new Array();
         var selectedIndex = -1;
         angular.forEach($scope.oe.orderContainerList, function (item, index) {
             if (item.activeCls) {
                 selectedIndex = index;
             }

             if (item.chk) {
                 tempArray.push(index);
             }
         });

         angular.forEach(tempArray, function (item, index) {
             $scope.oe.orderContainerList[item].SpecialHandling = $scope.oe.orderContainerList[selectedIndex].SpecialHandling;
             $scope.oe.orderContainerList[item].ContainerGrade = $scope.oe.orderContainerList[selectedIndex].ContainerGrade;
             $scope.oe.orderContainerList[item].SealNo = $scope.oe.orderContainerList[selectedIndex].SealNo;
             $scope.oe.orderContainerList[item].Temprature = $scope.oe.orderContainerList[selectedIndex].Temprature;
             $scope.oe.orderContainerList[item].TemperatureMode = $scope.oe.orderContainerList[selectedIndex].TemperatureMode;
             $scope.oe.orderContainerList[item].TrailerType = $scope.oe.orderContainerList[selectedIndex].TrailerType;
             $scope.oe.orderContainerList[item].TrailerTypeDescription = $scope.oe.orderContainerList[selectedIndex].TrailerTypeDescription;
             $scope.oe.orderContainerList[item].GrossWeight = $scope.oe.orderContainerList[selectedIndex].GrossWeight;
             $scope.oe.orderContainerList[item].CargoType = $scope.oe.orderContainerList[selectedIndex].CargoType;
             $scope.oe.orderContainerList[item].CargoTypeDescription = $scope.oe.orderContainerList[selectedIndex].CargoTypeDescription;
             $scope.oe.orderContainerList[item].Volume = $scope.oe.orderContainerList[selectedIndex].Volume;
             $scope.oe.orderContainerList[item].IMOCode = $scope.oe.orderContainerList[selectedIndex].IMOCode;
             $scope.oe.orderContainerList[item].UNNo = $scope.oe.orderContainerList[selectedIndex].UNNo;
             $scope.oe.orderContainerList[item].CargoHandling = $scope.oe.orderContainerList[selectedIndex].CargoHandling;
             $scope.oe.orderContainerList[item].RequiredDate = $scope.oe.orderContainerList[selectedIndex].RequiredDate;
             $scope.oe.orderContainerList[item].DehireDate = $scope.oe.orderContainerList[selectedIndex].DehireDate;
             $scope.oe.orderContainerList[item].ContainerRef = $scope.oe.orderContainerList[selectedIndex].ContainerRef;
             $scope.oe.orderContainerList[item].Remarks = $scope.oe.orderContainerList[selectedIndex].Remarks;
             $scope.oe.orderContainerList[item].Dimension = $scope.oe.orderContainerList[selectedIndex].Dimension;
             $scope.oe.orderContainerList[item].chk = false;
         });

         $scope.oe.orderContainerList[selectedIndex].activeCls = false;
     };

     $scope.ClearSelection = function () {
         angular.forEach($scope.oe.orderContainerList, function (item, index) {
             item.chk = false;
             item.activeCls = false;
         });
     };
     /*Container code*/
     /*transport code*/
     var transIndex = -1;
     $scope.AddTransport = function (index) {
         transIndex = index;
         $scope.modalInstance = $uibModal.open({
             animation: true,
             templateUrl: 'Js/Operation/Templates/OrderEntry/add-transport.html?v=' + Utility.Version,
             controller: 'AddOrderEntryTransport',
             windowClass: 'app-modal-window',
             resolve: {
                 dataObj: function () {
                     return {
                         transItem: (transIndex == -1 ? {} : $scope.oe.orderContainerList[transIndex])
                     };
                 }
             }
         });

         $scope.modalInstance.result.then(function (trans) {
             if (transIndex != -1) {
                 $scope.oe.orderContainerList[transIndex] = trans;
             }
             else {
                 if ($scope.oe.orderContainerList != null) {
                     $scope.oe.orderContainerList.push(trans);
                 }
                 else {
                     $scope.oe.orderContainerList = new Array();
                     $scope.oe.orderContainerList.push(trans);
                 }
             }

         }, function () {

         });
     };

     $scope.DeleteTransport = function (index) {
         $scope.oe.orderContainerList = UtilityFunc.removeArrayElementByKey($scope.oe.orderContainerList, 'Index', index);
     }

     $scope.CloneTransport = function () {
         var tempArray = new Array();
         angular.forEach($scope.oe.orderContainerList, function (item, index) {
             if (item.chk) {
                 tempArray.push(index);
             }
         });

         angular.forEach(tempArray, function (item, index) {
             var tempObj = {};
             angular.copy($scope.oe.orderContainerList[item], tempObj);
             $scope.oe.orderContainerList.push(tempObj);
         });

         angular.forEach($scope.oe.orderContainerList, function (item, index) {
             item.Index = index;
             item.chk = false;
         });
     };

     $scope.RemoveTransport = function () {
         var tempArray = new Array();
         angular.forEach($scope.oe.orderContainerList, function (item, index) {
             if (item.chk) {
                 tempArray.push(index);
             }
         });

         angular.forEach(tempArray, function (item, index) {
             $scope.oe.orderContainerList = UtilityFunc.removeArrayElementByKey($scope.oe.orderContainerList, 'Index', item);
         });

         angular.forEach($scope.oe.orderContainerList, function (item, index) {
             item.Index = index;
             item.chk = false;
         });
     };
     $scope.hdrChkTransport = false;
     $scope.toggleChksTransport = function () {
         $scope.hdrChkTransport = !$scope.hdrChkTransport;
         angular.forEach($scope.oe.orderContainerList, function (item, index) {
             item.chk = $scope.hdrChkTransport;
         });
     };
     /*Transport code*/
     /* Cargo Code */

     var cargoIndex = -1;
     $scope.AddCargo = function (index) {
         cargoIndex = index;
         $scope.modalInstance = $uibModal.open({
             animation: true,
             templateUrl: 'Js/Operation/Templates/OrderEntry/add-cargo.html?v=' + Utility.Version,
             windowClass: 'app-modal-window2',
             controller: 'AddOrderEntryCargo',
             resolve: {
                 dataObj: function () {
                     return {
                         FreightMode: $scope.TempFreightMode,
                         TransportType: $scope.oe.TransportType,
                         List: $scope.oe.orderContainerList,
                         uomList: $scope.lookupData.uomList,
                         stockRoomList: $scope.lookupData.stockRoomList,
                         currencyList: $scope.lookupData.currencyList,
                         cargoItem: (cargoIndex == -1 ? {} : $scope.oe.orderCargoList[cargoIndex])
                     };
                 }
             }
         });

         $scope.modalInstance.result.then(function (cargo) {
             if (cargoIndex != -1) {
                 $scope.oe.orderCargoList[cargoIndex] = cargo;
             }
             else {
                 if ($scope.oe.orderCargoList != null) {
                     $scope.oe.orderCargoList.push(cargo);
                 }
                 else {
                     $scope.oe.orderCargoList = new Array();
                     $scope.oe.orderCargoList.push(cargo);
                 }
             }
             $scope.generate(false, true);
         }, function () {

         });
     };

     $scope.DeleteCargo = function (index) {
         $scope.oe.orderCargoList = UtilityFunc.removeArrayElementByKey($scope.oe.orderCargoList, 'Index', index);
     };

     $scope.CloneCargo = function () {
         var tempArray = new Array();
         angular.forEach($scope.oe.orderCargoList, function (item, index) {
             if (item.chk) {
                 tempArray.push(index);
             }
         });

         angular.forEach(tempArray, function (item, index) {
             var tempObj = {};
             angular.copy($scope.oe.orderCargoList[item], tempObj);
             $scope.oe.orderCargoList.push(tempObj);
         });

         angular.forEach($scope.oe.orderCargoList, function (item, index) {
             item.Index = index;
             item.chk = false;
         });
     };

     $scope.RemoveCargo = function () {
         var tempArray = new Array();
         angular.forEach($scope.oe.orderCargoList, function (item, index) {
             if (item.chk) {
                 tempArray.push(index);
             }
         });

         angular.forEach(tempArray, function (item, index) {
             $scope.oe.orderCargoList = UtilityFunc.removeArrayElementByKey($scope.oe.orderCargoList, 'Index', item);
         });

         angular.forEach($scope.oe.orderCargoList, function (item, index) {
             item.Index = index;
             item.chk = false;
         });
     };
     $scope.hdrChkCargo = false;
     $scope.toggleChksCargo = function () {
         $scope.hdrChkCargo = !$scope.hdrChkCargo;
         angular.forEach($scope.oe.orderCargoList, function (item, index) {
             item.chk = $scope.hdrChkCargo;
         });
     };
     /* Cargo Code */
     $scope.cancel = function () {
         $scope.modalInstance.dismiss();
     };

     $scope.doSomething = function () {
         console.log("Do Something");
     };

     $scope.imagesArr = new Array();

     $scope.ValidatePorts = function () {
         if ($scope.oe.TransportType == 1021) {
             if ($scope.oe.PortOfLoading == $scope.oe.PortOfDischarge) {
                 growl.error('Loading Port and Discharge Port can not be the same', {});
                 return false;
             }
             else if ($scope.oe.PortOfLoading == $scope.oe.TranshipmentPort) {
                 growl.error('Loading Port and Transhipment Port can not be the same', {});
                 return false;
             }
             else if ($scope.oe.PortOfDischarge == $scope.oe.TranshipmentPort) {
                 growl.error('Transhipment Port and Discharge Port can not be the same', {});
                 return false;
             }
             else
                 return true;
         }
         else
             return true;
     };

     $scope.SaveOrderEntry = function (oe) {
         $scope.submitted = true;
         if ($scope.isFrmOrderEntryValid) {
             if ($scope.ValidatePorts()) {
                 var data = new FormData();
                 for (var i in $scope.files) {
                     data.append("uploadedFile", $scope.files[i]);
                 }

                 OrderEntryService.SaveOrderEntry(oe).then(function (d) {
                     $scope.showLoading = true;
                     $scope.oe.OrderNo = d.data.OrderNo;
                     OrderEntryService.uploadFiles(data, d.data.OrderNo).then(function (d) {
                         $scope.showLoading = false;
                         growl.success('Order Saved Successfully', {});
                         //$route.reload('operation/orderentry/' + $scope.oe.OrderNo);
                         $scope.orderNoSelected({ Value: $scope.oe.OrderNo });
                     }, function (err) { });
                 }, function (err) { });
             }
         } else {
             growl.error('please enter all mandatory fields',


                 {});
         }
     };

     $scope.DeleteOrderEntry = function () {
         if ($window.confirm('Are you sure, you want to delete \'' + $scope.oe.OrderNo + '\' ?')) {
             OrderEntryService.deleteOrderEntry($scope.oe.OrderNo).then(function (d) {
                 if (d.data) {
                     growl.success('Order Deleted Successfully', {});
                     $location.path('/operation/orderentrylist');
                 }
             });
         }
     };

     $scope.back = function () {
         $location.path('/operation/orderentrylist');
     };

     $scope.clear = function () {
         $route.reload('/operation/orderentry');
     };

     /* dt pic config */
     $scope.popup = {
         OrderDate: false
     };

     $scope.open = function (type) {
         $scope.popup[type] = true;
     };
     /* dt pic */

     OrderEntryService.GetLookupData().then(function (d) {
         $scope.showLoading = false;
         $scope.lookupData = d.data;
     }, function (err) { });

     OrderEntryService.GetTerminalList().then(function (d) {
         debugger;
         $scope.terminalList = d.data.terminalList;
     }, function (err) { });

     /*
     $scope.MerchantResults = function ($query) {        
         return $http.get(Utility.ServiceUrl + '/master/MerchantProfile/search/' + $query + '/billingCustomer').then(function (response) {
             return limitToFilter(response.data, 15);
         });
     };
 
     $scope.TerminalResults = function ($query) {
         return $http.get(Utility.ServiceUrl + '/master/MerchantProfile/search/' + $query + '/terminal').then(function (response) {
             return limitToFilter(response.data, 15);
         });
     };
 
     $scope.ForeignAgentsResults = function (text) {
         return $http.get(Utility.ServiceUrl + '/master/MerchantProfile/search/' + text + '/liner').then(function (response) {
             return limitToFilter(response.data, 15);
         });
     };
     */
     $scope.GenericMerchantResults = function (text, filter, addresstype) {
         return $http.get(Utility.ServiceUrl + '/master/MerchantProfile/search/' + text + '/' + filter).then(function (response) {
             if (response.data.length == 0)
                 $scope.oe[addresstype] = '';
             return limitToFilter(response.data, 15);
         });
     };

     $scope.OrderResults = function (text) {
         return OrderEntryService.SearchOrder(text).then(function (res) {
             return limitToFilter(res.data, 15);
         }, function (err) { });
     };

     $scope.VoyageResults = function (text) {
         if ($scope.oe.JobType == 1060 || $scope.oe.JobType == 1061) {
             return VesselScheduleService.VoyageSearch(text, $scope.oe.JobType).then(function (d) {
                 debugger;
                 return limitToFilter(d.data, 15);
             }, function (err) { });
         } else {
             return limitToFilter({}, 15);
         }

     };


     $scope.VoyageSelected = function (obj) {
         if ($scope.oe.JobType == 1060) {
             $scope.oe.YardCutOffTime = obj.ImpAvaliableDate;
             $scope.oe.Voyageno = obj.VoyageNoInWard;
         } else if ($scope.oe.JobType == 1061) {
             $scope.oe.YardCutOffTime = obj.ExpAvailableDate;
             $scope.oe.Voyageno = obj.VoyageNoOutWard;
         }
         $scope.oe.ETA = obj.ETA;
         $scope.oe.ETD = obj.ETD;
         $scope.oe.VesselName = obj.VesselName;
         $scope.oe.SCNNo = obj.ShipCallNo;
         $scope.oe.VesselID = obj.VesselID;
         $scope.oe.CallSignNo = obj.CallSignNo;

     };

     $scope.orderNoSelected = function (item) {
         $scope.showLoading = true;
         OrderEntryService.GetOrderEntryByNo(item.Value).then(function (d) {
             debugger;
             $scope.IsNew = false;
             $scope.showLoading = false;
             $scope.oe = d.data;
             $scope.BindAddress(d.data, 'Customer');
             $scope.BindAddress(d.data, 'NotifyParty');
             $scope.BindAddress(d.data, 'Consignee');
             $scope.BindAddress(d.data, 'Shipper');
             $scope.BindAddress(d.data, 'FwdAgent');
             $scope.BindAddress(d.data, 'ShippingAgent');

             //console.log(JSON.stringify(d.data));
             $scope.jobTypeChanged($scope.oe.JobType);
             $scope.orderCategoryChanged();

             $scope.tabs[6].visible = true;
             //$scope.tabs[7].visible = true;

             if (!angular.isUndefined($scope.oe)) {
                 if ($scope.oe.ETA == null) {
                     $scope.oe.ETA = undefined;
                 }
                 if ($scope.oe.ETD == null) {
                     $scope.oe.ETD = undefined;
                 }
                 if ($scope.oe.YardCutOffTime == null) {
                     $scope.oe.YardCutOffTime = undefined;
                 }

                 if ($scope.oe.SalesOrderDate == null) {
                     $scope.oe.SalesOrderDate = undefined;
                 }

             }
         }, function (err) { });
     };

     var orderNo = $routeParams.orderno;
     var branchId = $routeParams.branchid;

     if (!angular.isUndefined(orderNo) && !angular.isUndefined(branchId)) {
         $scope.showLoading = true;
         OrderEntryService.GetWebOrder(orderNo, branchId).then(function (d) {
             $scope.showLoading = false;
             $scope.oe = d.data;
             $scope.jobTypeChanged($scope.oe.JobType);
             $scope.orderCategoryChanged();

             $scope.tabs[6].visible = true;

         }, function (err) { });
     }
     $scope.CustomerSelected = function (item, type, addresstype, ROCNo, addressID) {
         $scope.showLoading = true;
         $scope.oe[type] = item.Value;
         var html = '';
         $scope.oe[addresstype] = '';
         $scope.oe[ROCNo] = item.RegNo;
         if (type == 'FwdAgent') {
             $scope.isForwardingAgent = true;
         }
         if (type == 'ShippingAgent') {
             $scope.isShippingAgent = true;
         }
         AddressService.GetAddress(item.Value).then(function (d) {
             if (d.data != null) {
                 $scope.oe[addressID] = d.data.AddressId;
                 if (!angular.isUndefined(d.data.Address1))
                     html += d.data.Address1 + '<br/>';
                 if (!angular.isUndefined(d.data.Address2))
                     html += d.data.Address2 + '<br/>';
                 if (!angular.isUndefined(d.data.Address3))
                     html += d.data.Address3 + '<br/>';
                 if (!angular.isUndefined(d.data.City))
                     html += d.data.City + '<br/>';
                 if (!angular.isUndefined(d.data.State))
                     html += d.data.State + '<br/>';
                 if (!angular.isUndefined(d.data.CountryCode))
                     html += d.data.CountryCode + '<br/>';
                 if (!angular.isUndefined(d.data.ZipCode))
                     html += d.data.ZipCode;
             }

             $scope.oe[addresstype] = html;
             $scope.showLoading = false;
         }, function (err) { });

     };

     $scope.portResults = function (text) {
         return PortAreaService.PortAutoComplete(text).then(function (d) {
             return limitToFilter(d.data, 15);
         }, function (err) { });
     };

     $scope.PortSelected = function (item, type) {
         $scope.oe[type] = item.PortCode;
     };

     $scope.VesselNameResults = function (text) {
         return VesselScheduleService.VesselNameResults(text).then(function (d) {
             debugger;
             return limitToFilter(d.data, 15);
         }, function (err) { });
     };

     //$scope.VesselNameResults = function (text) {
     //    return $http.get(Utility.ServiceUrl + '/master/Vessel/search/vesselName/' + text).then(function (response) {
     //        return limitToFilter(response.data, 15);
     //    });
     //};

     $scope.vesselNameClick = function (obj) {
         //$scope.oe.VesselID = obj.Value;
         //$scope.oe.CallSignNo = obj.CallSignNo;


         if ($scope.oe.JobType == 1060) {
             $scope.oe.YardCutOffTime = obj.ImpAvaliableDate;
             $scope.oe.Voyageno = obj.VoyageNoInWard;
         } else if ($scope.oe.JobType == 1061) {
             $scope.oe.YardCutOffTime = obj.ExpAvailableDate;
             $scope.oe.Voyageno = obj.VoyageNoOutWard;
         }
         $scope.oe.ETA = obj.ETA;
         $scope.oe.ETD = obj.ETD;
         $scope.oe.VesselName = obj.VesselName;
         $scope.oe.SCNNo = obj.ShipCallNo;
         $scope.oe.VesselID = obj.VesselID;
         $scope.oe.CallSignNo = obj.CallSignNo;
     };


     //test
     $scope.BLText = 'B/L';
     $scope.jobTypeChanged = function (jobType) {
         debugger;
         if (!angular.isUndefined(jobType)) {
             $scope.showLoading = true;
             $scope.BLText = ($scope.oe.JobType == 1061 ? 'Booking' : 'B/L');
             JobCategoryService.getJobCategoryListByJobType(jobType, $scope.oe.ShipmentType).then(function (d) {
                 $scope.showLoading = false;
                 $scope.lookupData.OrderCategoryList = d.data;
             }, function (err) { })
         }
     };

     $scope.transportTypeChanged = function () {

         if ($scope.oe.TransportType == 1021) {
             $scope.pallet = false;
         }
         else
             $scope.pallet = true;

         if ($scope.oe.TransportType != 1020 && $scope.oe.TransportType != 1024 && $scope.oe.TransportType != 1025) {
             $scope.tabs[2].visible = true;
             $scope.showCoLoader = false;
         } else {
             $scope.tabs[2].visible = false;
             $scope.showCoLoader = true;
         }
     };

     $scope.oe.TransportType = 1021;
     $scope.transportTypeChanged();

     $scope.TempFreightMode = -1;
     $scope.orderCategoryChanged = function () {
         $scope.showLoading = true;
         JobCategoryService.getJobCategoryItem($scope.oe.OrderCategory).then(function (d) {
             $scope.showLoading = false;
             $scope.tabs[2].visible = false;
             $scope.tabs[1].visible = false;
             $scope.TempFreightMode = d.data.jobCategory.FreightMode;
             if (d.data.jobCategory.FreightMode == 1030 && $scope.oe.TransportType != 1020) {
                 $scope.tabs[2].visible = true;
             }
             else if (d.data.jobCategory.FreightMode == 1031) {
                 $scope.tabs[1].visible = true;
             }


             $scope.oe.IsCFS = d.data.jobCategory.IsCFS;
             $scope.oe.IsDepot = d.data.jobCategory.IsDepot;
             $scope.oe.IsForwarding = d.data.jobCategory.IsForwarding;
             $scope.oe.IsFreight = d.data.jobCategory.IsFreight;
             $scope.oe.IsHaulage = d.data.jobCategory.IsHaulage;
             $scope.oe.IsRail = d.data.jobCategory.IsRail;
             $scope.oe.IsTransport = d.data.jobCategory.IsTransport;
             $scope.oe.IsWareHouse = d.data.jobCategory.IsWareHouse;

             //$scope.oe.orderContainerList = new Array();

         }, function (err) { })
     };

     $scope.generate = function (isMarks, isCargo) {
         if (isMarks) {
             var marksNumbers = '';
             marksNumbers += $scope.oe.ConsigneeName + '<br/>';
             marksNumbers += $scope.oe.CustomerRef + '<br/>';

             angular.forEach($scope.oe.orderContainerList, function (item, index) {
                 marksNumbers += item.ContainerNo + '/' + '<br/>'
             });

             $scope.oe.orderText.MarksNumbers = marksNumbers.toUpperCase();
         }

         if (isCargo) {
             var cargoDesc = '';
             cargoDesc += 'SAID TO CONTAIN:-' + '<br/>';

             angular.forEach($scope.oe.orderCargoList, function (item, index) {
                 cargoDesc += '   ' + item.Qty + ' ' + item.UOMDescription + ' ' + item.ProductDescription + '<br/>';
             });
             $scope.oe.orderText.CargoDescription = cargoDesc.toUpperCase();
         }

     };

     $scope.SendToAgent = function () {
         $scope.showLoading = true;
         OrderEntryService.SendToAgent($scope.oe.OrderNo).then(function (d) {
             $scope.showLoading = false;
             $scope.oe.WebOrderStatus = 4301;
         }, function (err) { });
     };

     $scope.ApproveWebOrder = function (flag) {
         OrderEntryService.ApproveWebOrder({
             branchID: $scope.oe.BranchID,
             orderNo: $scope.oe.OrderNo,
             isApproved: flag,
             remarks: ''
         }).then(function (d) {

         }, function (err) { });
     };

     /*
     var orderno = $routeParams.orderno;
     if (orderno != null && orderno != '') {
         OrderEntryService.GetOrderEntryByNo(orderno).then(function (d) {
             d.data.OrderDate = new Date(moment(d.data.OrderDate).format('DD/MM/YYYY'));
             $scope.oe = d.data;
         }, function (err) { });
     }*/

     $scope.GenerateDeclaration = function () {
         OrderEntryService.GenerateDeclaration($scope.oe).then(function (d) {
             growl.success('Declaration ' + d.data + ' Generated', {});
         }, function (err) { });
     };

     $scope.GenerateBookingEntry = function () {
         OrderEntryService.GenerateBookingEntry($scope.oe).then(function (d) {
             growl.success('Booking Posted To Port Successfully.', {});
         }, function (err) { });
     };

     var orderno = $routeParams.orderno;
     debugger;
     if (!angular.isUndefined(orderno)) {
         debugger;
         if (orderno != 'NEW') {
             $scope.showLoading = true;
             OrderEntryService.GetOrderEntryByNo(orderno).then(function (d) {
                 debugger;
                 $scope.showLoading = false;
                 $scope.oe = d.data;
                 $scope.BindAddress(d.data, 'Customer');
                 $scope.BindAddress(d.data, 'NotifyParty');
                 $scope.BindAddress(d.data, 'Consignee');
                 $scope.BindAddress(d.data, 'Shipper');
                 $scope.BindAddress(d.data, 'FwdAgent');
                 $scope.BindAddress(d.data, 'ShippingAgent');
                 debugger;
                 OrderEntryService.GetOrderEntryDocsNo(orderno).then(function (d) {
                     debugger;
                     $scope.docList = d.data;
                 });
                 //console.log(JSON.stringify(d.data));
                 $scope.jobTypeChanged($scope.oe.JobType);
                 $scope.orderCategoryChanged();

                 $scope.tabs[6].visible = true;

                 if (!angular.isUndefined($scope.oe)) {
                     if ($scope.oe.ETA == null) {
                         $scope.oe.ETA = undefined;
                     }
                     if ($scope.oe.ETD == null) {
                         $scope.oe.ETD = undefined;
                     }
                     if ($scope.oe.YardCutOffTime == null) {
                         $scope.oe.YardCutOffTime = undefined;
                     }

                     if ($scope.oe.SalesOrderDate == null) {
                         $scope.oe.SalesOrderDate = undefined;
                     }
                 }
                 $scope.transportTypeChanged();
             }, function (err) { });
         }
         else {
             $scope.IsNew = true;
         }
     }
     else {
         $scope.IsNew = true;
     }

     $scope.BindAddress = function (item, addresstype) {
         $scope.showLoading = true;
         var html = '';
         $scope.oe[addresstype + 'Address'] = '';
         debugger;
         if (item != null) {
             $scope.oe[addresstype + 'AddressID'] = item[addresstype + 'AddressID'];
             if (addresstype == 'FwdAgent') {
                 $scope.isForwardingAgent = true;
             }
             if (addresstype == 'ShippingAgent') {
                 $scope.isShippingAgent = true;
             }

             if (!angular.isUndefined(item[addresstype + 'Address1']))
                 html += item[addresstype + 'Address1'] + '<br/>';
             if (!angular.isUndefined(item[addresstype + 'Address2']))
                 html += item[addresstype + 'Address2'] + '<br/>';
             if (!angular.isUndefined(item[addresstype + 'Address3']))
                 html += item[addresstype + 'Address3'] + '<br/>';
             if (!angular.isUndefined(item[addresstype + 'City']))
                 html += item[addresstype + 'City'] + '<br/>';
             debugger;
             if (!angular.isUndefined(item[addresstype + 'State']))
                 html += item[addresstype + 'State'] + '<br/>';
             if (!angular.isUndefined(item[addresstype + 'CountryCode']))
                 html += item[addresstype + 'CountryCode'] + '<br/>';
             if (!angular.isUndefined(item[addresstype + 'ZipCode']))
                 html += item[addresstype + 'ZipCode'];
         }
         $scope.oe[addresstype + 'Address'] = html.toUpperCase();

     };
     $scope.CalulateLocalAmount = function () {
         // debugger;
         var invoiceAmount = parseFloat($scope.oe.InvoiceAmount);
         var exchangeRate = parseFloat($scope.oe.ExchangeRate);
         if (!isNaN(invoiceAmount) && !isNaN(exchangeRate)) {
             $scope.oe.LocalAmount = (invoiceAmount * exchangeRate).toFixed(4);
         }
         else {
             $scope.oe.LocalAmount = 0;
         }
     };



     var fileType = ['image/jpg', 'image/jpeg', 'image/png', 'image/tif', 'application/pdf'];
     var fileSize = 1024 * 1024 * 5;
     var isValidFileType = false;
     var isValidFileSize = false;
     var fileTotalSize = 0;

     $scope.files = [];
     $scope.getFileDetails = function (e) {
         $scope.$apply(function () {
             // STORE THE FILE OBJECT IN AN ARRAY.
             for (var i = 0; i < e.files.length; i++) {
                 debugger;
                 //var name = e.files[i].name;
                 $scope.files.push(e.files[i])
             }
             //$scope.uploadedDocList = $scope.files
             // $scope.fileValidate($scope.files);
         });
     };

     $scope.fileValidate = function (files) {
         for (var i = 0; i < files.length; i++) {
             fileTotalSize += files[i].size;
             angular.forEach(fileType, function (item, index) {
                 if (item == files[i].type)
                     isValidFileType = true;
             });
         }
         if (fileTotalSize <= fileSize)
             isValidFileSize = true;

         if (!isValidFileType) {
             growl.error('Please upload valid file type', {});
             return false
         }
         else if (!isValidFileSize) {
             growl.error('File size must be not more that 5 MB', {});
             return false;
         }
         return true;

     };

     $scope.DeleteDocument = function (index) {
         $scope.files.splice(index, 1);
     };

     $scope.DeleteImage = function (branchID, orderNo, itemNo) {
         OrderEntryService.DeleteImage(branchID, orderNo, itemNo).then(function (d) {
             OrderEntryService.GetOrderEntryDocsNo(orderNo).then(function (d) {
                 $scope.docList = d.data;
             });
         }, function (err) { });
     };

     var cargoIndex = -1;
     $scope.AddMerchant = function () {
         $scope.modalInstance = $uibModal.open({
             animation: true,
             templateUrl: 'Js/Operation/Templates/OrderEntry/add-merchant.html?v=' + Utility.Version,
             windowClass: 'app-modal-window',
             controller: 'AddMerchant',
             resolve: {
                 dataObj: function () {
                     return {
                         FreightMode: $scope.TempFreightMode,
                         TransportType: $scope.oe.TransportType,
                         List: $scope.oe.orderContainerList,
                         uomList: $scope.lookupData.uomList,
                         stockRoomList: $scope.lookupData.stockRoomList,
                         currencyList: $scope.lookupData.currencyList,
                         cargoItem: (cargoIndex == -1 ? {} : $scope.oe.orderCargoList[cargoIndex])
                     };
                 }
             }
         });

         $scope.modalInstance.result.then(function (cargo) {
             if (cargoIndex != -1) {
                 $scope.oe.orderCargoList[cargoIndex] = cargo;
             }
             else {
                 if ($scope.oe.orderCargoList != null) {
                     $scope.oe.orderCargoList.push(cargo);
                 }
                 else {
                     $scope.oe.orderCargoList = new Array();
                     $scope.oe.orderCargoList.push(cargo);
                 }
             }
             $scope.generate(false, true);
         }, function () {

         });
     };

     $scope.validateRecord = function (value, title) {

         $scope.oeCntrl.isOpenMerchantPPCustomer = false;
         $scope.oeCntrl.isOpenMerchantPPNotifyParty = false;
         $scope.oeCntrl.isOpenMerchantPPConsignee = false;
         $scope.oeCntrl.isOpenMerchantPPConsignor = false;
         $scope.mp.IsBillingCustomer = false;
         $scope.mp.IsShipper = false;
         $scope.mp.IsConsignee = false;

         if (title == 'CUSTOMER') {
             $scope.oeCntrl.isOpenMerchantPPCustomer = value;
             $scope.mp.IsBillingCustomer = true;
             $scope.mp.IsShipper = true;
             $scope.mp.IsConsignee = true;
         }
         else if (title == 'NOTIFY PARTY') {
             $scope.oeCntrl.isOpenMerchantPPNotifyParty = value;
             $scope.mp.IsBillingCustomer = true;
         }
         else if (title == 'CONSIGNEE') {
             $scope.oeCntrl.isOpenMerchantPPConsignee = value;
             $scope.mp.IsConsignee = true;
         }
         else if (title == 'CONSIGNOR') {
             $scope.oeCntrl.isOpenMerchantPPConsignor = value;
             $scope.mp.IsShipper = true;
         }
         $scope.oeCntrl.modaltitle = 'NEW ' + title;
     };

     $scope.countryResults = function (text) {
         return CountryService.SearchCountries(text).then(function (d) {
             return limitToFilter(d.data, 15);
         }, function (err) { });
     };

     $scope.countrySelect = function (item) {
         $scope.mp.address.CountryCode = item.Value;
     };

     $scope.isFrmMerchant = false;
     $scope.$watch('oeCntrl.frmMerchant.$valid', function (isValid) {
         $scope.isFrmMerchant = isValid;
     });

     $scope.mp = {};
     $scope.SaveMerchant = function (mp) {
         if ($scope.isFrmMerchant) {
             $scope.mp.AddressList = new Array();
             if (!angular.isUndefined(mp.address))
                 $scope.mp.AddressList.push(mp.address);

             var fResults = $scope.fResults;
             var yResults = $scope.yResults;
             var tResults = $scope.tResults;

             $scope.mp.MerchantRelationList = new Array();
             if (!angular.isUndefined(fResults)) {
                 for (var i = 0; i < fResults.length; i++) {
                     var obj = {
                         RelatedMerchantCode: fResults[i].Value,
                         RelationshipType: fResults[i].RelationshipType,

                     };

                     $scope.mp.MerchantRelationList.push(obj);
                 }
             }

             if (!angular.isUndefined(yResults)) {
                 for (var i = 0; i < yResults.length; i++) {
                     var obj = {
                         RelatedMerchantCode: yResults[i].Value,
                         RelationshipType: yResults[i].RelationshipType
                     };

                     $scope.mp.MerchantRelationList.push(obj);
                 }
             }

             if (!angular.isUndefined(tResults)) {
                 for (var i = 0; i < tResults.length; i++) {
                     var obj = {
                         RelatedMerchantCode: tResults[i].Value,
                         RelationshipType: tResults[i].RelationshipType
                     };

                     $scope.mp.MerchantRelationList.push(obj);
                 }
             }

             MerchantProfileService.SaveMerchantProfile(mp).then(function (d) {
                 $scope.showLoading = false;
                 if (d.data) {
                     $scope.mp = {};
                     //$location.path('/masterdata/merchantlist');
                     $scope.oeCntrl.isOpenMerchantPPCustomer = false;
                     $scope.oeCntrl.isOpenMerchantPPNotifyParty = false;
                     $scope.oeCntrl.isOpenMerchantPPConsignee = false;
                     $scope.oeCntrl.isOpenMerchantPPConsignor = false;
                     growl.success(d.data, {});
                 }
             }, function (err) {
                 growl.error(err.statusText, {});
             });

             //$uibModalInstance.close(mp);
         } else {
             growl.error('please entry all mandatory fields', {});
         }
     };

     $scope.IsCloneBtnDisabled = false;
     $scope.CloneOrderEntry = function (orderNo) {
         $scope.IsCloneBtnDisabled = true;
         $scope.showLoading = true;
         OrderEntryService.CloneOrderEntry(orderNo).then(function (d) {
             debugger;
             $scope.IsNew = true;
             $scope.tabs[6].visible = false;
             $scope.showLoading = false;
             $scope.oe = d.data;
             //$scope.oe.orderContainerList = new Array();
             $scope.BindAddress(d.data, 'Customer');
             $scope.BindAddress(d.data, 'NotifyParty');
             $scope.BindAddress(d.data, 'Consignee');
             $scope.BindAddress(d.data, 'Shipper');
             $scope.BindAddress(d.data, 'FwdAgent');
             $scope.BindAddress(d.data, 'ShippingAgent');

             //console.log(JSON.stringify(d.data));
             $scope.jobTypeChanged($scope.oe.JobType);
             $scope.orderCategoryChanged();

             if (!angular.isUndefined($scope.oe)) {
                 if ($scope.oe.ETA == null) {
                     $scope.oe.ETA = undefined;
                 }
                 if ($scope.oe.ETD == null) {
                     $scope.oe.ETD = undefined;
                 }
                 if ($scope.oe.YardCutOffTime == null) {
                     $scope.oe.YardCutOffTime = undefined;
                 }

                 if ($scope.oe.SalesOrderDate == null) {
                     $scope.oe.SalesOrderDate = undefined;
                 }
             }
             $scope.transportTypeChanged();
         }, function (err) { });
     };

 }]);






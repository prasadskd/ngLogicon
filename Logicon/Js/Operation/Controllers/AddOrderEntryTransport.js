﻿app.controller('AddOrderEntryTransport', ['$scope', '$uibModalInstance', 'OrderEntryService', 'dataObj', 'growl', function ($scope, $uibModalInstance, OrderEntryService, dataObj, growl) {
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    debugger;
    $scope.tra = dataObj.transItem;
    $scope.isFrmTransportValid = false;
    $scope.$watch('frmTransport.$valid', function (isValid) {
        $scope.isFrmTransportValid = isValid;
    });

    OrderEntryService.GetContainerLookupData($scope.tra.Size).then(function (d) {
        $scope.lookupData = d.data;
    }, function (err) { });

    $scope.sizeTransportChanged = function () {
        OrderEntryService.GetSizeType($scope.tra.Size).then(function (d) {            
            $scope.lookupData.TypeList = d.data;
        }, function () { })
    };

    $scope.SaveTransport = function (tra) {        
        if ($scope.isFrmTransportValid) {
            $uibModalInstance.close(tra);
        } else {
            growl.error('please enter all mandatory fields', {});
        }        
    };
}]);
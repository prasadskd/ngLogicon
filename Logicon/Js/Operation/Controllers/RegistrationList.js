﻿app.controller('RegistrationListCntrl', ['$scope', 'RegisteredCompanyService', 'RegistrationService', '$q', '$location','$route','UtilityFunc',
function ($scope, RegisteredCompanyService, RegistrationService, $q, $location, $route, UtilityFunc) {
    $scope.DateFormat = UtilityFunc.DateFormat();
    $scope.currentPage = 1;
    $scope.limit = 10;
    $scope.filter = 4063;
    $scope.Search = {
        filter: 4063,
        CompanyName:'',
        RegistrationNo:'',
        DateFrom: UtilityFunc.FirstDateOfMonth(),
        DateTo: moment()
    };

    var promise1 = RegisteredCompanyService.GetRegisteredCompanies($scope.filter, 0);
    var promise2 = RegistrationService.GetLookUpdata();
    $scope.showLoading = true;
    $q.all([promise1, promise2]).then(function (d) {
        $scope.showLoading = false;

        $scope.companies = d[0].data.registeredCompanies;
        $scope.totalItems = d[0].data.totalItems;

        var arr = new Array();
        angular.forEach(d[1].data.registrationStatusList, function (item, index) {
            if (item.Text != 'Rejected' && item.Text != 'Approved')
                arr.push(item);
        });

        d[1].data.registrationStatusList = arr;
        $scope.lookUpData = d[1].data;
    }, function (err) { });

    $scope.viewInfo = function (companyID) {
        $location.path('/company/' + companyID);
    };

    $scope.viewInfo2 = function (companyID) {
        $location.path('/map');
    };

    $scope.getData = function () {
        var skip = ($scope.currentPage == 1 ? 0 : ($scope.limit * ($scope.currentPage - 1)));
        RegisteredCompanyService.GetRegisteredCompanies($scope.filter, skip).then(function (d) {
            $scope.companies = d.data.registeredCompanies;
            $scope.totalItems = d.data.totalItems;

            $scope.showLoading = false;
        }, function (err) { });
    };

    $scope.filterChanged = function () {
        $scope.showLoading = true;
        $scope.getData();
    };

    $scope.pageChanged = function () {
        $scope.showLoading = true;
        $scope.getData();
    };

    $scope.ExportData = function (type, mimetype) {
        RegisteredCompanyService.ExportData(type, mimetype);
    };

    $scope.isFrmSearchValid = false;
    $scope.$watch('RegistrationListCntrl.frmSearch.$valid', function (isValid) {
        $scope.isFrmSearchValid = isValid;
    });

    $scope.SearchRegisteredCompanies = function (CompanyName, RegistrationNo, DateFrom, DateTo, filter, Email) {
        var obj = {
            CompanyName: CompanyName == undefined ? null : CompanyName,
            RegistrationNo: RegistrationNo == undefined ? null : RegistrationNo,
            DateFrom: DateFrom == undefined ? null : DateFrom,
            DateTo: DateTo == undefined ? null : DateTo,
            RegistrationStatus: (filter == undefined || filter == "") ? null : filter,
            Email: (Email == undefined ? null : Email)
        };
        
        if ($scope.isFrmSearchValid) {
            $scope.showLoading = true;
            RegisteredCompanyService.SearchRegisteredCompanies(obj).then(function (d) {
                $scope.showLoading = false;
                $scope.companies = d.data;
                $scope.searchPopOver = false;
            }, function (err) { });
        }
    };

    $scope.isFrmDeleteValid = false;
    $scope.$watch('RegistrationListCntrl.frmDelete.$valid', function (isValid) {
        $scope.isFrmDeleteValid = isValid;
    });
    $scope.deletedcompanyid=null;
    $scope.validateRecord = function (deletedcompanyid) {
        $scope.deletedcompanyid = deletedcompanyid;
        isOpenRemarksPP = true;
    };

    $scope.Delete = function (remarks) {
        if ($scope.isFrmDeleteValid) {
            var Obj = {
                CompanyID: $scope.deletedcompanyid,
                Status: false,
                Remarks: remarks
            };
            debugger;

            //$location.reload();
            ////$location.path('/operation/subscriberslist');
            RegisteredCompanyService.DeletedCompany(Obj).then(function (d) {
                if (d.data) {
                    $route.reload('operation/registrationlist');
                    growl.warning('Deletedted..!', {});

                } else {

                }
            }, function (err) { });
        } else {
            growl.error('Please enter remarks for deletion', {});
        }
    };
}]);
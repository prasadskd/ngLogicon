﻿app.controller('OrderEntryListCntrl', ['$scope', 'OrderEntryListService', '$q', '$location', '$routeParams','UtilityFunc','$route',
function ($scope, OrderEntryListService, $q, $location, $routeParams, UtilityFunc, $route) {
    $scope.currentPage = 1;
    $scope.limit = 10;
    $scope.Search = {};

    $scope.dateFormat = UtilityFunc.DateFormat();
    $scope.dataGridNorecords = UtilityFunc.DataGridNorecords();
    $scope.Search.DateFrom = UtilityFunc.FirstDateOfMonth(); //new Date(d.getFullYear(), d.getMonth(), 1);
    $scope.Search.DateTo = moment();

    $scope.GetOrderEntries = function (obj) {
        OrderEntryListService.SearchOrderEntries(obj).then(function (d) {
            $scope.orderentries = d.data.orderentries;
            $scope.totalItems = d.data.recordsCount;
        }, function (err) { });

    };

    $scope.isFrmSearchValid = false;
    $scope.$watch('oelCntrl.frmSearch.$valid', function (isValid) {
        $scope.isFrmSearchValid = isValid;
    });

    $scope.SearchOrderEntries = function () {        
        var obj = {
            branchID: $routeParams.branchID == undefined ? null : $routeParams.branchID,
            orderNo: null,
            VoyageNo: $scope.Search.VoyageNo == undefined ? null : $scope.Search.VoyageNo,
            ManifestNo: $scope.Search.ManifestNo == undefined ? null : $scope.Search.ManifestNo,
            DateFrom: $scope.Search.DateFrom == undefined ? null : $scope.Search.DateFrom,
            DateTo: $scope.Search.DateTo == undefined ? null : $scope.Search.DateTo,
            HouseBLNo: $scope.Search.HouseBLNo == undefined ? null : $scope.Search.HouseBLNo,
            Shipper: $scope.Search.Shipper == undefined ? null : $scope.Search.Shipper,
            Consignee: $scope.Search.Consignee == undefined ? null : $scope.Search.Consignee,
            vesselName: $scope.Search.vesselName == undefined ? null : $scope.Search.vesselName,
            IsDefault: false,
            Skip: ($scope.currentPage == 1 ? 0 : ($scope.limit * ($scope.currentPage - 1))),
            Limit: $scope.limit
        };
        if ($scope.isFrmSearchValid) {
            OrderEntryListService.SearchOrderEntries(obj).then(function (d) {
                $scope.orderentries = d.data.orderentries;
                $scope.totalItems = d.data.recordsCount;
            }, function (err) { });
        }
    };

    $scope.pageChanged = function () {
        $scope.SearchOrderEntries();
    };
    $scope.GetOrderEntries({
        branchID: $routeParams.branchID == undefined ? null : $routeParams.branchID,
        orderNo: null,
        VoyageNo: null,
        ManifestNo: null,
        DateFrom: null,
        DateTo: null,
        HouseBLNo: null,
        Shipper: null,
        Consignee: null,
        vesselName: null,
        IsDefault: true,
        Skip: 0,
        Limit: 0
    });
    $scope.viewInfo = function (orderno) {
        $location.path('/operation/orderentry/' + orderno);
    };

    $scope.viewInfo2 = function (orderno) {        
        $location.path('/operation/orderentry/' + orderno);
    };
    $scope.refresh = function () {
        $route.reload();
    }
}]);
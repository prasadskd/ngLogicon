﻿app.controller('AddOrderEntryCargo', ['$scope', 'OrderEntryService', 'HSCodeService', '$uibModalInstance', 'dataObj', '$http', 'Utility', 'limitToFilter', 'CountryService', 'growl',
    function ($scope, OrderEntryService, HSCodeService, $uibModalInstance, dataObj, $http, Utility, limitToFilter, CountryService, growl) {


        $scope.showTrucks = dataObj.TransportType != 1020 ? true : false;
        $scope.showPallet = dataObj.TransportType == 1020 ? true : false;
        $scope.c = {};
        $scope.isFrmCargoValid = false;
        $scope.$watch('frmCargo.$valid', function (isValid) {
            $scope.isFrmCargoValid = isValid;
        });

        var SelectData = new Array();
        if (dataObj.FreightMode == 1030) {
            $scope.lblText = 'Containers';
        } else if (dataObj.FreightMode == 1031) {
            $scope.lblText = 'Trucks';
        } else {
            $scope.lblText = 'Trucks';
        }

        angular.forEach(dataObj.List, function (item, index) {           

            if (!angular.isUndefined(item.ContainerNo)) {
                var obj = {
                    Value: item.ContainerNo,
                    Text: item.ContainerNo
                };
                SelectData.push(obj);
            }
            
        });

        $scope.lookUpData = {};
        $scope.lookUpData.List = SelectData;
        $scope.lookUpData.uomList = dataObj.uomList;
        $scope.lookUpData.stockRoomList = dataObj.stockRoomList;
        $scope.lookUpData.currencyList = dataObj.currencyList;
        $scope.c = dataObj.cargoItem;

        $scope.GenericMerchantResults = function (text, filter) {
            return $http.get(Utility.ServiceUrl + '/master/MerchantProfile/search/' + text + '/' + filter).then(function (response) {
                return limitToFilter(response.data, 15);
            });
        };
        //test
        CountryService.GetCountriesList().then(function (d) {
            $scope.lookUpData.CountriesList = d.data;
        }, function (err) { growl.error(err.statusText, {}); });

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.CustomerSelected = function (item, type) {
            $scope.c[type] = item.Value;
        };

        $scope.HSCodeResults = function (text) {
            return HSCodeService.Search(text).then(function (res) {
                return limitToFilter(res.data, 15);
            }, function (err) { });
        };

        $scope.SaveCargo = function (c) {
           
            if ($scope.isFrmCargoValid) {
                $uibModalInstance.close(c);
            } else {
                growl.error('please entry all mandatory fields', {});
            }
        };
        $scope.CaluclateLocalCargoAmount = function () {
            // debugger;
            var foreignAmount = parseFloat($scope.c.ForeignPrice);
            var exchangeRate = parseFloat($scope.c.ExchangeRate);
            if (!isNaN(foreignAmount) && !isNaN(exchangeRate)) {
                $scope.c.LocalPrice = (foreignAmount * exchangeRate).toFixed(4);
            }
            else {
                $scope.c.LocalPrice = 0;
            }

        };
    }]);
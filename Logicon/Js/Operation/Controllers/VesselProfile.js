﻿app.controller('VesselProfileCntrl', ['$scope', 'VesselProfileService', 'growl', '$routeParams', '$q', '$location', '$uibModal', 'Utility', function ($scope, VesselProfileService, growl, $routeParams, $q, $location, $uibModal, Utility) {
    $scope.vp = {};
    $scope.showLoading = true;
    $scope.currentPage = 1;
    $scope.limit = 10;

    $scope.IsNew = false;
    $scope.isFrmVesselProfileValid = false;
    $scope.$watch('frmVesselProfile.$valid', function (isValid) {
        $scope.isFrmVesselProfileValid = isValid;
    });

    $scope.addSchedule = function (vesselScheduleID) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'Js/Operation/Templates/VesselSchedule/vessel-schedule.html?v=' + Utility.Version,
            controller: 'addEditVesselScheduleCntrl',
            //size: 'lg',
            windowClass: 'app-modal-window2',
            resolve: {
                vesselScheduleID: function () {
                    return vesselScheduleID;
                }
            }
        });

        modalInstance.result.then(function () {
            var dt = moment(vm.viewDate).format('MM-01-YYYY');
            var m = vm.calendarView;
            $scope.GetVesselScheduleListByDate(dt);
        }, function () {
            //$log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.SaveVesselProfile = function (vp) {
        $scope.showLoading = true;
        if ($scope.isFrmVesselProfileValid) {
            VesselProfileService.SaveVesselProfile(vp).then(function (d) {

                $scope.showLoading = false;
                growl.success('Saved Successfully..', {});
            }, function (err) { });
        } else {
            growl.error('Please enter all mandatory fields', {});
            $scope.showLoading = false;
        }
    };

    $scope.lookUpData = function () {
        VesselProfileService.lookUpData().then(function (d) {            
            $scope.lookUpData = d.data;
            $scope.showLoading = false;
        }, function (err) { });
    };

    //$scope.GetVessel = function (vesselID) {
    //    VesselProfileService.GetVessel(vesselID).then(function (d) {
    //        $scope.vp = d.data.vessel;
    //        $scope.vs = d.data.vesselScheduleList;            
    //    }, function (err) { });
    //};

    $scope.GetVesselList = function (skip, limit) {
        VesselProfileService.GetVesselList(skip, limit).then(function (d) {            
            $scope.vpList = d.data.vesselList;
            $scope.totalItems = d.data.totalItems;

            $scope.showLoading = false;
        }, function (err) { });
    };

    $scope.DeleteVessel = function (vesselID) {
        $scope.showLoading = true;
        VesselProfileService.DeleteVessel(vesselID).then(function (d) {
            $scope.getData();
            $scope.showLoading = false;
        }, function (err) { });
    };

    $scope.AddVessel = function (vesselID) {
        $location.path('/operation/manifest/vesselprofile/' + vesselID);
    };

    $scope.back = function () {
        $location.path('/operation/manifest/vesselprofilelist')
    };

    $scope.getData = function () {
        $scope.showLoading = true;
        var skip = $scope.currentPage == 1 ? 0 : ($scope.limit * ($scope.currentPage - 1));
        $scope.GetVesselList(skip, $scope.limit);
    };

    $scope.pageChanged = function () {
        $scope.getData();
    };

    var vesselID = $routeParams.vesselID;

    debugger;
    if (!angular.isUndefined(vesselID)) {
        if (vesselID != 'NEW') {
            var lookUpPromise = VesselProfileService.lookUpData();
            var vesselPromise = VesselProfileService.GetVessel(vesselID);

            $q.all([lookUpPromise, vesselPromise]).then(function (d) {
                $scope.lookUpData = d[0].data;
                $scope.vp = d[1].data.vessel;
                $scope.Cntrl.vs = d[1].data.vesselScheduleList;
                debugger;
                $scope.showLoading = false;
            }, function () { });
        } else {
            $scope.IsNew = true;
            $scope.lookUpData();
        }
    } else {
        $scope.IsNew = true;
        $scope.getData();
    }


}]);
﻿app.controller('AddOrderEntryContainer', ['$scope', 'OrderEntryService', '$uibModalInstance', 'dataObj', 'growl','UtilityFunc',
    function ($scope, OrderEntryService, $uibModalInstance, dataObj, growl, UtilityFunc) {

        $scope.dateFormat = UtilityFunc.DateFormat();
        $scope.dateTimeFormat = UtilityFunc.DateTimeFormat();
        
        $scope.showLoading = true;
        $scope.con = dataObj.containerItem;

        if (!angular.isUndefined($scope.con)) {
            if ($scope.con.RequiredDate == null) {
                $scope.con.RequiredDate = undefined;
            }

            if ($scope.con.WeighingDate == null) {
                $scope.con.WeighingDate = undefined;
            }
        }
        $scope.IsContainerMandatory = dataObj.JobType == 1060 ? true : false;
        
        OrderEntryService.GetContainerLookupData($scope.con.Size).then(function (d) {            
            $scope.showLoading = false;
            $scope.lookupData = d.data;
        }, function (err) { });

        $scope.popup = {
            RequiredDate: false,
            DehireDate: false
        };

        $scope.open = function (type) {
            $scope.popup[type] = true;
        };

        $scope.isFrmContainerValid = false;
        $scope.$watch('frmContainer.$valid', function (isValid) {
            $scope.isFrmContainerValid = isValid;
        });

        $scope.SaveContainer = function (con) {
            if ($scope.isFrmContainerValid) {
                $uibModalInstance.close(con);
            } else {
                growl.error('please enter all mandatory fields', {});
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.sizeChanged = function () {
            $scope.showLoading = true;
            OrderEntryService.GetSizeType($scope.con.Size).then(function (d) {
                $scope.showLoading = false;
                $scope.lookupData.TypeList = d.data;
            }, function () { })
        };
    }]);
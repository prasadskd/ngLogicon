﻿app.controller('addEditVesselScheduleCntrl', ['$scope', '$uibModalInstance', 'VesselScheduleService', 'vesselScheduleID', 'Utility', 'limitToFilter', '$http', 'PortAreaService', '$filter', 'UtilityFunc',
    function ($scope, $uibModalInstance, VesselScheduleService, vesselScheduleID, Utility, limitToFilter, $http, PortAreaService, $filter, UtilityFunc) {

        $scope.dateFormat = UtilityFunc.DateFormat();
        $scope.dateTimeFormat = UtilityFunc.DateTimeFormat();
        $scope.dateTimeFormat12 = UtilityFunc.DateTimeFormat12();
        //$scope.showLoading = false;
        $scope.isEdit = false;
        $scope.isChangeTerminal = false;
        $scope.isCallNewPort = false;
        $scope.isfrmVesselScheduleValid = false;
        $scope.$watch('frmVesselSchedule.$valid', function (isValid) {
            $scope.isfrmVesselScheduleValid = isValid;
        });
        $scope.vs = {};
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.formats = ['dd/MM/yyyy'];
        $scope.format = $scope.formats[0];

        $scope.popup = {};

        $scope.open = function (type) {
            $scope.popup[type] = true;
        };

        $scope.SaveVesselSchedule = function (vs) {
            if ($scope.isfrmVesselScheduleValid) {
                VesselScheduleService.SaveVesselSchedule(vs).then(function (d) {
                    $uibModalInstance.close();
                }, function () { });
            }
        };

        if (vesselScheduleID != -1) {
            $scope.isEdit = true;
            VesselScheduleService.GetVesselSchedule(vesselScheduleID).then(function (d) {
                $scope.vs = d.data;
            }, function () { });
        }
        $scope.VesselIDResults = function (text) {
            //$scope.showLoading = true;
            return $http.get(Utility.ServiceUrl + '/master/Vessel/search/vesselID/' + text).then(function (response) {
                debugger;
                //$scope.showLoading = false;
                return limitToFilter(response.data, 15);
            });
        };

        $scope.VesselNameResults = function (text) {
            return $http.get(Utility.ServiceUrl + '/master/Vessel/search/vesselName/' + text).then(function (response) {
                debugger;
                return limitToFilter(response.data, 15);
            });
        };

        $scope.portResults = function (text) {
            return PortAreaService.PortAutoComplete(text).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.PortSelected = function (item, type) {
            $scope.vs[type] = item.PortCode;
        };

        /*
        $scope.dischargePortResults = function (text) {
            return $http.get(Utility.ServiceUrl + '/master/Vessel/search/vesselName/' + text).then(function (response) {
                return limitToFilter(response.data, 15);
            });
        };
    
        $scope.destinationPortResults = function (text) {
            return $http.get(Utility.ServiceUrl + '/master/Vessel/search/vesselName/' + text).then(function (response) {
                return limitToFilter(response.data, 15);
            });
        };
        */
        $scope.vesselIDClick = function (item) {
            $scope.vs.VesselName = item.Text;
        };

        $scope.vesselNameClick = function (obj) {
            $scope.vs.VesselID = obj.Value;
        };

        $scope.changeTerminal = function () {
            $scope.isChangeTerminal = true;
        };

        VesselScheduleService.GetTerminalList().then(function (d) {
            debugger;

            $scope.vesselClassList = d.data.terminalList;
        }, function (err) {
            debugger;
        });

        //vs.Terminal=(vesselClassList | filter : {Text: vs.TerminalText })[0].Text.toString()
        $scope.terminalChange = function () {

            var obj = $filter('filter')($scope.vesselClassList, { Text: $scope.vs.TerminalText })[0];
            $scope.vs.Terminal = obj.Value.toString();

        };

    }]);
﻿app.service('OrderEntryService', ['$http', '$q', 'Utility', 'UtilityFunc', function ($http, $q, Utility, UtilityFunc) {
    this.SaveOrderEntry = function (oe) {
        var deferred = $q.defer();
        $http.post(Utility.ServiceUrl + '/Operation/OrderEntry/save', JSON.stringify(oe)).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.GetLookupData = function (type) {
        var deferred = $q.defer();
        $http.get(Utility.ServiceUrl + '/Operation/OrderEntry/lookup').then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.GetTerminalList = function () {
        var deferred = $q.defer();
        $http.get(Utility.ServiceUrl + '/Master/TerminalOperator/operatorList').then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.GetContainerLookupData = function (size) {
        var deferred = $q.defer();
        $http.get(Utility.ServiceUrl + '/Operation/OrderEntry/container/lookup/' + size).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.GetPendingOrders = function (type, text) {
        var deferred = $q.defer();
        $http.get(Utility.ServiceUrl + '/Operation/OrderEntry/' + type + '/' + text).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.GetSizeType = function (size) {
        var deferred = $q.defer();
        $http.get(Utility.ServiceUrl + '/master/equipment/' + size + '/type').then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.GetOrderEntryByNo = function (orderNo) {
        var deferred = $q.defer();
        $http.get(Utility.ServiceUrl + '/Operation/OrderEntry/orderno/' + orderNo).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.CloneOrderEntry = function (orderNo) {
        var deferred = $q.defer();
        $http.get(Utility.ServiceUrl + '/Operation/OrderEntry/clone/orderno/' + orderNo).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.deleteOrderEntry = function (orderNo) {
        debugger;
        var deferred = $q.defer();
        $http.delete(Utility.ServiceUrl + '/Operation/OrderEntry/deleteorderheader/' + orderNo).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.SearchOrder = function (text) {
        var deferred = $q.defer();
        $http.get(Utility.ServiceUrl + '/Operation/OrderEntry/search/' + text).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.SendToAgent = function (orderNo) {
        var deferred = $q.defer();
        $http.put(Utility.ServiceUrl + '/Operation/OrderEntry/' + orderNo).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.GetWebOrdersForAgent = function () {
        var deferred = $q.defer();
        $http.get(Utility.ServiceUrl + '/Operation/OrderEntry/agentweborders').then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.GetWebOrder = function (orderNo, branchId) {
        var deferred = $q.defer();
        $http.get(Utility.ServiceUrl + '/Operation/OrderEntry/weborder/' + orderNo + '/' + branchId).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.ApproveWebOrder = function (obj) {
        var deferred = $q.defer();
        $http.put(Utility.ServiceUrl + '/Operation/OrderEntry/weborder/approve', JSON.stringify(obj)).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.GenerateDeclaration = function (oe) {
        var deferred = $q.defer();
        $http.post(Utility.ServiceUrl + '/Operation/OrderEntry/declaration', JSON.stringify(oe)).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.GenerateBookingEntry = function (oe) {
        var deferred = $q.defer();
        $http.post(Utility.ServiceUrl + '/Operation/OrderEntry/bookingEntry', JSON.stringify(oe)).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };


    this.uploadFiles = function (data, OrderNo) {
        debugger;
        var deferred = $q.defer();

        var objXhr = new XMLHttpRequest();
        objXhr.onreadystatechange = function () {
            if (objXhr.readyState == 4) {
                deferred.resolve('Success');
            }
        };

        objXhr.onerror = function () {
            deferred.reject('Error');
        };

        objXhr.open('POST', Utility.ServiceUrl + '/Operation/OrderEntry/uploadFiles/' + OrderNo);
        objXhr.setRequestHeader('USERID', UtilityFunc.UserID());
        objXhr.setRequestHeader('BRANCH_ID', UtilityFunc.BranchID());
        objXhr.send(data);
        return deferred.promise;
    };

    this.GetOrderEntryDocsNo = function (orderNo) {
        var deferred = $q.defer();
        debugger;
        $http.get(Utility.ServiceUrl + '/Operation/OrderEntry/getOrderEntryDocs/' + orderNo).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.DeleteImage = function (branchID, orderNo, itemNo) {
        var deferred = $q.defer();
        debugger;
        $http.delete(Utility.ServiceUrl + '/Operation/OrderEntry/deleteOrderEntryDoc/' + branchID + '/' + orderNo + '/' + itemNo).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };
}]);
﻿app.controller('AddEditItemEntryCntrl', ['$scope', '$uibModalInstance', 'HSCodeService', 'ItemEntryService', 'limitToFilter', '$filter', 'dataObj', 'growl', 'UtilityFunc',
function ($scope, $uibModalInstance, HSCodeService, ItemEntryService, limitToFilter, $filter, dataObj, growl, UtilityFunc) {
    debugger;
    //$scope.ie = {
    //    declarationsubItems: new Array()
    //};
    $scope.defaultCountry = UtilityFunc.DefaultCountry();

    $scope.ie = dataObj.itemEntry;
    $scope.showLoading = true;

    /*
    $scope.HSCodeChange = function (hsCode) {
        var description = $filter('filter')($scope.lookUpData.hsCodeList, { Value: hsCode })[0].Text;
        if (description.indexOf('PETROLEUM') >= 0) {
            $scope.showPetrolPnl = true;
        } else {
            $scope.showPetrolPnl = false;
        }
    };*/
    
    $scope.HSCodeChange = function (obj) {
        $scope.ie.HSCode = obj.Value;
        if (obj.Text.indexOf('PETROLEUM') >= 0) {
            $scope.showPetrolPnl = true;
        } else {
            $scope.showPetrolPnl = false;
        }
        
        
        $scope.ie.ImportDutyPercentage = obj.ImportRate;
        $scope.ie.ImportDutySpecific = obj.ImportSpecific;

        $scope.ie.ExciseDutyPercentage = obj.ExciseRate;
        $scope.ie.ExciseDutySpecific = obj.ExciseSpecific;
        /*
        $scope.ie.ImportDutyAmount = dataObj.CIFC * $scope.ie.ImportDutyPercentage;
        $scope.ie.ImportDutySpecificAmount = $scope.ie.DeclaredQty * $scope.ie.ImportDutyPercentage;*/
        $scope.ImportDutyCalculation();
    };

    $scope.HSCodeResults = function (text) {
        return HSCodeService.AutoComplete(text).then(function (res) {
            return limitToFilter(res.data, 15);
        }, function (err) { });
    };

    $scope.GetLookupData = function () {
        ItemEntryService.GetLookupData().then(function (d) {
            $scope.lookUpData = d.data;
            if (dataObj.itemEntry.OriginCountryCode == null)
                $scope.ie.OriginCountryCode = $scope.defaultCountry;

            //$scope.HSCodeChange($scope.ie.HSCode);
            $scope.showLoading = false;
        }, function (err) { });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.showPetrolPnl = false;


    $scope.IsFrmItemEntryValid = false;
    $scope.$watch('frmItemEntry.$valid', function (isValid) {
        $scope.IsFrmItemEntryValid = isValid;
    });

    $scope.SaveItemEntry = function (ie) {
        if ($scope.IsFrmItemEntryValid) {
            $uibModalInstance.close(ie);
        }
        else {
            growl.error('Please enter all mandatory fields', {});
        }
    };

    $scope.IsImportDutyTariffCodeDisable = false;
    $scope.ImportDutyMethodChange = function () {
        if ($scope.ie.ImportDutyMethod == 25900) {
            $scope.ie.ImportDutyTariffCode = 25930;
            $scope.IsImportDutyTariffCodeDisable = true;
            $scope.ImportDutyCalculation();
        } else {
            $scope.IsImportDutyTariffCodeDisable = false;
        }

    };

    $scope.IsGSTDutyTariffCodeDisable = false;
    $scope.GSTDutyMethodChange = function () {
        if ($scope.ie.GSTMethod == 25900) {
            $scope.ie.GSTTariffCode = 25930;
            $scope.IsGSTDutyTariffCodeDisable = true;
            $scope.ImportDutyCalculation();
        } else {
            $scope.IsGSTDutyTariffCodeDisable = false;
        }
    };

    $scope.IsExciseDutyTariffCodeDisable = false;
    $scope.ExciseDutyMethodChange = function () {
        if ($scope.ie.ExciseDutyMethod == 25900) {
            $scope.ie.ExciseDutyTariffCode = 25930;
            $scope.IsExciseDutyTariffCodeDisable = true;
            $scope.ImportDutyCalculation();
        } else {
            $scope.IsExciseDutyTariffCodeDisable = false;
        }
    };

    $scope.ie.ImportDutyAmount = 0.00;
    $scope.ie.ImportDutySpecificAmount = 0.00;
    $scope.ie.ExciseDutyAmount = 0.00;
    $scope.ie.ExciseDutySpecificAmount = 0.00;
    $scope.ie.GSTDutyAmount = 0.00;
    $scope.ie.TotalPayableAmount = 0.00;
    $scope.ImportDutyCalculation = function () {
        var obj = {
            OriginCountryCode: $scope.ie.OriginCountryCode,
            HSCode: $scope.ie.HSCode,
            CIFC: dataObj.CIFC,
            DeclaredQty: $scope.ie.DeclaredQty,
            DeclaredUOM: $scope.ie.DeclaredUOM,
            StatisticalQty: $scope.ie.StatisticalQty,
            StatisticalUOM: $scope.ie.StatisticalUOM,
            ImportDutyMethod: $scope.ie.ImportDutyMethod,
            ImportDutyTariffCode: $scope.ie.ImportDutyTariffCode,
            ImportDutyPercentage: $scope.ie.ImportDutyPercentage,
            ImportDutySpecific: $scope.ie.ImportDutySpecific,
            ImportDutyExcemption: $scope.ie.ImportDutyExcemption,
            ImportDutySpecificExcemption: $scope.ie.ImportDutySpecificExcemption,
            ExciseDutyMethod: $scope.ie.ExciseDutyMethod,
            ExciseDutyTariffCode: $scope.ie.ExciseDutyTariffCode,
            ExciseDutyPercentage: $scope.ie.ExciseDutyPercentage,
            ExciseDutySpecific: $scope.ie.ExciseDutySpecific,
            ExciseDutyExcemption: $scope.ie.ExciseDutyExcemption,
            ExciseDutySpecificExcemption: $scope.ie.ExciseDutySpecificExcemption,
            GSTMethod: $scope.ie.GSTMethod,
            GSTTariffCode: $scope.ie.GSTTariffCode,
            GSTPercentage: $scope.ie.GSTPercentage,
            GSTExcemption: $scope.ie.GSTExcemption
        };
        debugger;
        ItemEntryService.ImportDutyCalculation(obj).then(function (d) {
            debugger;
            $scope.ie.ImportDutyAmount = d.data.ImportDutyAmount;
            $scope.ie.ImportDutySpecificAmount = d.data.ImportDutySpecificAmount;
            $scope.ie.ExciseDutyAmount = d.data.ExciseDutyAmount;
            $scope.ie.ExciseDutySpecificAmount = d.data.ExciseDutySpecificAmount;
            $scope.ie.GSTDutyAmount = d.data.GSTDutyAmount;
            $scope.ie.GSTDutySpecificAmount = d.data.GSTDutySpecificAmount;
            $scope.ie.TotalPayableAmount = d.data.TotalPayableAmount;

        }, function (err) { });
    };

    $scope.GetLookupData();
}]);
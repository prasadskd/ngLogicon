﻿app.controller('AddEditk1ClauseCntrl', ['$scope', '$uibModalInstance', 'limitToFilter', '$filter', 'dataObj', 'growl',
function ($scope, $uibModalInstance, limitToFilter, $filter, dataObj, growl) {
    debugger;
    //$scope.ie = {
    //    declarationsubItems: new Array()
    //};
    $scope.lookUpData = {
        clauseTypeList : dataObj.clauseTypeList
    };
    $scope.cl = dataObj.Clause;
    $scope.showLoading = true;   

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    

    $scope.IsFrmClauseValid = false;
    $scope.$watch('frmClause.$valid', function (isValid) {
        $scope.IsFrmClauseValid = isValid;
    });

    $scope.SaveClause = function (cl) {
        if ($scope.IsFrmClauseValid) {
            $uibModalInstance.close(cl);
        }
        else {
            growl.error('Please enter all mandatory fields', {});
        }
    };

    $scope.showLoading = false;
}]);
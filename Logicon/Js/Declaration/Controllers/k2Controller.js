﻿app.controller('k2Cntrl', ['$scope', 'k2Service', 'MerchantProfileService', 'VesselScheduleService', 'PortAreaService', 'CurrencyRateService', 'OrderEntryService', 'VesselMasterService', 'Utility', '$uibModal', 'limitToFilter', 'growl', '$routeParams', '$timeout', '$location', 'AddressService', 'UtilityFunc', '$filter', '$route',
    function ($scope, k2Service, MerchantProfileService, VesselScheduleService, PortAreaService, CurrencyRateService, OrderEntryService, VesselMasterService, Utility, $uibModal, limitToFilter, growl, $routeParams, $timeout, $location, AddressService, UtilityFunc, $filter,$route) {

        $scope.dateFormat = UtilityFunc.DateFormat();
        $scope.timeFormat = UtilityFunc.TimeFormat();
        $scope.datetimeFormat = UtilityFunc.DateTimeFormat();
        $scope.defaultCurrency = UtilityFunc.DefaultCurrency();
        $scope.defaultCountry = UtilityFunc.DefaultCountry();
        var declarationNo = $routeParams.declarationNo;

        $scope.branchID = UtilityFunc.BranchID();
        $scope.customresponses = {};
        $scope.customresponseheaderItem = {};
        $scope.IsCustomResponsesShow = false;
        $scope.transportMode = 1021;

        $scope.IsNew = true;
        $scope.showLoading = true;
        $scope.showAddBtn = true;

        $scope.showPrintBtn = false;
        if (declarationNo != "NEW") {
            $scope.showPrintBtn = true;
            $scope.IsNew = false;
        }
        else {
            $scope.IsNew = true;
        }

        $scope.k2 = {
            OpenDate: moment(),
            declarationContainersK2: new Array(),
            declarationDocumentsK2: new Array(),
            declarationClausesK2: new Array(),
            declarationItemsK2: new Array(),
            declarationInvoiceK2: {
                IsFreightCurrency: false,
                IsInsuranceCurrency: false,
                FOBAmount: 0.00,
                CIFAmount: 0.00,
                EXWAmount: 0.00,
                CNFAmount: 0.00,
                CNIAmount: 0.00,
                FreightAmount: 0.00,
                InsuranceAmount: 0.00,
                CIFCAmount: 0.00,
                PortAmountPercent: 0.00,
                FreightAmountPercent: 0.00,
                InsuranceAmountPercent: 0.00,
                OthersAmountPercent: 0.00,
                FreightAmountValue: '',
                InsuranceAmountValue: ''
            },
            declarationShipmentK2: {},
            declarationExcemptionK2: {}
        };

        if (!angular.isUndefined(declarationNo) && declarationNo != 'NEW') {
            k2Service.GetDeclaration(declarationNo).then(function (d) {
                $scope.k2 = d.data.declaration;
                if (!angular.isUndefined($scope.k2) && $scope.k2 != null) {                    
                    if (!angular.isUndefined($scope.k2.declarationExcemptionK2) && $scope.k2.declarationExcemptionK2 != null) {
                        if ($scope.k2.declarationExcemptionK2.SalesTaxRegistratonDate == null) {
                            $scope.k2.declarationExcemptionK2.SalesTaxRegistratonDate = undefined;
                        }
                    }
                    if (!angular.isUndefined($scope.k2.declarationInvoiceK2) && $scope.k2.declarationInvoiceK2 != null) {                        
                        if ($scope.k2.declarationInvoiceK2.InvoiceDate == null) {                            
                            $scope.k2.declarationInvoiceK2.InvoiceDate = undefined;
                        }
                    }
                    if (!angular.isUndefined($scope.k2.declarationShipmentK2) && $scope.k2.declarationShipmentK2 != null) {
                        if ($scope.k2.declarationShipmentK2.ETADate == null) {
                            $scope.k2.declarationShipmentK2.ETADate = undefined;
                        }
                    }
                    if (!angular.isUndefined($scope.k2.declarationDocumentsK2)) {
                        if ($scope.k2.declarationDocumentsK2.DocDateType == null) {
                            $scope.k2.declarationDocumentsK2.DocDateType = undefined;
                        }
                    }
                }
                $scope.IsCustomResponsesShow = true;
                $scope.GetCustomResponse(declarationNo);
                $scope.BindAddress(d.data.declaration.ShippingAgent, 'ShippingAgentAddress');
                $scope.Cntrl.activityList = d.data.activityList
            }, function (err) { });
        }




        $scope.ic = {};//Invoice Cargo
        $scope.sd = {};//Shipment Details
        $scope.ex = {};//Excemptions
        $scope.con = {};//Container Info
        $scope.doc = {};//Documents
        $scope.cl = {};//Clause
        $scope.ie = {};//item entry
        $scope.ies = {};//item entry subitem

        $scope.dc = {};//declaration container

        $scope.GetLookUpdata = function () {
            k2Service.GetLookupData().then(function (d) {
                $scope.lookUpData = d.data;

                if (declarationNo == 'NEW') {
                    $scope.k2.TransportMode = 1021;
                    $scope.k2.declarationInvoiceK2.LocalCurrencyCode = $scope.defaultCurrency;
                    $scope.k2.declarationInvoiceK2.PortAmountCurrencyCode = $scope.defaultCurrency;
                    $scope.k2.declarationInvoiceK2.FreightAmountCurrencyCode = $scope.defaultCurrency;
                    $scope.k2.declarationInvoiceK2.InsuranceAmountCurrencyCode = $scope.defaultCurrency;
                    $scope.k2.declarationInvoiceK2.OthersAmountCurrencyCode = $scope.defaultCurrency;
                    $scope.k2.declarationInvoiceK2.PayCountry = $scope.defaultCountry;
                    $scope.k2.declarationShipmentK2.OriginCountry = $scope.defaultCountry;
                }


                $scope.showLoading = false;
            }, function (err) { });
        };

        OrderEntryService.GetTerminalList().then(function (d) {
            debugger;
            $scope.terminalList = d.data.terminalList;
        }, function (err) { });

        //$scope.k2.TransportMode = 1021;
        $scope.PayCountryChanged = function (item, type) {
            $scope.k2.declarationInvoiceK2[type] = item.text;
        };

        $scope.GenericMerchantResults = function (text, filter) {
            return MerchantProfileService.SearchMerchantResults(text, filter).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.ChangeTransportMode = function (TransportMode) {
            debugger;
            $scope.transportMode = TransportMode;
        };

        $scope.GenericMerchantResults = function (text, filter, type) {
            return MerchantProfileService.SearchMerchantResults(text, filter).then(function (d) {
                if (d.data.length == 0)
                    $scope.k2[type] = '';
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.ShipCallResults = function (text) {
            return VesselScheduleService.ShipCallNoSearch(text).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.VoyageNoInwardResults = function (text) {
            return VesselScheduleService.VoyageNoOutWardSearch(text).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.VoyageSelected = function (item) {
            $scope.k2.declarationShipmentK2.VoyageNo = item.VoyageNoInWard;
            $scope.k2.declarationShipmentK2.SCNNo = item.ShipCallNo;
            $scope.k2.declarationShipmentK2.VesselID = item.VesselID;
            $scope.k2.declarationShipmentK2.VesselName = item.VesselName;
            $scope.k2.declarationShipmentK2.ETADate = item.ETA;

        };

        $scope.ShipCallNoSelect = function (item) {
            $scope.k2.declarationShipmentK2.VoyageNo = item.VoyageNoInWard;
            $scope.k2.declarationShipmentK2.VesselID = item.VesselID;
            $scope.k2.declarationShipmentK2.VesselName = '';
            $scope.k2.declarationShipmentK2.ETADate = item.ETA;
        };

        $scope.VoyageNoInwardSelect = function (item) {
            $scope.k2.declarationShipmentK2.SCNNo = item.ShipCallNo;
            $scope.k2.declarationShipmentK2.VesselID = item.VesselID;
            $scope.k2.declarationShipmentK2.VesselName = '';
            $scope.k2.declarationShipmentK2.ETADate = item.ETA;
        };

        $scope.MerchantSelected = function (item, Type) {
            $scope.k2[Type] = item.Value
        };

        $scope.PortAutoComplete = function (text) {
            return PortAreaService.PortAutoComplete(text).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.PortSelected = function (item, type) {
            $scope.k2.declarationShipmentK2[type] = item.PortCode;
        };

        $scope.exRateChanged = function (currencyCode, field) {
            $scope.showLoading = true;
            CurrencyRateService.GetExRate($scope.k2.declarationInvoiceK2[currencyCode]).then(function (d) {
                $scope.k2.declarationInvoiceK2[field] = d.data;
                $scope.showLoading = false;
            }, function (err) { });
        };

        $scope.TransactionType = function (item, field) {
            $scope.k2[field] = item.Code;
        };

        $scope.sizeChanged = function () {
            $scope.showLoading = true;
            OrderEntryService.GetSizeType($scope.dc.Size).then(function (d) {
                $scope.lookUpData.TypeList = d.data;
                $scope.showLoading = false;
            }, function () { })
        };

        $scope.AddDeclarationContainer = function () {
            $scope.k2.declarationContainersK2.push($scope.dc);
            $scope.dc = {};
        };





        /*shipment details*/
        $scope.VesselNameResults = function (text) {
            return VesselMasterService.GetVesselByVesselName(text).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.vesselNameClick = function (obj) {
            $scope.k2.declarationShipmentK2.VesselID = obj.Value;
        };

        $scope.MerchantSelected2 = function (item, Type) {
            $scope.k2.declarationShipmentK2[Type] = item.Value
        };

        $scope.editContainer = function (index) {
            $scope.showLoading = true;
            $scope.dc = $scope.k2.declarationContainersK2[index];
            $scope.sizeChanged();
            $timeout(function () { $scope.showLoading = false; }, 500);
        };

        $scope.deleteContainer = function (index) {
            $scope.k2.declarationContainersK2.splice(index, 1);
        };

        $scope.AdddeclarationDocumentsK2 = function () {
            if ($scope.isEditDoc) {
                $scope.k2.declarationDocumentsK2[documentIndex] = $scope.doc;
            } else {
                $scope.k2.declarationDocumentsK2.push($scope.doc);
            }
            $scope.doc = {};
            $scope.isEditDoc = false;
            documentIndex = -1;
        };
        /*
        $scope.isEditDoc = false;
        var documentIndex = -1;        
        $scope.editDocument = function (index) {
            documentIndex = index;
            $scope.isEditDoc = true;
            $scope.showLoading = true;
            $scope.doc = $scope.k2.declarationDocumentsK2[index];
            $timeout(function () { $scope.showLoading = false; }, 500);
        };*/

        $scope.deleteDocument = function (index) {
            $scope.showLoading = true;
            $scope.k2.declarationDocumentsK2.splice(index, 1);
            $timeout(function () { $scope.showLoading = false; }, 500);
            $scope.doc = {};
        };

        $scope.AddDeclarationClause = function () {
            if ($scope.isEditClause) {
                $scope.k2.declarationClausesK2[clauseIndex] = $scope.cl;
            } else {
                $scope.k2.declarationClausesK2.push($scope.cl);
            }

            $scope.cl = {};
            $scope.isEditClause = false;
            clauseIndex = -1;
        };

        $scope.isEditClause = false;
        var clauseIndex = -1;
        $scope.editClause = function (index) {
            $scope.isEditClause = true;
            clauseIndex = index;
            $scope.showLoading = true;
            $scope.cl = $scope.k2.declarationClausesK2[index];
            $timeout(function () { $scope.showLoading = false; }, 500);
        };

        $scope.deleteClause = function (index) {
            $scope.showLoading = true;
            $scope.k2.declarationClausesK2.splice(index, 1);
            $timeout(function () { $scope.showLoading = false; }, 500);
            $scope.cl = {};
        };

        var itemEntryIndex = -1;
        $scope.editItem = function (index) {
            itemEntryIndex = index;
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/Declaration/Templates/k2/item-entry.html?v=' + Utility.Version,
                controller: 'AddEditk2ItemEntryCntrl',
                windowClass: 'app-modal-window3',
                resolve: {
                    dataObj: function () {
                        return {
                            itemEntry: (itemEntryIndex == -1 ? {} : $scope.k2.declarationItemsK2[index]),
                            CIFC: $scope.k2.declarationInvoiceK2.CIFCAmount
                        }
                    }
                }
            });

            modalInstance.result.then(function (res) {
                res.declarationsubItemsK2 = $.map(res.declarationsubItemsK2, function (el) { return el });

                if (itemEntryIndex != -1)
                    $scope.k2.declarationItemsK2[itemEntryIndex] = res;
                else
                    $scope.k2.declarationItemsK2.push(res);

                itemEntryIndex = -1;
            }, function (err) {
                if (!angular.isUndefined(err.statusText))
                    growl.error(err.statusText, {});
            });
        };

        $scope.deleteItem = function (index) {
            $scope.k2.declarationItemsK2.splice(index, 1);
        };

        /*
        $scope.OpenModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/Declaration/Templates/k2/item-entry.html?v=' + Utility.Version,
                controller: 'AddEditItemEntryCntrl',
                windowClass: 'app-modal-window3',
                resolve: {
                    itemEntry: function () {
                        return {};
                    }
                }
            });
    
            modalInstance.result.then(function (res) {
                $scope.k2.declarationItemsK2.push(res);
            }, function (err) {
                growl.error(err.statusText, {});
            });
        };
        */
        $scope.GetLookUpdata();

        $scope.GenerateFile = function (declarationNo) {
            $scope.btncustom = true;
            k2Service.GenerateFile(declarationNo).then(function (d) {
                $scope.btncustom = false;
                $scope.customresponses = d.data;
                growl.success('Submitted to Customs successfully..!', {});
                $scope.active = 8;
            }, function (err) { });
            //k2Service.GenerateFile(declarationNo).then(function (d) {
            //    var modalInstance = $uibModal.open({
            //        animation: true,
            //        templateUrl: 'Js/Declaration/Templates/k2/download-file.html?v=' + Utility.Version,
            //        controller: 'AddDownLoadCntrl',
            //        windowClass: 'app-modal-window',
            //        resolve: {
            //            fileName: function () {
            //                return d.data.fileName;
            //            }
            //        }
            //    });

            //    modalInstance.result.then(function () {

            //    }, function (err) {

            //    });
            //}, function (err) {
            //    growl.error(err, {});
            //});
        };
        $scope.clear = function () {
            $route.reload('/declaration/k2declaration/NEW');
        };

        $scope.back = function () {
            $location.path('/declaration/k2inquiry');
        };

        $scope.isFrmK2Valid = false;
        $scope.$watch('frmk2.$valid', function (isValid) {
            $scope.isFrmK2Valid = isValid;
        });

        $scope.ValidatePorts = function () {
            if ($scope.k2.TransportMode == 1021) {
                if ($scope.k2.declarationShipmentK2.LoadingPort == $scope.k2.declarationShipmentK2.DischargePort) {
                    growl.error('Loading Port and Discharge Port can not be the same', {});
                    return false;
                }
                else if ($scope.k2.declarationShipmentK2.LoadingPort == $scope.k2.declarationShipmentK2.TranshipmentPort) {
                    growl.error('Loading Port and Transhipment Port can not be the same', {});
                    return false;
                }
                else if ($scope.k2.declarationShipmentK2.DischargePort == $scope.k2.declarationShipmentK2.TranshipmentPort) {
                    growl.error('Transhipment Port and Discharge Port can not be the same', {});
                    return false;
                }
                else
                    return true;
            }
            else
                return true;
        };

        $scope.Savek2Declaration = function (k2) {
            if ($scope.isFrmK2Valid) {
                if ($scope.ValidatePorts()) {
                    $scope.showLoading = true;
                    
                    k2Service.Savek2Declaration(k2).then(function (d) {
                        $scope.showLoading = false;
                        growl.success('Declaration Saved Successfully..', {});

                        $scope.k2.DeclarationNo = d.data.declarationNo;
                        $scope.k2.OrderNo = d.data.orderNo;
                        debugger;
                    }, function (err) { });
                }
            } else {
                growl.error('Please enter all mandatory fields', {});
            }
        };

        $scope.CustomerSelected = function (item, type, addresstype) {
            $scope.showLoading = true;
            var html = '';
            $scope.k2[type] = item.Value;
            $scope.k2[addresstype] = '';

            $scope.BindAddress(item.Value, addresstype);
        };


        $scope.BindAddress = function (agentCode, addresstype) {
            var html = '';
            AddressService.GetAddress(agentCode).then(function (d) {
                if (d.data != null) {
                    if (!angular.isUndefined(d.data.Address1))
                        html += d.data.Address1 + '<br/>';
                    if (!angular.isUndefined(d.data.Address2))
                        html += d.data.Address2 + '<br/>';
                    if (!angular.isUndefined(d.data.Address3))
                        html += d.data.Address3 + '<br/>';
                    if (!angular.isUndefined(d.data.City))
                        html += d.data.City + '<br/>';
                    if (!angular.isUndefined(d.data.State))
                        html += d.data.State + '<br/>';
                    if (!angular.isUndefined(d.data.CountryCode))
                        html += d.data.CountryCode + '<br/>';
                    if (!angular.isUndefined(d.data.ZipCode))
                        html += d.data.ZipCode;
                }

                $scope.k2[addresstype] = html.toUpperCase();
                $scope.showLoading = false;
            }, function (err) { });
        };

        var DocIndex = -1;
        $scope.editDocument = function (index) {
            DocIndex = index;
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/Declaration/Templates/k2/document.html?v=' + Utility.Version,
                controller: 'AddEditk2DocumentCntrl',
                size: 'lg',
                resolve: {
                    dataObj: function () {
                        return {
                            Document: (DocIndex == -1 ? {} : $scope.k2.declarationDocumentsK2[index]),
                            docDateTypeList: $scope.lookUpData.docDateTypeList,
                            OGACodeList: $scope.lookUpData.OGACodeList,
                            customStationCodeList: $scope.lookUpData.customStationCodeList,
                        };
                    }
                }
            });

            modalInstance.result.then(function (res) {
                if (DocIndex != -1)
                    $scope.k2.declarationDocumentsK2[DocIndex] = res;
                else
                    $scope.k2.declarationDocumentsK2.push(res);

                DocIndex = -1;
            }, function (err) {
                if (!angular.isUndefined(err.statusText)) {
                    growl.error(err.statusText, {});
                }
            });
        };

        var conInfoIndex = -1;
        $scope.editConInfo = function (index) {
            conInfoIndex = index;
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/Declaration/Templates/k2/container-info.html?v=' + Utility.Version,
                controller: 'AddEditk2ContainerInfoCntrl',
                size: 'lg',
                resolve: {
                    dataObj: function () {
                        return {
                            dc: (conInfoIndex == -1 ? {} : $scope.k2.declarationContainersK2[conInfoIndex]),
                            sizeList: $scope.lookUpData.sizeList,
                            jobTypeList: $scope.lookUpData.jobTypeList,
                            containerStatusList: $scope.lookUpData.containerStatusList
                        };
                    }
                }
            });

            modalInstance.result.then(function (res) {
                if (angular.isUndefined(res.Size))
                    res.Size = null;
                if (angular.isUndefined(res.EQDStatus))
                    res.EQDStatus = null;

                if (conInfoIndex != -1)
                    $scope.k2.declarationContainersK2[conInfoIndex] = res;
                else
                    $scope.k2.declarationContainersK2.push(res);

                conInfoIndex = -1;
            }, function (err) {
                if (!angular.isUndefined(err.statusText)) {
                    growl.error(err.statusText, {});
                }
            });
        };

        var clauseIndex = -1;
        $scope.editClause = function (index) {
            clauseIndex = index;
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/Declaration/Templates/k2/clause.html?v=' + Utility.Version,
                controller: 'AddEditk2ClauseCntrl',
                size: 'md',
                resolve: {
                    dataObj: function () {
                        return {
                            Clause: (clauseIndex == -1 ? {} : $scope.k2.declarationClausesK2[index]),
                            clauseTypeList: $scope.lookUpData.clauseTypeList
                        };
                    }
                }
            });

            modalInstance.result.then(function (res) {
                if (clauseIndex != -1)
                    $scope.k2.declarationClausesK2[clauseIndex] = res;
                else
                    $scope.k2.declarationClausesK2.push(res);

                clauseIndex = -1;
            }, function (err) {
                if (!angular.isUndefined(err.statusText)) {
                    growl.error(err.statusText, {});
                }
            });
        };

        $scope.LoadSMKCode = function (TransType) {
            var obj = $filter('filter')($scope.lookUpData.K2ATransactionTypeList, { Value: TransType })[0];
            $scope.k2.TransactionTypeDescription = obj.Text;
            $scope.k2.TransactionTypeCode = obj.Code;
        };

        $scope.exRateChangedAndInvoiceValue = function (currencyCode, field) {
            $scope.showLoading = true;
            CurrencyRateService.GetExRate($scope.k2.declarationInvoiceK2[currencyCode]).then(function (d) {
                $scope.k2.declarationInvoiceK2[field] = d.data;
                $scope.CalulateLocalCurrencyValue();
                $scope.showLoading = false;
            }, function (err) { });
        };

        $scope.exRateChangedAndAmountReceivedLocalValue = function (currencyCode, field) {
            $scope.showLoading = true;
            CurrencyRateService.GetExRate($scope.k2.declarationInvoiceK2[currencyCode]).then(function (d) {
                $scope.k2.declarationInvoiceK2[field] = d.data;
                $scope.CalulateAmountReceivedLocalValue();
                $scope.showLoading = false;
            }, function (err) { });
        };

        $scope.CalulateLocalCurrencyValue = function () {
            var invoiceAmount = parseFloat($scope.k2.declarationInvoiceK2.InvoiceValue);
            var exchangeRate = parseFloat($scope.k2.declarationInvoiceK2.ExchangeRate);

            if (!isNaN(invoiceAmount) && !isNaN(exchangeRate) && exchangeRate != 0) {
                $scope.k2.declarationInvoiceK2.LocalCurrencyValue = invoiceAmount * exchangeRate;
            }
            else {
                $scope.k2.declarationInvoiceK2.LocalCurrencyValue = $scope.k2.declarationInvoiceK2.InvoiceValue;
            }
        };

        $scope.CalulateAmountReceivedLocalValue = function () {
            var invoiceAmount = parseFloat($scope.k2.declarationInvoiceK2.AmountReceived);
            var exchangeRate = parseFloat($scope.k2.declarationInvoiceK2.AmountReceivedExchangeRate);

            if (!isNaN(invoiceAmount) && !isNaN(exchangeRate) && exchangeRate != 0) {
                $scope.k2.declarationInvoiceK2.AmountReceivedLocalValue = invoiceAmount * exchangeRate;
            }
            else {
                $scope.k2.declarationInvoiceK2.AmountReceivedLocalValue = $scope.k2.declarationInvoiceK2.AmountReceived;
            }
        };

        $scope.GenerateReportPDF = function () {
            k2Service.GenerateReportPDF($scope.branchID, declarationNo);
        };

        $scope.GenerateReport = function (name, reportUrl, reportID) {
            if (true) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'Js/Reports/Templates/Report.html?v=' + Utility.Version,
                    controller: 'ReportCntrl',
                    windowClass: 'app-modal-window2',
                    resolve: {
                        reportObj: function () {
                            return {
                                reportId: reportID,
                                reportName: name,
                                branchID: $scope.branchID,
                                declarationNo: declarationNo,
                                Url: '/1TradeReports/' + reportUrl
                            }
                        }
                    }
                });

                modalInstance.result.then(function (d) {

                }, function () {

                });
            }
            else {
                growl.error('Please enter all mandatory fields', {});
            }
        };

        $scope.IncoTermChanged = function () {            
            $scope.k2.declarationInvoiceK2.FreightAmountValue = $scope.k2.declarationInvoiceK2.InsuranceAmountValue = $scope.k2.declarationInvoiceK2.IncoTerm;
            $scope.IncoTermCalculation();
        };

        $scope.IncoTermCalculation = function () {            
            var incoTerm = parseFloat(angular.isUndefined($scope.k2.declarationInvoiceK2.IncoTerms) ? 0 : $scope.k2.declarationInvoiceK2.IncoTerms);
            var freight = parseFloat(angular.isUndefined($scope.k2.declarationInvoiceK2.FreightAmount) ? 0 : $scope.k2.declarationInvoiceK2.FreightAmount);
            var insurance = parseFloat(angular.isUndefined($scope.k2.declarationInvoiceK2.InsuranceAmount) ? 0 : $scope.k2.declarationInvoiceK2.InsuranceAmount);
            var port = parseFloat(angular.isUndefined($scope.k2.declarationInvoiceK2.PortAmount) ? 0 : $scope.k2.declarationInvoiceK2.PortAmount);
            var otherCharges = parseFloat(angular.isUndefined($scope.k2.declarationInvoiceK2.OtherAmount) ? 0 : $scope.k2.declarationInvoiceK2.OtherAmount);
            var invoiceValue = parseFloat(angular.isUndefined($scope.k2.declarationInvoiceK2.InvoiceValue) ? 0 : $scope.k2.declarationInvoiceK2.InvoiceValue);
            if (incoTerm == 0)
                return;

            var obj = {
                incoTerm: incoTerm,
                freight: freight,
                insurance: insurance,
                port: port,
                otherCharges: otherCharges,
                invoiceValue: invoiceValue,
                FOB: $scope.k2.declarationInvoiceK2.FOBAmount,
                CIF: $scope.k2.declarationInvoiceK2.CIFAmount,
                EXW: $scope.k2.declarationInvoiceK2.EXWAmount,
                CNF: $scope.k2.declarationInvoiceK2.CNFAmount,
                CNI: $scope.k2.declarationInvoiceK2.CNIAmount
            };
            
            $scope.showLoading = true;
            k2Service.OutPutFOBCIF(obj).then(function (d) {
                $scope.k2.declarationInvoiceK2.FOBAmount = d.data.FOB;
                $scope.k2.declarationInvoiceK2.CIFAmount = d.data.CIF;
                $scope.k2.declarationInvoiceK2.EXWAmount = d.data.EXW;
                $scope.k2.declarationInvoiceK2.CNFAmount = d.data.CNF;
                $scope.k2.declarationInvoiceK2.CNIAmount = d.data.CNI;
                $scope.k2.declarationInvoiceK2.FreightAmount = d.data.freight;
                $scope.k2.declarationInvoiceK2.InsuranceAmount = d.data.insurance;
                $scope.k2.declarationInvoiceK2.CIFCAmount = d.data.CIFC;
                $scope.showLoading = false;
            }, function (err) { });
        };



        $scope.GetCustomResponse = function (declarationNo) {
            k2Service.GetCustomResponse(declarationNo).then(function (d) {                
                $scope.customresponses = d.data.customResponse;
            }, function (err) { });
        };

        $scope.DisplayResponseData = function (obj) {            
            k2Service.GetCustomResponseInfo(obj).then(function (d) {
                $scope.customresponseheaderItem = d.data;                
            }, function (err) { });
        };

        $scope.CloneK2 = function (declarationNo) {
            $scope.showLoading = true;
            k2Service.CloneDeclaration(declarationNo).then(function (d) {
                $scope.IsCloneBtnDisabled = true;
                $scope.k2 = d.data.declaration;
                $scope.showLoading = false;

                $scope.IsNew = true;
                $scope.customresponses = null;
                $scope.active = 0;
            }, function (err) { });
        };

    }]);


app.controller('AddEditk2ContainerInfoCntrl', ['$scope', '$uibModalInstance', 'OrderEntryService', 'dataObj', 'growl', function ($scope, $uibModalInstance, OrderEntryService, dataObj, growl) {
    $scope.dc = dataObj.dc;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.lookUpData = {
        sizeList: dataObj.sizeList,
        jobTypeList: dataObj.jobTypeList,
        containerStatusList: dataObj.containerStatusList
    };

    $scope.isFrmContainerInfoValid = false;
    $scope.$watch('frmContainerInfo.$valid', function (isValid) {
        $scope.isFrmContainerInfoValid = isValid;
    });
    $scope.SaveContainerInfo = function (dc) {
        if ($scope.isFrmContainerInfoValid) {
            $uibModalInstance.close(dc);
        } else {
            growl.error('Please enter all mandatory fields', {});
        }
    };

    $scope.sizeChanged = function () {
        $scope.showLoading = true;
        OrderEntryService.GetSizeType($scope.dc.Size).then(function (d) {
            $scope.lookUpData.TypeList = d.data;
            $scope.showLoading = false;
        }, function () { })
    };

    if (!angular.isUndefined($scope.dc.Size)) {
        $scope.showLoading = true;
        OrderEntryService.GetSizeType($scope.dc.Size).then(function (d) {
            $scope.lookUpData.TypeList = d.data;
            $scope.showLoading = false;
        }, function () { })
    }
}]);
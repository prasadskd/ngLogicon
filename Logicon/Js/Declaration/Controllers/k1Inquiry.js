﻿app.controller('k1InquiryCntrl', ['$scope', 'k1Service', 'UtilityFunc', '$route', function ($scope, k1Service, UtilityFunc, $route) {
    $scope.i = {};

    $scope.currentPage = 1;
    $scope.limit = 10;

    $scope.dateFormat = UtilityFunc.DateFormat();
    $scope.dataGridNorecords = UtilityFunc.DataGridNorecords();
    $scope.i.dateFrom = UtilityFunc.FirstDateOfMonth(); //new Date(d.getFullYear(), d.getMonth(), 1);
    $scope.i.dateTo = moment();

    $scope.SearchResults = function (i) {
        $scope.showLoading = true;
        k1Service.k1Inquiry(i).then(function (d) {            
            $scope.declarations = d.data.list;
            $scope.totalItems = d.data.recordsCount;
            $scope.showLoading = false;
        }, function (err) { });
    };

    $scope.Search = function (i) {        
        i.isDefault = false;
        i.skip = $scope.currentPage == 1 ? 0 : ($scope.limit * ($scope.currentPage - 1));
        i.limit = $scope.limit;

        $scope.SearchResults(i);
    };    

    $scope.pageChanged = function () {
        $scope.Search($scope.i);
    };
    
    $scope.SearchResults({
        declarationNo: null,
        manifestNo: null,
        oceanBLNo: null,
        houseBLNo: null,
        voyageNo: null,
        dateFrom: null,
        dateTo: null,
        isDefault: true,
        skip: 0,
        limit: 0
    });
    $scope.refresh = function () {
        debugger;
        $route.reload();
        //$route.reload();
    }
}]);
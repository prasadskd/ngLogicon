﻿app.controller('AddEditk2ItemEntryCntrl', ['$scope', '$uibModalInstance', 'ItemEntryService', 'limitToFilter', '$filter', 'dataObj', 'growl', 'UtilityFunc', 'HSCodeService',
function ($scope, $uibModalInstance, ItemEntryService, limitToFilter, $filter, dataObj, growl, UtilityFunc, HSCodeService) {
    debugger;
    //$scope.ie = {
    //    declarationsubItems: new Array()
    //};
    $scope.defaultCountry = UtilityFunc.DefaultCountry();

    $scope.ie = dataObj.itemEntry;
    $scope.showLoading = true;
    /*
    $scope.HSCodeChange = function (hsCode) {
        debugger;
        var description = $filter('filter')($scope.lookUpData.hsCodeList, { Value: hsCode })[0].Text;
        if (description.indexOf('PETROLEUM') >= 0) {
            $scope.showPetrolPnl = true;
        } else {
            $scope.showPetrolPnl = false;
        }
    };*/

    $scope.HSCodeChange = function (obj) {
        debugger;
        $scope.ie.HSCode = obj.Value;
        if (obj.Text.indexOf('PETROLEUM') >= 0) {
            $scope.showPetrolPnl = true;
        } else {
            $scope.showPetrolPnl = false;
        }


        $scope.ie.ExportDutyPercentage = obj.ExportRate;
        $scope.ie.ExportDutySpecific = obj.ExportSpecific;

        $scope.ExportDutyCalculation();
    };

    $scope.HSCodeResults = function (text) {
        return HSCodeService.AutoComplete(text).then(function (res) {
            return limitToFilter(res.data, 15);
        }, function (err) { });
    };

    $scope.GetLookupData = function () {
        ItemEntryService.GetLookupData().then(function (d) {
            $scope.lookUpData = d.data;
            if (dataObj.itemEntry.OriginCountryCode == null)
                $scope.ie.OriginCountryCode = $scope.defaultCountry;

            //$scope.HSCodeChange($scope.ie.HSCode);
            $scope.showLoading = false;
        }, function (err) { });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.showPetrolPnl = false;


    $scope.IsFrmk2ItemEntryValid = false;
    $scope.$watch('frmk2ItemEntry.$valid', function (isValid) {
        $scope.IsFrmk2ItemEntryValid = isValid;
    });

    $scope.Savek2ItemEntry = function (ie) {
        if ($scope.IsFrmk2ItemEntryValid) {
            $uibModalInstance.close(ie);
        }
        else {
            growl.error('Please enter all mandatory fields', {});
        }
    };



    $scope.IsExportDutyTariffCodeDisable = false;
    $scope.ExportDutyMethodChange = function () {
        if ($scope.ie.ExportDutyMethod == 25900) {
            $scope.ie.ExportDutyTariffCode = 25930;
            $scope.IsExportDutyTariffCodeDisable = true;
            $scope.ExportDutyCalculation();
        } else {
            $scope.IsExportDutyTariffCodeDisable = false;
        }

    };

    $scope.ie.ExportDutyAmount = 0.00;
    $scope.ie.ExportDutySpecificAmount = 0.00;
    
    $scope.ie.TotalPayableAmount = 0.00;
    $scope.ExportDutyCalculation = function () {
        var obj = {
            OriginCountryCode: $scope.ie.OriginCountryCode,
            HSCode: $scope.ie.HSCode,
            CIFC: dataObj.CIFC,
            DeclaredQty: $scope.ie.DeclaredQty,
            DeclaredUOM: $scope.ie.DeclaredUOM,
            StatisticalQty: $scope.ie.StatisticalQty,
            StatisticalUOM: $scope.ie.StatisticalUOM,
            ExportDutyMethod: $scope.ie.ExportDutyMethod,
            ExportDutyTariffCode: $scope.ie.ExportDutyTariffCode,
            ExportDutyPercentage: $scope.ie.ExportDutyPercentage,
            ExportDutySpecific: $scope.ie.ExportDutySpecific,
            ExportDutyExcemption: $scope.ie.ExportDutyExcemption,
            ExportDutySpecificExcemption: $scope.ie.ExportDutySpecificExcemption,
            
        };
        debugger;
        ItemEntryService.ExportDutyCalculation(obj).then(function (d) {
            debugger;
            $scope.ie.ImportDutyAmount = d.data.ImportDutyAmount;
            $scope.ie.ImportDutySpecificAmount = d.data.ImportDutySpecificAmount;
            $scope.ie.ExciseDutyAmount = d.data.ExciseDutyAmount;
            $scope.ie.ExciseDutySpecificAmount = d.data.ExciseDutySpecificAmount;
            $scope.ie.GSTDutyAmount = d.data.GSTDutyAmount;
            $scope.ie.GSTDutySpecificAmount = d.data.GSTDutySpecificAmount;
            $scope.ie.TotalPayableAmount = d.data.TotalPayableAmount;

        }, function (err) { });
    };

    $scope.GetLookupData();
}]);
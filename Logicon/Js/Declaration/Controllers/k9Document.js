﻿app.controller('k9DocumentCntrl', ['$scope', '$uibModalInstance', 'limitToFilter', '$filter', 'dataObj', 'growl', 'UtilityFunc',
function ($scope, $uibModalInstance, limitToFilter, $filter, dataObj, growl, UtilityFunc) {
    debugger;
    $scope.dateFormat = UtilityFunc.DateFormat();
    //$scope.ie = {
    //    declarationsubItems: new Array()
    //};
    $scope.lookUpData = {
        docDateTypeList: dataObj.docDateTypeList,
        OGACodeList: dataObj.OGACodeList,
        customStationCodeList: dataObj.customStationCodeList

    };
    $scope.document = dataObj.Document;
    $scope.showLoading = true;   

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    

    $scope.IsFrmDocumentValid = false;
    $scope.$watch('frmdocument.$valid', function (isValid) {
        $scope.IsFrmDocumentValid = isValid;
    });

    $scope.SaveDocument = function (document) {
        if ($scope.IsFrmDocumentValid) {
            $uibModalInstance.close(document);
        }
        else {
            growl.error('Please enter all mandatory fields', {});
        }
    };

    $scope.showLoading = false;
}]);
﻿app.controller('k8InquiryCntrl', ['$scope', 'k8Service', 'UtilityFunc', function ($scope, k8Service, UtilityFunc) {
    $scope.i = {};
    //var d = new Date();
    //var month = d.getMonth() + 1;
    //if (month < 10) {
    //    month = "0" + month;
    //};
  
    //$scope.i.dateFrom = new Date(d.getFullYear(), d.getMonth(), 1);
    //$scope.i.dateTo = month + "/" + d.getDate() + "/" + d.getFullYear();  

    $scope.i.dateFrom = moment().format('01/MM/YYYY'); //new Date(d.getFullYear(), d.getMonth(), 1);
    $scope.i.dateTo = moment();
    $scope.dateFormat = UtilityFunc.DateFormat();

    $scope.Search = function (i) {
        $scope.showLoading = true;
        k8Service.k8Inquiry(i).then(function (d) {
            $scope.declarations = d.data;
            $scope.showLoading = false;
        }, function (err) { });
    };
    $scope.LoadDeclarations = function () {
        var Obj = {
            declarationNo: null,
            manifestNo: null,
            oceanBLNo: null,
            houseBLNo: null,
            voyageNo: null,
            dateFrom: null,
            dateTo: null
        };

        $scope.showLoading = true;
        k8Service.k8Inquiry(Obj).then(function (d) {
            $scope.declarations = d.data;
            $scope.showLoading = false;
        }, function (err) { });
    };
    $scope.LoadDeclarations();
}]);
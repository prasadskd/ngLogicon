﻿app.controller('k9Cntrl', ['$scope', 'k9Service', 'MerchantProfileService', 'VesselScheduleService', 'PortAreaService', 'CurrencyRateService', 'OrderEntryService', 'VesselMasterService', 'Utility', '$uibModal', 'limitToFilter', 'growl', '$routeParams', '$timeout', '$location', 'AddressService', 'UtilityFunc','$filter',
    function ($scope, k9Service, MerchantProfileService, VesselScheduleService, PortAreaService, CurrencyRateService, OrderEntryService, VesselMasterService, Utility, $uibModal, limitToFilter, growl, $routeParams, $timeout, $location, AddressService, UtilityFunc, $filter) {
        $scope.dateFormat = UtilityFunc.DateFormat();
        $scope.timeFormat = UtilityFunc.TimeFormat();
        $scope.datetimeFormat = UtilityFunc.DateTimeFormat();
        $scope.defaultCurrency = UtilityFunc.DefaultCurrency();
        $scope.defaultCountry = UtilityFunc.DefaultCountry();


        var declarationNo = $routeParams.declarationNo;
        if (!angular.isUndefined(declarationNo) && declarationNo != 'NEW') {
            k9Service.GetDeclaration(declarationNo).then(function (d) {
                $scope.k9 = d.data.declaration;
                if (!angular.isUndefined($scope.k9)) {
                    debugger;
                    if (!angular.isUndefined($scope.k9.declarationExcemption)) {
                        if ($scope.k9.declarationExcemption.SalesTaxRegistrationDate == null) {
                            $scope.k9.declarationExcemption.SalesTaxRegistrationDate = undefined;
                        }
                    }
                    if (!angular.isUndefined($scope.k9.declarationInvoice)) {
                        if ($scope.k9.declarationInvoice.InvoiceDate == null) {
                            $scope.k9.declarationInvoice.InvoiceDate = undefined;
                        }
                    }
                    if (!angular.isUndefined($scope.k9.declarationShipment)) {
                        if ($scope.k9.declarationShipment.ETADate == null) {
                            $scope.k9.declarationShipment.ETADate = undefined;
                        }
                    }
                    if (!angular.isUndefined($scope.k9.declarationDocuments)) {
                        if ($scope.k9.declarationDocuments.DocDateType == null) {
                            $scope.k9.declarationDocuments.DocDateType = undefined;
                        }
                    }
                }
                $scope.BindAddress(d.data.declaration.ShippingAgent, 'ShippingAgentAddress');
                $scope.Cntrl.activityList = d.data.activityList
            }, function (err) { });
        }



        $scope.showLoading = true;
        $scope.showAddBtn = true;
        $scope.k9 = {
            declarationContainers: new Array(),
            declarationDocuments: new Array(),
            declarationClauses: new Array(),
            declarationItems: new Array()
        };
        $scope.k9.declarationInvoice = {};
        $scope.ic = {};//Invoice Cargo
        $scope.sd = {};//Shipment Details
        $scope.ex = {};//Excemptions
        $scope.con = {};//Container Info
        $scope.doc = {};//Documents
        $scope.cl = {};//Clause
        $scope.ie = {};//item entry
        $scope.ies = {};//item entry subitem

        $scope.dc = {};//declaration container

        $scope.GetLookUpdata = function () {
            k9Service.GetLookupData().then(function (d) {
                $scope.lookUpData = d.data;


                if (declarationNo == 'NEW') {
                    $scope.k9.TransportMode = 1021;
                    debugger;
                    $scope.k9.declarationInvoice.LocalCurrencyCode = $scope.defaultCurrency;
                    $scope.k9.declarationInvoice.PortAmountCurrencyCode = $scope.defaultCurrency;
                    $scope.k9.declarationInvoice.FreightAmountCurrencyCode = $scope.defaultCurrency;
                    $scope.k9.declarationInvoice.InsuranceAmountCurrencyCode = $scope.defaultCurrency;
                    $scope.k9.declarationInvoice.OthersAmountCurrencyCode = $scope.defaultCurrency;
                    $scope.k9.declarationInvoice.PayCountry = $scope.defaultCountry;
                }

                $scope.showLoading = false;
            }, function (err) { });
        };


        $scope.PayCountryChanged = function (item, type) {
            $scope.k9.declarationInvoice[type] = item.text;
        };

        $scope.GenericMerchantResults = function (text, filter) {
            return MerchantProfileService.SearchMerchantResults(text, filter).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.GenericMerchantResults = function (text, filter, type) {
            return MerchantProfileService.SearchMerchantResults(text, filter).then(function (d) {
                if (d.data.length == 0)
                    $scope.k9[type] = '';
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.ShipCallResults = function (text) {
            return VesselScheduleService.ShipCallNoSearch(text).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.VoyageNoInwardResults = function (text) {
            return VesselScheduleService.VoyageNoInWardSearch(text).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.ShipCallNoSelect = function (item) {
            $scope.k9.declarationShipment.VoyageNo = item.VoyageNoInWard;
            $scope.k9.declarationShipment.VesselID = item.VesselID;
            $scope.k9.declarationShipment.VesselName = '';
            $scope.k9.declarationShipment.ETADate = item.ETA;
        };

        $scope.VoyageNoInwardSelect = function (item) {
            $scope.k9.declarationShipment.SCNNo = item.ShipCallNo;
            $scope.k9.declarationShipment.VesselID = item.VesselID;
            $scope.k9.declarationShipment.VesselName = '';
            $scope.k9.declarationShipment.ETADate = item.ETA;
        };

        $scope.MerchantSelected = function (item, Type) {
            $scope.k9[Type] = item.Value
        };

        $scope.PortAutoComplete = function (text) {
            return PortAreaService.PortAutoComplete(text).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.PortSelected = function (item, type) {
            $scope.k9.declarationShipment[type] = item.PortCode;
        };

        $scope.exRateChanged = function (currencyCode, field) {
            $scope.showLoading = true;
            CurrencyRateService.GetExRate($scope.k9.declarationInvoice[currencyCode]).then(function (d) {
                $scope.k9.declarationInvoice[field] = d.data;
                $scope.showLoading = false;
            }, function (err) { });
        };

        $scope.TransactionType = function (item, field) {
            debugger;
            $scope.k9[field] = item.Code;
        };

        $scope.sizeChanged = function () {
            $scope.showLoading = true;
            OrderEntryService.GetSizeType($scope.dc.Size).then(function (d) {
                $scope.lookUpData.TypeList = d.data;
                $scope.showLoading = false;
            }, function () { })
        };

        $scope.AddDeclarationContainer = function () {
            $scope.k9.declarationContainers.push($scope.dc);
            $scope.dc = {};
        };





        /*shipment details*/
        $scope.VesselNameResults = function (text) {
            return VesselMasterService.GetVesselByVesselName(text).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.vesselNameClick = function (obj) {
            $scope.k9.declarationShipment.VesselID = obj.Value;
        };

        $scope.MerchantSelected2 = function (item, Type) {
            $scope.k9.declarationShipment[Type] = item.Value
        };

        $scope.editContainer = function (index) {
            $scope.showLoading = true;
            $scope.dc = $scope.k9.declarationContainers[index];
            $scope.sizeChanged();
            $timeout(function () { $scope.showLoading = false; }, 500);
        };

        $scope.deleteContainer = function (index) {
            $scope.k9.declarationContainers.splice(index, 1);
        };

        $scope.AddDeclarationDocuments = function (doc) {
            if ($scope.isEditDoc) {
                $scope.k9.declarationDocuments[documentIndex] = $scope.doc;
            } else {
                $scope.k9.declarationDocuments.push($scope.doc);
            }
            $scope.doc = {};
            $scope.isEditDoc = false;
            documentIndex = -1;
        };

        $scope.isEditDoc = false;
        //var documentIndex = -1;
        //$scope.editDocument = function (index) {
        //    documentIndex = index;
        //    $scope.isEditDoc = true;
        //    $scope.showLoading = true;
        //    $scope.doc = $scope.k9.declarationDocuments[index];
        //    if ($scope.doc.DocDate == null)
        //        $scope.doc.DocDate = undefined;
        //    $timeout(function () { $scope.showLoading = false; }, 500);
        //};

        var DocIndex = -1;
        $scope.editDocument = function (index) {
            DocIndex = index;
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/Declaration/Templates/k9/document.html?v=' + Utility.Version,
                controller: 'k9DocumentCntrl',
                size: 'lg',
                resolve: {
                    dataObj: function () {
                        return {
                            Document: (DocIndex == -1 ? {} : $scope.k9.declarationDocuments[index]),
                            docDateTypeList: $scope.lookUpData.docDateTypeList,
                            OGACodeList: $scope.lookUpData.OGACodeList,
                            customStationCodeList: $scope.lookUpData.customStationCodeList,
                        };
                    }
                }
            });

            modalInstance.result.then(function (res) {

                if (DocIndex != -1)
                    $scope.k9.declarationDocuments[DocIndex] = res;
                else {
                    debugger;
                    $scope.k9.declarationDocuments.push(res);
                }

                DocIndex = -1;
            }, function (err) {
                if (!angular.isUndefined(err.statusText))
                    growl.error(err.statusText, {});
            });
        };


        $scope.deleteDocument = function (index) {
            $scope.showLoading = true;
            $scope.k9.declarationDocuments.splice(index, 1);
            $timeout(function () { $scope.showLoading = false; }, 500);
            $scope.doc = {};
        };

        $scope.AddDeclarationClause = function () {
            if ($scope.isEditClause) {
                $scope.k9.declarationClauses[clauseIndex] = $scope.cl;
            } else {
                $scope.k9.declarationClauses.push($scope.cl);
            }

            $scope.cl = {};
            $scope.isEditClause = false;
            clauseIndex = -1;
        };

        $scope.isEditClause = false;
        //var clauseIndex = -1;
        //$scope.editClause = function (index) {
        //    $scope.isEditClause = true;
        //    clauseIndex = index;
        //    $scope.showLoading = true;
        //    $scope.cl = $scope.k9.declarationClauses[index];
        //    $timeout(function () { $scope.showLoading = false; }, 500);
        //};

        $scope.deleteClause = function (index) {
            $scope.showLoading = true;
            $scope.k9.declarationClauses.splice(index, 1);
            $timeout(function () { $scope.showLoading = false; }, 500);
            $scope.cl = {};
        };

        var clauseIndex = -1;
        $scope.editClause = function (index) {
            clauseIndex = index;
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/Declaration/Templates/k9/clause.html?v=' + Utility.Version,
                controller: 'k9ClauseCntrl',
                size: 'md',
                resolve: {
                    dataObj: function () {
                        return {
                            Clause: (clauseIndex == -1 ? {} : $scope.k9.declarationClauses[index]),
                            clauseTypeList: $scope.lookUpData.clauseTypeList
                        };
                    }
                }
            });

            modalInstance.result.then(function (res) {
                if (clauseIndex != -1)
                    $scope.k9.declarationClauses[clauseIndex] = res;
                else
                    $scope.k9.declarationClauses.push(res);

                clauseIndex = -1;
            }, function (err) {
                if (!angular.isUndefined(err.statusText))
                    growl.error(err.statusText, {});
            });
        };


        var itemEntryIndex = -1;
        $scope.editItem = function (index) {
            itemEntryIndex = index;
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/Declaration/Templates/k9/item-entry.html?v=' + Utility.Version,
                controller: 'k9ItemEntryCntrl',
                windowClass: 'app-modal-window3',
                resolve: {
                    itemEntry: function () {
                        return (itemEntryIndex == -1 ? {} : $scope.k9.declarationItems[index]);
                    }
                }
            });

            modalInstance.result.then(function (res) {
                res.declarationsubItems = $.map(res.declarationsubItems, function (el) { return el });

                if (itemEntryIndex != -1)
                    $scope.k9.declarationItems[itemEntryIndex] = res;
                else
                    $scope.k9.declarationItems.push(res);

                itemEntryIndex = -1;
            }, function (err) {
                if (!angular.isUndefined(err.statusText))
                    growl.error(err.statusText, {});
            });
        };

        $scope.deleteItem = function (index) {
            $scope.k9.declarationItems.splice(index, 1);
        };

        /*
        $scope.OpenModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/Declaration/Templates/k9/item-entry.html?v=' + Utility.Version,
                controller: 'AddEditItemEntryCntrl',
                windowClass: 'app-modal-window3',
                resolve: {
                    itemEntry: function () {
                        return {};
                    }
                }
            });
    
            modalInstance.result.then(function (res) {
                $scope.k9.declarationItems.push(res);
            }, function (err) {
                growl.error(err.statusText, {});
            });
        };
        */
        $scope.GetLookUpdata();

        $scope.GenerateFile = function (declarationNo) {
            k9Service.GenerateFile(declarationNo).then(function (d) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'Js/Declaration/Templates/k9/download-file.html?v=' + Utility.Version,
                    controller: 'AddDownLoadCntrl',
                    windowClass: 'app-modal-window',
                    resolve: {
                        fileName: function () {
                            return d.data.fileName;
                        }
                    }
                });

                modalInstance.result.then(function () {

                }, function (err) {

                });
            }, function (err) {
                growl.error(err, {});
            });
        };

        $scope.back = function () {
            $location.path('/declaration/k9inquiry');
        };

        $scope.isFrmk9Valid = false;
        $scope.$watch('frmk9.$valid', function (isValid) {
            $scope.isFrmk9Valid = isValid;
        });

        $scope.Savek9Declaration = function (k9) {
            if ($scope.isFrmk9Valid) {
                $scope.showLoading = true;
                k9Service.Savek9Declaration(k9).then(function (d) {
                    $scope.showLoading = false;
                    growl.success('Declaration Saved Successfully..', {});
                }, function (err) { });
            } else {
                var error = $scope.frmk9.$error;
                angular.forEach(error.required, function (field) {
                    if (field.$invalid) {
                        var fieldName = field.$name;
                    }
                });
                growl.error('Please enter all mandatory fields', {});
            }
        };

        $scope.CustomerSelected = function (item, type, addresstype) {
            $scope.showLoading = true;
            var html = '';
            $scope.k9[type] = item.Value;
            $scope.k9[addresstype] = '';

            $scope.BindAddress(item.Value, addresstype);
        };


        $scope.BindAddress = function (agentCode, addresstype) {
            var html = '';
            AddressService.GetAddress(agentCode).then(function (d) {
                if (d.data != null) {
                    if (!angular.isUndefined(d.data.Address1))
                        html += d.data.Address1 + '<br/>';
                    if (!angular.isUndefined(d.data.Address2))
                        html += d.data.Address2 + '<br/>';
                    if (!angular.isUndefined(d.data.Address3))
                        html += d.data.Address3 + '<br/>';
                    if (!angular.isUndefined(d.data.City))
                        html += d.data.City + '<br/>';
                    if (!angular.isUndefined(d.data.State))
                        html += d.data.State + '<br/>';
                    if (!angular.isUndefined(d.data.CountryCode))
                        html += d.data.CountryCode + '<br/>';
                    if (!angular.isUndefined(d.data.ZipCode))
                        html += d.data.ZipCode;
                }
                $scope.k9[addresstype] = html;
                $scope.showLoading = false;
            }, function (err) { });
        };

        var conInfoIndex = -1;
        $scope.editConInfo = function (index) {
            conInfoIndex = index;
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/Declaration/Templates/k9/container-info.html?v=' + Utility.Version,
                controller: 'AddEditContainerInfoCntrl',
                size: 'lg',
                resolve: {
                    dataObj: function () {
                        return {
                            dc: (conInfoIndex == -1 ? {} : $scope.k9.declarationContainers[conInfoIndex]),
                            sizeList: $scope.lookUpData.sizeList,
                            jobTypeList: $scope.lookUpData.jobTypeList,
                            containerStatusList: $scope.lookUpData.containerStatusList
                        };
                    }
                }
            });

            modalInstance.result.then(function (res) {
                if (angular.isUndefined(res.Size))
                    res.Size = null;
                if (angular.isUndefined(res.EQDStatus))
                    res.EQDStatus = null;

                if (conInfoIndex != -1)
                    $scope.k9.declarationContainers[conInfoIndex] = res;
                else
                    $scope.k9.declarationContainers.push(res);

                conInfoIndex = -1;
            }, function (err) {
                if (!angular.isUndefined(err.statusText))
                    growl.error(err.statusText, {});
            });
        };


    }]);

app.controller('AddEditContainerInfoCntrl', ['$scope', '$uibModalInstance', 'OrderEntryService', 'dataObj', 'growl', function ($scope, $uibModalInstance, OrderEntryService, dataObj, growl) {
    $scope.dc = dataObj.dc;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.lookUpData = {
        sizeList: dataObj.sizeList,
        jobTypeList: dataObj.jobTypeList,
        containerStatusList: dataObj.containerStatusList
    };

    $scope.isFrmContainerInfoValid = false;
    $scope.$watch('frmContainerInfo.$valid', function (isValid) {
        $scope.isFrmContainerInfoValid = isValid;
    });
    $scope.SaveContainerInfo = function (dc) {
        if ($scope.isFrmContainerInfoValid) {
            $uibModalInstance.close(dc);
        } else {
            growl.error('Please enter all mandatory fields', {});
        }
    };

    $scope.sizeChanged = function () {
        $scope.showLoading = true;        
        OrderEntryService.GetSizeType($scope.dc.Size).then(function (d) {
            $scope.lookUpData.TypeList = d.data;
            $scope.showLoading = false;
        }, function () { })
    };

    if (!angular.isUndefined($scope.dc.Size) && $scope.dc.Size != null) {
        $scope.showLoading = true;
        OrderEntryService.GetSizeType($scope.dc.Size).then(function (d) {
            $scope.lookUpData.TypeList = d.data;
            $scope.showLoading = false;
        }, function () { })
    }
}]);
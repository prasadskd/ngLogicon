﻿app.controller('k1Cntrl', ['$scope', 'k1Service', 'MerchantProfileService', 'VesselScheduleService', 'PortAreaService', 'CurrencyRateService', 'OrderEntryService', 'VesselMasterService', 'Utility', '$uibModal', 'limitToFilter', 'growl', '$routeParams', '$timeout', '$location', 'AddressService', 'UtilityFunc', '$filter', '$route',
    function ($scope, k1Service, MerchantProfileService, VesselScheduleService, PortAreaService, CurrencyRateService, OrderEntryService, VesselMasterService, Utility, $uibModal, limitToFilter, growl, $routeParams, $timeout, $location, AddressService, UtilityFunc, $filter, $route) {
        $scope.dateFormat = UtilityFunc.DateFormat();
        $scope.timeFormat = UtilityFunc.TimeFormat();
        $scope.datetimeFormat = UtilityFunc.DateTimeFormat();
        $scope.defaultCurrency = UtilityFunc.DefaultCurrency();
        $scope.defaultCountry = UtilityFunc.DefaultCountry();
        $scope.branchID = UtilityFunc.BranchID();
        $scope.customresponses = {};
        $scope.customresponseheaderItem = {};
        $scope.IsCustomResponsesShow = false;
        $scope.transportMode = 1021;

        $scope.IsNew = true;
        $scope.showLoading = true;
        $scope.showAddBtn = true;
        $scope.showPrintBtn = false;
        var declarationNo = $routeParams.declarationNo;
        if (declarationNo != "NEW") {
            $scope.showPrintBtn = true;
            $scope.IsNew = false;
        }
        else {
            $scope.IsNew = true;
        }

        $scope.k1 = {

            OpenDate: moment(),
            declarationContainers: new Array(),
            declarationDocuments: new Array(),
            declarationClauses: new Array(),
            declarationItems: new Array(),
            declarationInvoice: {
                IsFreightCurrency: false,
                IsInsuranceCurrency: false,
                FOBAmount: 0.00,
                CIFAmount: 0.00,
                EXWAmount: 0.00,
                CNFAmount: 0.00,
                CNIAmount: 0.00,
                FreightAmount: 0.00,
                InsuranceAmount: 0.00,
                CIFCAmount: 0.00,
                PortAmountPercent: 0.00,
                FreightAmountPercent: 0.00,
                InsuranceAmountPercent: 0.00,
                OthersAmountPercent: 0.00,
                FreightAmountValue: '',
                InsuranceAmountValue: ''
            }
        };

        $scope.ic = {};//Invoice Cargo
        $scope.sd = {};//Shipment Details
        $scope.ex = {};//Excemptions
        $scope.con = {};//Container Info
        $scope.doc = {};//Documents
        $scope.cl = {};//Clause
        $scope.ie = {};//item entry
        $scope.ies = {};//item entry subitem

        $scope.dc = {};//declaration container


        debugger;
        if (!angular.isUndefined(declarationNo) && declarationNo != 'NEW') {
            k1Service.GetDeclaration(declarationNo).then(function (d) {
                $scope.k1 = d.data.declaration;
                if (!angular.isUndefined($scope.k1) && $scope.k1 != null) {
                    debugger;
                    if (!angular.isUndefined($scope.k1.declarationExcemption) && $scope.k1.declarationExcemption != null) {
                        if ($scope.k1.declarationExcemption.SalesTaxRegistrationDate == null) {
                            $scope.k1.declarationExcemption.SalesTaxRegistrationDate = undefined;
                        }
                    }
                    if (!angular.isUndefined($scope.k1.declarationInvoice) && $scope.k1.declarationInvoice != null) {
                        if ($scope.k1.declarationInvoice.InvoiceDate == null) {
                            $scope.k1.declarationInvoice.InvoiceDate = undefined;
                        }
                    }
                    if (!angular.isUndefined($scope.k1.declarationShipment) && $scope.k1.declarationShipment != null) {
                        if ($scope.k1.declarationShipment.ETADate == null) {
                            $scope.k1.declarationShipment.ETADate = undefined;
                        }
                    }
                    if (!angular.isUndefined($scope.k1.declarationDocuments) && $scope.k1.declarationDocuments != null) {
                        if ($scope.k1.declarationDocuments.DocDateType == null) {
                            $scope.k1.declarationDocuments.DocDateType = undefined;
                        }
                    }
                }
                $scope.IsCustomResponsesShow = true;
                $scope.GetCustomResponse(declarationNo);
                $scope.BindAddress(d.data.declaration.ShippingAgent, 'ShippingAgentAddress');
                $scope.Cntrl.activityList = d.data.activityList
            }, function (err) { });
        }





        $scope.GetLookUpdata = function () {
            k1Service.GetLookupData().then(function (d) {
                $scope.lookUpData = d.data;


                if (declarationNo == 'NEW') {
                    $scope.k1.TransportMode = 1021;
                    $scope.k1.declarationInvoice.LocalCurrencyCode = $scope.defaultCurrency;
                    $scope.k1.declarationInvoice.PortAmountCurrencyCode = $scope.defaultCurrency;
                    $scope.k1.declarationInvoice.FreightAmountCurrencyCode = $scope.defaultCurrency;
                    $scope.k1.declarationInvoice.InsuranceAmountCurrencyCode = $scope.defaultCurrency;
                    $scope.k1.declarationInvoice.OthersAmountCurrencyCode = $scope.defaultCurrency;
                    $scope.k1.declarationInvoice.PayCountry = $scope.defaultCountry;
                }

                $scope.showLoading = false;
            }, function (err) { });
        };

        OrderEntryService.GetTerminalList().then(function (d) {
            debugger;
            $scope.terminalList = d.data.terminalList;
        }, function (err) { });



        $scope.PayCountryChanged = function (item, type) {
            $scope.k1.declarationInvoice[type] = item.text;
        };

        $scope.GenericMerchantResults = function (text, filter) {
            return MerchantProfileService.SearchMerchantResults(text, filter).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.ChangeTransportMode = function (TransportMode) {
            debugger;
            $scope.transportMode = TransportMode;
        };

        $scope.GenericMerchantResults = function (text, filter, type) {
            return MerchantProfileService.SearchMerchantResults(text, filter).then(function (d) {
                if (d.data.length == 0)
                    $scope.k1[type] = '';
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.ShipCallResults = function (text) {
            return VesselScheduleService.ShipCallNoSearch(text).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.VoyageNoInwardResults = function (text) {
            debugger;
            return VesselScheduleService.VoyageNoInWardSearch(text).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.VoyageSelected = function (item) {
            $scope.k1.declarationShipment.VoyageNo = item.VoyageNoInWard;
            $scope.k1.declarationShipment.SCNNo = item.ShipCallNo;
            $scope.k1.declarationShipment.VesselID = item.VesselID;
            $scope.k1.declarationShipment.VesselName = item.VesselName;
            $scope.k1.declarationShipment.ETADate = item.ETA;

        };

        $scope.ShipCallNoSelect = function (item) {
            $scope.k1.declarationShipment.VoyageNo = item.VoyageNoInWard;
            $scope.k1.declarationShipment.VesselID = item.VesselID;
            $scope.k1.declarationShipment.VesselName = item.VesselName;
            $scope.k1.declarationShipment.ETADate = item.ETA;
        };

        $scope.VoyageNoInwardSelect = function (item) {
            $scope.k1.declarationShipment.SCNNo = item.ShipCallNo;
            $scope.k1.declarationShipment.VesselID = item.VesselID;
            $scope.k1.declarationShipment.VesselName = '';
            $scope.k1.declarationShipment.ETADate = item.ETA;
        };

        $scope.MerchantSelected = function (item, Type) {
            $scope.k1[Type] = item.Value
        };

        $scope.PortAutoComplete = function (text) {
            return PortAreaService.PortAutoComplete(text).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.PortSelected = function (item, type) {
            $scope.k1.declarationShipment[type] = item.PortCode;
        };

        $scope.exRateChanged = function (currencyCode, field) {
            $scope.showLoading = true;
            CurrencyRateService.GetExRate($scope.k1.declarationInvoice[currencyCode]).then(function (d) {
                $scope.k1.declarationInvoice[field] = d.data;
                $scope.showLoading = false;
            }, function (err) { });
        };

        $scope.TransactionType = function (item, field) {
            $scope.k1[field] = item.Code;
        };

        $scope.sizeChanged = function () {
            $scope.showLoading = true;
            OrderEntryService.GetSizeType($scope.dc.Size).then(function (d) {
                $scope.lookUpData.TypeList = d.data;
                $scope.showLoading = false;
            }, function () { })
        };

        $scope.AddDeclarationContainer = function () {
            $scope.k1.declarationContainers.push($scope.dc);
            $scope.dc = {};
        };





        /*shipment details*/
        $scope.VesselNameResults = function (text) {
            return VesselMasterService.GetVesselByVesselName(text).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.vesselNameClick = function (obj) {
            $scope.k1.declarationShipment.VesselID = obj.Value;
        };

        $scope.MerchantSelected2 = function (item, Type) {
            $scope.k1.declarationShipment[Type] = item.Value
        };

        $scope.editContainer = function (index) {
            $scope.showLoading = true;
            $scope.dc = $scope.k1.declarationContainers[index];
            $scope.sizeChanged();
            $timeout(function () { $scope.showLoading = false; }, 500);
        };

        $scope.deleteContainer = function (index) {
            $scope.k1.declarationContainers.splice(index, 1);
        };

        $scope.AddDeclarationDocuments = function (doc) {
            if ($scope.isEditDoc) {
                $scope.k1.declarationDocuments[documentIndex] = $scope.doc;
            } else {
                $scope.k1.declarationDocuments.push($scope.doc);
            }
            $scope.doc = {};
            $scope.isEditDoc = false;
            documentIndex = -1;
        };

        $scope.isEditDoc = false;
        //var documentIndex = -1;
        //$scope.editDocument = function (index) {
        //    documentIndex = index;
        //    $scope.isEditDoc = true;
        //    $scope.showLoading = true;
        //    $scope.doc = $scope.k1.declarationDocuments[index];
        //    if ($scope.doc.DocDate == null)
        //        $scope.doc.DocDate = undefined;
        //    $timeout(function () { $scope.showLoading = false; }, 500);
        //};

        var DocIndex = -1;
        $scope.editDocument = function (index) {
            DocIndex = index;
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/Declaration/Templates/k1/document.html?v=' + Utility.Version,
                controller: 'AddEditk1DocumentCntrl',
                size: 'lg',
                resolve: {
                    dataObj: function () {
                        return {
                            Document: (DocIndex == -1 ? {} : $scope.k1.declarationDocuments[index]),
                            docDateTypeList: $scope.lookUpData.docDateTypeList,
                            OGACodeList: $scope.lookUpData.OGACodeList,
                            customStationCodeList: $scope.lookUpData.customStationCodeList,
                        };
                    }
                }
            });

            modalInstance.result.then(function (res) {

                if (DocIndex != -1)
                    $scope.k1.declarationDocuments[DocIndex] = res;
                else {
                    $scope.k1.declarationDocuments.push(res);
                }

                DocIndex = -1;
            }, function (err) {
                if (!angular.isUndefined(err.statusText))
                    growl.error(err.statusText, {});
            });
        };


        $scope.deleteDocument = function (index) {
            $scope.showLoading = true;
            $scope.k1.declarationDocuments.splice(index, 1);
            $timeout(function () { $scope.showLoading = false; }, 500);
            $scope.doc = {};
        };

        $scope.AddDeclarationClause = function () {
            if ($scope.isEditClause) {
                $scope.k1.declarationClauses[clauseIndex] = $scope.cl;
            } else {
                $scope.k1.declarationClauses.push($scope.cl);
            }

            $scope.cl = {};
            $scope.isEditClause = false;
            clauseIndex = -1;
        };

        $scope.isEditClause = false;
        //var clauseIndex = -1;
        //$scope.editClause = function (index) {
        //    $scope.isEditClause = true;
        //    clauseIndex = index;
        //    $scope.showLoading = true;
        //    $scope.cl = $scope.k1.declarationClauses[index];
        //    $timeout(function () { $scope.showLoading = false; }, 500);
        //};

        $scope.deleteClause = function (index) {
            $scope.showLoading = true;
            $scope.k1.declarationClauses.splice(index, 1);
            $timeout(function () { $scope.showLoading = false; }, 500);
            $scope.cl = {};
        };

        var clauseIndex = -1;
        $scope.editClause = function (index) {
            clauseIndex = index;
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/Declaration/Templates/k1/clause.html?v=' + Utility.Version,
                controller: 'AddEditk1ClauseCntrl',
                size: 'md',
                resolve: {
                    dataObj: function () {
                        return {
                            Clause: (clauseIndex == -1 ? {} : $scope.k1.declarationClauses[index]),
                            clauseTypeList: $scope.lookUpData.clauseTypeList
                        };
                    }
                }
            });

            modalInstance.result.then(function (res) {
                if (clauseIndex != -1)
                    $scope.k1.declarationClauses[clauseIndex] = res;
                else
                    $scope.k1.declarationClauses.push(res);

                clauseIndex = -1;
            }, function (err) {
                if (!angular.isUndefined(err.statusText))
                    growl.error(err.statusText, {});
            });
        };


        var itemEntryIndex = -1;
        $scope.editItem = function (index) {

            var _CIFC = 0.00;
            if ($scope.k1.declarationInvoice != null && $scope.k1.declarationInvoice.CIFCAmount)
                _CIFC = $scope.k1.declarationInvoice.CIFCAmount;

            itemEntryIndex = index;
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/Declaration/Templates/k1/item-entry.html?v=' + Utility.Version,
                controller: 'AddEditItemEntryCntrl',
                windowClass: 'app-modal-window3',
                resolve: {
                    dataObj: function () {
                        return {
                            itemEntry: (itemEntryIndex == -1 ? {} : $scope.k1.declarationItems[index]),
                            CIFC: _CIFC
                        }
                    }
                }
            });

            modalInstance.result.then(function (res) {
                res.declarationsubItems = $.map(res.declarationsubItems, function (el) { return el });

                if (itemEntryIndex != -1)
                    $scope.k1.declarationItems[itemEntryIndex] = res;
                else
                    $scope.k1.declarationItems.push(res);

                itemEntryIndex = -1;
            }, function (err) {
                if (!angular.isUndefined(err.statusText))
                    growl.error(err.statusText, {});
            });
        };

        $scope.deleteItem = function (index) {
            $scope.k1.declarationItems.splice(index, 1);
        };

        /*
        $scope.OpenModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/Declaration/Templates/k1/item-entry.html?v=' + Utility.Version,
                controller: 'AddEditItemEntryCntrl',
                windowClass: 'app-modal-window3',
                resolve: {
                    itemEntry: function () {
                        return {};
                    }
                }
            });
    
            modalInstance.result.then(function (res) {
                $scope.k1.declarationItems.push(res);
            }, function (err) {
                growl.error(err.statusText, {});
            });
        };
        */
        $scope.GetLookUpdata();
        $scope.btncustom = false;
        $scope.GenerateFile = function (declarationNo) {
            $scope.btncustom = true;            
            k1Service.GenerateFile(declarationNo).then(function (d) {
                $scope.btncustom = false;
                $scope.customresponses = d.data;
                $scope.active = 8;
                growl.success('Submitted to Customs successfully..!', {});
            }, function (err) { });           
        };

        $scope.back = function () {
            $location.path('/declaration/k1inquiry');
        };

        $scope.clear = function () {
            $route.reload('/declaration/k1declaration/NEW');
        };

        $scope.refersh = function () {
            debugger; $location.path('/declaration/k1inquiry');
            $route.reload();
        }

        $scope.isFrmK1Valid = false;
        $scope.$watch('frmk1.$valid', function (isValid) {
            $scope.isFrmK1Valid = isValid;
        });

        $scope.ValidatePorts = function () {
            debugger;
            if ($scope.k1.TransportMode == 1021) {
                if ($scope.k1.declarationShipment.LoadingPort == $scope.k1.declarationShipment.DischargePort) {
                    growl.error('Loading Port and Discharge Port can not be the same', {});
                    return false;
                }
                else if ($scope.k1.declarationShipment.LoadingPort == $scope.k1.declarationShipment.TranshipmentPort) {
                    growl.error('Loading Port and Transhipment Port can not be the same', {});
                    return false;
                }
                else if ($scope.k1.declarationShipment.DischargePort == $scope.k1.declarationShipment.TranshipmentPort) {
                    growl.error('Transhipment Port and Discharge Port can not be the same', {});
                    return false;
                }
                else
                    return true;
            }
            else
                return true;
        };
        $scope.Savek1Declaration = function (k1) {
            if ($scope.isFrmK1Valid) {
                if ($scope.ValidatePorts()) {
                    $scope.showLoading = true;
                    k1Service.Savek1Declaration(k1).then(function (d) {
                        $scope.showLoading = false;
                        growl.success('Declaration Saved Successfully..', {});

                        $scope.k1.DeclarationNo = d.data.declarationNo;
                        $scope.k1.OrderNo = d.data.orderNo;
                        debugger;
                    }, function (err) { });
                }
            } else {
                var error = $scope.frmk1.$error;
                angular.forEach(error.required, function (field) {
                    if (field.$invalid) {
                        var fieldName = field.$name;
                    }
                });
                growl.error('Please enter all mandatory fields', {});
            }
        };

        $scope.CustomerSelected = function (item, type, addresstype) {
            $scope.showLoading = true;
            var html = '';
            $scope.k1[type] = item.Value;
            $scope.k1[addresstype] = '';

            $scope.BindAddress(item.Value, addresstype);
        };


        $scope.BindAddress = function (agentCode, addresstype) {
            var html = '';
            AddressService.GetAddress(agentCode).then(function (d) {
                if (d.data != null) {
                    if (!angular.isUndefined(d.data.Address1))
                        html += d.data.Address1 + '<br/>';
                    if (!angular.isUndefined(d.data.Address2))
                        html += d.data.Address2 + '<br/>';
                    if (!angular.isUndefined(d.data.Address3))
                        html += d.data.Address3 + '<br/>';
                    if (!angular.isUndefined(d.data.City))
                        html += d.data.City + '<br/>';
                    if (!angular.isUndefined(d.data.State))
                        html += d.data.State + '<br/>';
                    if (!angular.isUndefined(d.data.CountryCode))
                        html += d.data.CountryCode + '<br/>';
                    if (!angular.isUndefined(d.data.ZipCode))
                        html += d.data.ZipCode;
                }
                $scope.k1[addresstype] = html.toUpperCase();
                $scope.showLoading = false;
            }, function (err) { });
        };

        var conInfoIndex = -1;
        $scope.editConInfo = function (index) {
            conInfoIndex = index;
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/Declaration/Templates/k1/container-info.html?v=' + Utility.Version,
                controller: 'AddEditContainerInfoCntrl',
                size: 'lg',
                resolve: {
                    dataObj: function () {
                        return {
                            dc: (conInfoIndex == -1 ? {} : $scope.k1.declarationContainers[conInfoIndex]),
                            sizeList: $scope.lookUpData.sizeList,
                            jobTypeList: $scope.lookUpData.jobTypeList,
                            containerStatusList: $scope.lookUpData.containerStatusList
                        };
                    }
                }
            });

            modalInstance.result.then(function (res) {
                if (angular.isUndefined(res.Size))
                    res.Size = null;
                if (angular.isUndefined(res.EQDStatus))
                    res.EQDStatus = null;

                if (conInfoIndex != -1)
                    $scope.k1.declarationContainers[conInfoIndex] = res;
                else
                    $scope.k1.declarationContainers.push(res);

                conInfoIndex = -1;
            }, function (err) {
                if (!angular.isUndefined(err.statusText))
                    growl.error(err.statusText, {});
            });
        };
        $scope.GenerateReportPDF = function () {
            k1Service.GenerateReportPDF($scope.branchID, declarationNo);
        };

        $scope.GenerateReport = function (name, reportUrl, reportID) {
            if (true) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'Js/Reports/Templates/Report.html?v=' + Utility.Version,
                    controller: 'ReportCntrl',
                    windowClass: 'app-modal-window2',
                    resolve: {
                        reportObj: function () {
                            return {
                                reportId: reportID,
                                reportName: name,
                                branchID: $scope.branchID,
                                declarationNo: declarationNo,
                                Url: '/1TradeReports/' + reportUrl
                            }
                        }
                    }
                });

                modalInstance.result.then(function (d) {

                }, function () {

                });
            }
            else {
                growl.error('Please enter all mandatory fields', {});
            }
        };

        $scope.IncoTermChanged = function () {
            debugger;
            $scope.k1.declarationInvoice.FreightAmountValue = $scope.k1.declarationInvoice.InsuranceAmountValue = $scope.k1.declarationInvoice.IncoTerm;
            var isFreightCurrency = (angular.isUndefined($scope.k1.declarationInvoice.IsFreightCurrency) ? false : $scope.k1.declarationInvoice.IsFreightCurrency);
            var isInsuranceCurrency = (angular.isUndefined($scope.k1.declarationInvoice.IsInsuranceCurrency) ? false : $scope.k1.declarationInvoice.IsInsuranceCurrency);
            $scope.IncoTermCalculation(isFreightCurrency, isInsuranceCurrency);
        };

        $scope.IsFreightCurrencyChecked = function (flag) {
            $scope.IncoTermCalculation(!flag, $scope.k1.declarationInvoice.IsInsuranceCurrency);
        };

        $scope.IsInsuranceCurrencyChecked = function (flag) {
            $scope.IncoTermCalculation($scope.k1.declarationInvoice.IsFreightCurrency, !flag);
        };
        $scope.IncoTermCalculation = function (IsFreightCurrency, IsInsuranceCurrency) {

            var incoTerm = parseFloat(angular.isUndefined($scope.k1.declarationInvoice.IncoTerm) ? 0 : $scope.k1.declarationInvoice.IncoTerm);
            var freight = parseFloat(angular.isUndefined($scope.k1.declarationInvoice.FreightAmountPercent) ? 0 : $scope.k1.declarationInvoice.FreightAmountPercent);
            var insurance = parseFloat(angular.isUndefined($scope.k1.declarationInvoice.InsuranceAmountPercent) ? 0 : $scope.k1.declarationInvoice.InsuranceAmountPercent);
            var port = parseFloat(angular.isUndefined($scope.k1.declarationInvoice.PortAmountPercent) ? 0 : $scope.k1.declarationInvoice.PortAmountPercent);
            var otherCharges = parseFloat(angular.isUndefined($scope.k1.declarationInvoice.OthersAmountPercent) ? 0 : $scope.k1.declarationInvoice.OthersAmountPercent);
            var invoiceValue = parseFloat(angular.isUndefined($scope.k1.declarationInvoice.InvoiceValue) ? 0 : $scope.k1.declarationInvoice.InvoiceValue);
            if (incoTerm == 0)
                return;

            var obj = {
                incoTerm: incoTerm,
                freight: freight,
                insurance: insurance,
                port: port,
                otherCharges: otherCharges,
                invoiceValue: invoiceValue,
                FOB: $scope.k1.declarationInvoice.FOBAmount,
                CIF: $scope.k1.declarationInvoice.CIFAmount,
                EXW: $scope.k1.declarationInvoice.EXWAmount,
                CNF: $scope.k1.declarationInvoice.CNFAmount,
                CNI: $scope.k1.declarationInvoice.CNIAmount,
                IsFreightCurrency: IsFreightCurrency,
                IsInsuranceCurrency: IsInsuranceCurrency
            };

            //var obj = {
            //    InvoiceValue: $scope.k1.declarationInvoice.InvoiceValue,
            //    InvoiceExchangeRate:$scope.k1.declarationInvoice.CurrencyExRate,
            //    InvoiceValueIncoterm: incoTerm,
            //    FreightValue: freight,
            //    FreightValueType: insurance,
            //    FreightExchangeRate:$scope.k1.declarationInvoice.FreightAmountExRate,
            //    FreightGrossWeight: '',
            //    InsuranceValue: $scope.k1.declarationInvoice.InsuranceAmountPercent,
            //    InsuranceValueType: '',
            //    InsuranceValueIncoterm: '',
            //    InsuranceExchangeRate: '',
            //    OtherValue: '',
            //    OtherValueType: '',
            //    OtherExchangeRate: '',
            //    PortValue: '',
            //    PortValueType: '',
            //    PortExchangeRate: '',
            //    GrossWeight: '',
            //    StatisticalQuantity: '',
            //    StatisticalUOM: '',
            //    DeclaredQuantity: '',
            //    DeclaredUOM: '',
            //    TotalItem: '',
            //    ItemAmount: '',
            //    ImportDutyMethod: '',
            //    ImportDutyTariffCodeType: '',
            //    ImportDutyPercentage: '',
            //    ImportDutyPercentageRateExempted: '',
            //    ImportDutySpecific: '',
            //    ImportDutySpecificRateExempted: '',
            //    ExciseDutyMethod: '',
            //    ExciseDutyTariffCodeType: '',
            //    ExciseDutyPercentage: '',
            //    ExciseDutyPercentageRateExempted: '',
            //    ExciseDutySpecific: '',
            //    ExciseDutySpecificRateExempted: '',
            //    SalesTaxDutyMethod: '',
            //    SalesTaxTariffCodeType: '',
            //    SalesTaxPercentage: '',
            //    SalesTaxPercentageRateExempted: '',
            //    SalesTaxSpecific: '',
            //    SalesTaxSpecificRateExempted: '',
            //    GoodsServicesTaxPercentage: '',
            //    GoodsServicesTaxPercentageRateExempted: '',
            //    AntiDumpingMethod: '',
            //    AntiDumpingTariffCodeType: '',
            //    AntiDumpingPercentage: '',
            //    AntiDumpingPercentageRateExempted: '',
            //    AntiDumpingSpecific: '',
            //    AntiDumpingSpecificRateExempted: '',
            //    ExportDutyMethod: '',
            //    ExportDutyTariffCodeType: '',
            //    ExportDutyPercentage: '',
            //    ExportDutyPercentageRateExempted: '',
            //    ExportDutySpecific: '',
            //    ExportDutySpecificRateExempted: '',
            //    GazettedMethod: '',
            //    GazettedTariffCodeType: '',
            //    GazettedPercentage: '',
            //    GazettedPercentageRateExempted: '',
            //    GazettedSpecific: '',
            //    GazettedSpecificRateExempted: '',
            //    GazettedUnitPrice: '',
            //    ReplantMethod: '',
            //    ReplantTariffCodeType: '',
            //    ReplantPercentage: '',
            //    ReplantPercentageRateExempted: '',
            //    ReplantSpecific: '',
            //    ReplantSpecificRateExempted: '',
            //    ResearchMethod: '',
            //    ResearchTariffCodeType: '',
            //    ResearchPercentage: '',
            //    ResearchPercentageRateExempted: '',
            //    ResearchSpecific: '',
            //    ResearchSpecificRateExempted: '',
            //    FinishGoodsMethod: '',
            //    FinishGoodsTariffCodeType: '',
            //    FinishGoodsPercentage: '',
            //    FinishGoodsPercentageRateExempted: '',
            //    FinishGoodsSpecific: '',
            //    FinishGoodsSpecificRateExempted: ''
            //};

            $scope.showLoading = true;
            k1Service.OutPutFOBCIF(obj).then(function (d) {
                $scope.k1.declarationInvoice.FOBAmount = d.data.FOB;
                $scope.k1.declarationInvoice.CIFAmount = d.data.CIF;
                $scope.k1.declarationInvoice.EXWAmount = d.data.EXW;
                $scope.k1.declarationInvoice.CNFAmount = d.data.CNF;
                $scope.k1.declarationInvoice.CNIAmount = d.data.CNI;
                $scope.k1.declarationInvoice.FreightAmount = d.data.freight;
                $scope.k1.declarationInvoice.InsuranceAmount = d.data.insurance;
                $scope.k1.declarationInvoice.CIFCAmount = d.data.CIFC;
                $scope.showLoading = false;
            }, function (err) {
                debugger;
            });
        };

        $scope.GetCustomResponse = function (declarationNo) {
            k1Service.GetCustomResponse(declarationNo).then(function (d) {
                $scope.customresponses = d.data.customResponse;
            }, function (err) { });
        };

        $scope.DisplayResponseData = function (obj) {
            k1Service.GetCustomResponseInfo(obj).then(function (d) {
                $scope.customresponseheaderItem = d.data;
            }, function (err) { });
        };

        $scope.IsCloneBtnDisabled = false;
        $scope.CloneK1 = function (declarationNo) {
            $scope.showLoading = true;
            k1Service.CloneDeclaration(declarationNo).then(function (d) {
                $scope.IsCloneBtnDisabled = true;
                $scope.k1 = d.data.declaration;
                $scope.showLoading = false;
                $scope.IsNew = true;
                $scope.customresponses = null;
                $scope.active = 0;
            }, function (err) { });
        };

    }]);

app.controller('AddEditContainerInfoCntrl', ['$scope', '$uibModalInstance', 'OrderEntryService', 'dataObj', 'growl', function ($scope, $uibModalInstance, OrderEntryService, dataObj, growl) {
    $scope.dc = dataObj.dc;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.lookUpData = {
        sizeList: dataObj.sizeList,
        jobTypeList: dataObj.jobTypeList,
        containerStatusList: dataObj.containerStatusList
    };

    $scope.isFrmContainerInfoValid = false;
    $scope.$watch('frmContainerInfo.$valid', function (isValid) {
        $scope.isFrmContainerInfoValid = isValid;
    });
    $scope.SaveContainerInfo = function (dc) {
        if ($scope.isFrmContainerInfoValid) {
            $uibModalInstance.close(dc);
        } else {
            growl.error('Please enter all mandatory fields', {});
        }
    };

    $scope.sizeChanged = function () {
        $scope.showLoading = true;
        OrderEntryService.GetSizeType($scope.dc.Size).then(function (d) {
            $scope.lookUpData.TypeList = d.data;
            $scope.showLoading = false;
        }, function () { })
    };

    if (!angular.isUndefined($scope.dc.Size) && $scope.dc.Size != null) {
        $scope.showLoading = true;
        OrderEntryService.GetSizeType($scope.dc.Size).then(function (d) {
            $scope.lookUpData.TypeList = d.data;
            $scope.showLoading = false;
        }, function () { })
    }
}]);
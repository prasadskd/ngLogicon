﻿app.controller('k3ItemEntryCntrl', ['$scope', '$uibModalInstance', 'ItemEntryService', 'limitToFilter', '$filter', 'itemEntry', 'growl', 'UtilityFunc',
function ($scope, $uibModalInstance, ItemEntryService, limitToFilter, $filter, itemEntry, growl, UtilityFunc) {
    debugger;
    //$scope.ie = {
    //    declarationsubItems: new Array()
    //};
    $scope.defaultCountry = UtilityFunc.DefaultCountry();

    $scope.ie = itemEntry;
    $scope.showLoading = true;
    $scope.HSCodeChange = function (hsCode) {
        var description = $filter('filter')($scope.lookUpData.hsCodeList, { Value: hsCode })[0].Text;
        if (description.indexOf('PETROLEUM') >= 0) {
            $scope.showPetrolPnl = true;
        } else {
            $scope.showPetrolPnl = false;
        }
    };

    $scope.GetLookupData = function () {
        ItemEntryService.GetLookupData().then(function (d) {
            $scope.lookUpData = d.data;
            debugger;
            if (itemEntry.OriginCountryCode == null)
                $scope.ie.OriginCountryCode = $scope.defaultCountry;

            $scope.HSCodeChange($scope.ie.HSCode);
            $scope.showLoading = false;
        }, function (err) { });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.showPetrolPnl = false;


    $scope.IsFrmItemEntryValid = false;
    $scope.$watch('frmItemEntry.$valid', function (isValid) {
        $scope.IsFrmItemEntryValid = isValid;
    });

    $scope.SaveItemEntry = function (ie) {
        if ($scope.IsFrmItemEntryValid) {
            $uibModalInstance.close(ie);
        }
        else {
            growl.error('Please enter all mandatory fields', {});
        }
    };

    $scope.GetLookupData();
}]);
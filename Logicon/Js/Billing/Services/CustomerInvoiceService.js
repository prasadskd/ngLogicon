﻿app.service('CustomerInvoiceService', ['$http', '$q', 'Utility', function ($http, $q, Utility) {
    
    
    this.GetLookupData = function () {
 
        var deferred = $q.defer();
        $http.get(Utility.ServiceUrl + '/Billing/CustomerInvoice/lookup').then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

   
    this.SaveCustomerInvoice = function (ci) {
        debugger;
        var deferred = $q.defer();
        $http.post(Utility.ServiceUrl + '/Billing/CustomerInvoice/save', JSON.stringify(ci)).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.GetExchangeRate = function (currCode)
    {
       
        var deferred = $q.defer();
        $http.get(Utility.ServiceUrl + '/Billing/CustomerInvoice/getExchangeRate/' + currCode).then(function (res) {
                deferred.resolve(res);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
    }


    this.GetList = function () {
        debugger;
        var deferred = $q.defer();
        $http.get(Utility.ServiceUrl + '/Billing/CustomerInvoice/list').then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };
    
}]);
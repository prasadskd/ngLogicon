﻿app.controller('CustomerCashBillCntrl', ['$scope', '$uibModal', 'CustomerCashBillService', 'CustomerInvoiceService', 'growl', 'DataTransferService', '$routeParams', 'Utility',
    function ($scope, $uibModal, CustomerCashBillService, CustomerInvoiceService, growl, DataTransferService, $routeParams, Utility) {
     

        $scope.ccb = {};
        $scope.ccb.InvoiceDate = new Date();
        var customerCashBillIndex = -1;
        $scope.customercashbillDetails = function (index) {
            debugger;
            customerCashBillIndex = index;
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/Billing/Templates/CustomerCashBill/add-customercash-bill.html?v=' + Utility.Version,
                controller: 'addEditCustomerCashBillCntrl',
                size: 'lg',
                resolve: {
                    customerCashBillItemObj: function () {
                        return {
                            customerCashBillItem: customerCashBillIndex != -1 ? $scope.ccb.customerCashBillList[customerCashBillIndex] : "",
                            currencyCode: $scope.ccb.CurrencyCode != "" ? $scope.ccb.CurrencyCode : ""
                        }
                    }
                }
            });

            modalInstance.result.then(function (cb) {
                debugger;
                if (customerCashBillIndex == -1) {
                    if ($scope.ccb.customerCashBillList != null) {
                        $scope.ccb.customerCashBillList.push(cb);
                    }
                    else {
                        $scope.ccb.customerCashBillList = new Array();
                        $scope.ccb.customerCashBillList.push(cb);
                    }
                }
                else {
                    $scope.ccb.customerCashBillList[customerCashBillIndex] = cb;
                }

            }, function () {

            });
        };

        /* datepicker configuration */
        $scope.formats = ['dd/MM/yyyy'];
        $scope.format = $scope.formats[0];

        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.popup = {
            InvoiceDate: false,
        };

        $scope.open = function (id) {
            $scope.popup[id] = true;
        };
        /* datepicker configuration */

        CustomerCashBillService.GetLookupData().then(function (d) {
            $scope.lookupData = d.data;
        }, function (err) { });

        $scope.SaveCustomerCashBill = function () {
            CustomerCashBillService.Save($scope.ccb).then(function (d) {
                growl.success(d.data, {});
            }, function (err) { })
        };
                      

        $scope.SelectCurrencyCode = function (curCode) {

            CustomerInvoiceService.GetExchangeRate(curCode).then(function (d) {

            }, function (err) { });
        }

        $scope.ccb.customerCashBillList = [];

        /* datepicker configuration */
        $scope.formats = ['dd/MM/yyyy'];
        $scope.format = $scope.formats[0];

        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.popup = {
            InvoiceDate: false,
        };

        $scope.open = function (id) {
            $scope.popup[id] = true;
        };
        /* datepicker configuration */

        function CalcTaxAmount() {
            var totalTaxAmount = 0.0;
            var totalAmount = 0.0;
            if ($scope.ccb.customerCashBillList.length > 0) {
                angular.forEach($scope.ccb.customerCashBillList, function (item, index) {
                    totalTaxAmount += parseFloat(item.TaxAmount);
                    totalAmount += parseFloat(item.TotalAmount);
                });
                $scope.ccb.TaxAmount = totalTaxAmount.toFixed(2);
                $scope.ccb.TotalAmount = totalAmount.toFixed(2);
            }
            else {
                $scope.ccb.TaxAmount = totalTaxAmount.toFixed(2);
                $scope.ccb.TotalAmount = totalAmount.toFixed(2);
            }

        }

        CalcTaxAmount();
        //var chkArr = DataTransferService.GetData();

        //if (chkArr.length > 0) {
        //    CustomerCashBillService.getDataByContainerKeys(chkArr).then(function (d) {
        //        $scope.cb = d.data;
        //        $scope.cb.InvoiceDate = new Date();
        //    }, function (err) { });
        //}

        //var type = $routeParams.type;
        //$scope.pageTitle = 'Customer Cash Bill';
        //if (type == 1)
        //    $scope.pageTitle = 'Customer Invoice';
        //else if (type == 2)
        //    $scope.pageTitle = 'Cash Bill';
    }]);




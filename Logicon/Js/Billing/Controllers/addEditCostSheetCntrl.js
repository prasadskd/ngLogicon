﻿app.controller('addEditCostSheetCntrl', ['$scope', '$uibModalInstance', 'CostSheetService', 'CustomerInvoiceService', 'costSheetItem', 'ChargeCodeService', 'limitToFilter', function ($scope, $uibModalInstance, CostSheetService, CustomerInvoiceService, costSheetItem, ChargeCodeService, limitToFilter) {


    var taxRate = 0.0;
    var excRate = 0.0;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $scope.isfrmCustomerInvoice = false;
    };


    $scope.cs = costSheetItem;
    $scope.isfrmCustomerInvoice = false;
    $scope.$watch('frmCustomerInvoice.$valid', function (isValid) {
        $scope.isfrmCustomerInvoice = isValid;
    });

    $scope.SaveCoseSheet = function (cs) {
        $uibModalInstance.close(cs);
    };

    $scope.GetLookupData = function (OrderNo) {
        CostSheetService.GetLookupData(OrderNo).then(function (d) {
            $scope.lookupData = d.data;
        }, function (err) {

        });
    };

    $scope.ChargeCodeResults = function (text) {

        return ChargeCodeService.GetChargeCodeSearch(text).then(function (d) {
            return limitToFilter(d.data);
        }, function (err) { });
    };

    $scope.GetLookupData(costSheetItem.OrderNo);


    $scope.ChargeCodeSelected = function (item, type) {
        debugger;
        if (type == 'ChargeCode') {
            ChargeCodeService.GetGSTValues(item.Value).then(function (d) {
                debugger;
                $scope.cs.OutPutGSTDescription = d.data.outputGSTCodeDetails[0].GSTCodeDescription;
                $scope.cs.InPutGSTDescription = d.data.inputGSTCodeDetails[0].GSTCodeDescription;
                taxRate = parseFloat(d.data.taxRate);

            }, function (err) { });
        }
    };

    $scope.SelectCurrencyCode = function (curCode) {

        CustomerInvoiceService.GetExchangeRate(curCode).then(function (d) {
            excRate = d.data;
            $scope.CaliculateAmounts();
            //$scope.lookupData = d.data;
        }, function (err) { });
    }


    $scope.CaliculateAmounts = function () {

        if ($scope.cs.Price != null && $scope.cs.Qty != "") {
            $scope.cs.TaxAmount = ($scope.cs.Price * $scope.cs.Qty * (taxRate / 100)).toFixed(2);
            $scope.cs.ExRate = ($scope.cs.Price * $scope.cs.Qty * excRate).toFixed(2);
            $scope.cs.LocalAmount = ($scope.cs.Price * $scope.cs.Qty).toFixed(2);
            $scope.cs.TotalAmount = ($scope.cs.Price * $scope.cs.Qty) + ($scope.cs.Price * $scope.cs.Qty * (taxRate / 100));
            $scope.cs.ActualAmount = ($scope.cs.Price * $scope.cs.Qty) + ($scope.cs.Price * $scope.cs.Qty * (taxRate / 100));
        }
    }

    $scope.CalDiscountAmt = function () {
      
        if ($scope.cs.DiscountType != "" && $scope.cs.DiscountAmount != "") {
            var disAmount = 0.0;
            
            if ($scope.cs.DiscountType == "1170") {
                disAmount = parseFloat($scope.cs.DiscountAmount);
                $scope.cs.TotalAmount = ($scope.cs.Price * $scope.cs.Qty) + ($scope.cs.Price * $scope.cs.Qty * (taxRate / 100)) - disAmount;
                $scope.cs.ActualAmount = ($scope.cs.Price * $scope.cs.Qty) + ($scope.cs.Price * $scope.cs.Qty * (taxRate / 100)) - disAmount;
            } else {
                var disAmount = parseFloat($scope.cs.Price * $scope.cs.Qty*($scope.cs.DiscountAmount / 100));
                $scope.cs.TotalAmount = ($scope.cs.Price * $scope.cs.Qty) + ($scope.cs.Price * $scope.cs.Qty * (taxRate / 100)) - disAmount;
                $scope.cs.ActualAmount = ($scope.cs.Price * $scope.cs.Qty) + ($scope.cs.Price * $scope.cs.Qty * (taxRate / 100)) - disAmount;
            }
        }
    }

}]);
﻿app.controller('GenerateReceiptCntrl', ['$scope', '$uibModal', '$routeParams', 'PaymentService', 'CustomerInvoiceService', 'growl', '$timeout', '$location', function ($scope, $uibModal, $routeParams, PaymentService, CustomerInvoiceService, growl, $timeout, $location) {
    var paymentNo = $routeParams.paymentno;
    var statementNo = $routeParams.statementno;
    $scope.showLoading = true;
    $scope.gi = {};
    if (!angular.isUndefined(paymentNo) && !angular.isUndefined(statementNo)) {
        PaymentService.GetPayment(paymentNo, statementNo).then(function (d) {
            $scope.showLoading = false;
            $scope.gi = d.data;
            console.log(JSON.stringify(d.data));
        }, function (err) { });
    }

    $scope.ApprovePayment = function (branchId, statementNo, paymentNo) {
        $scope.showLoading = true;
        PaymentService.ApprovePayment(branchId, paymentNo, statementNo).then(function (d) {
            $scope.showLoading = false;
            $timeout(function () {
                $location.path('/operation/subscriberslist');
            }, 500);
            growl.success('Payment Approved Successfully', {});
        }, function (err) { });
    };

    $scope.DeletePayment = function (branchId, statementNo, paymentNo) {
        $scope.showLoading = true;
        PaymentService.DeletePayment(branchId, paymentNo, statementNo).then(function (d) {
            $scope.showLoading = false;
            growl.success('Payment Deleted Successfully', {});
        }, function (err) { });
    };    

    

    CustomerInvoiceService.GetLookupData().then(function (d) {
        $scope.lookupData = d.data;
        $scope.showLoading = false;
    }, function (err) { });
}]);

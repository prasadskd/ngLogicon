﻿app.controller('GenerateInvoiceCntrl', ['$scope', '$uibModal', 'CustomerInvoiceService', '$routeParams', 'DataTransferService', '$location', 'Utility', 'limitToFilter', '$http', 'UtilityFunc', 'PaymentService', 'growl', 'StatementService',
    function ($scope, $uibModal, CustomerInvoiceService, $routeParams, DataTransferService, $location, Utility, limitToFilter, $http, UtilityFunc, PaymentService, growl, StatementService) {
        $scope.showLoading = true;
        $scope.gi = {};
        var statementNo = $routeParams.statementno;
        if (!angular.isUndefined(statementNo)) {
            StatementService.GetStatement(statementNo).then(function (d) {
                console.log(JSON.stringify(d.data));
                $scope.gi = d.data;
                $scope.showLoading = false;
            }, function (err) { });
        }

        CustomerInvoiceService.GetLookupData().then(function (d) {
            $scope.lookupData = d.data;            
            $scope.showLoading = false;
        }, function (err) { });


        $scope.GeneratePayment = function (branchId, statementNo) {
            $scope.showLoading = true;
            PaymentService.GeneratePayment(branchId, statementNo).then(function (d) {
                $scope.showLoading = false;
                $location.path('/billing/generatereceipt/' + d.data + '/' + statementNo);
            }, function (err) {
                growl.error(err.data.ExceptionMessage, {});
            });
        };
        

        $scope.DeleteStatement = function (branchId, statementNo) {
            $scope.showLoading = true;
            StatementService.DeleteStatement(branchId, statementNo).then(function (d) {
                $scope.showLoading = false;
                growl.success('Deleted Successfully', {});
                $location.path('/owner')
            }, function (err) { });
        };

        $scope.ApproveStatement = function (branchId, statementNo) {
            $scope.showLoading = true;
            StatementService.ApproveStatement(branchId, statementNo).then(function (d) {
                $scope.showLoading = false;
                growl.success('Approved Successfully', {});
                $scope.gi.IsApproved = true;
            }, function (err) { });
        };
}]);

﻿app.controller('CustomerInvoiceCntrl', ['$scope', '$uibModal', 'CustomerInvoiceService', '$routeParams', 'DataTransferService', '$location', 'Utility', 'limitToFilter', '$http', 'UtilityFunc', 'Utility',
                               function ($scope, $uibModal, CustomerInvoiceService, $routeParams, DataTransferService, $location, Utility, limitToFilter, $http, UtilityFunc, Utility) {

                                   $scope.cin = {};
                                   $scope.cin.InvoiceDate = new Date();
                                   $scope.chkArr = {};
                                   //$scope.cin.invoiceDetailList = [];
                                   $scope.isChecked = true;
                                   var customerInvoiceIndex = -1;


                                   $scope.customerInvoiceDetails = function (index) {
                                       customerInvoiceIndex = index;

                                       var modalInstance = $uibModal.open({
                                           animation: true,
                                           templateUrl: 'Js/Billing/Templates/CustomerInvoice/add-customer-invoice.html?v=' + Utility.Version,
                                           controller: 'addEditCustomerInvoiceCntrl',
                                           //size: 'lg',
                                           windowClass: 'app-modal-window',
                                           resolve: {
                                               customerInvoiceItemObj: function () {
                                                   return {
                                                       customerInvoiceItem: customerInvoiceIndex != -1 ? $scope.cin.invoiceDetailList[customerInvoiceIndex] : "",
                                                       currencyCode: $scope.cin.CurrencyCode != "" ? $scope.cin.CurrencyCode : ""
                                                   }
                                               }
                                           }
                                       });

                                       modalInstance.result.then(function (ci) {

                                           if (customerInvoiceIndex == -1) {
                                               if ($scope.cin.invoiceDetailList != null) {
                                                   $scope.cin.invoiceDetailList.push(ci);
                                               }
                                               else {
                                                   $scope.cin.invoiceDetailList = new Array();
                                                   $scope.cin.invoiceDetailList.push(ci);
                                               }
                                           }
                                           else {
                                               $scope.cin.invoiceDetailList[customerInvoiceIndex] = ci;
                                           }
                                           CalcTaxAmount();
                                       }, function () {
                                       });
                                   };





                                   $scope.SaveCustomerInvoiceDetails = function () {
                                       debugger;
                                       CustomerInvoiceService.SaveCustomerInvoice($scope.cin).then(function (d) {

                                       });

                                   };


                                   var chkArr = DataTransferService.GetData();

                                   $scope.MerchantResults = function ($query) {

                                       return $http.get(Utility.ServiceUrl + '/master/MerchantProfile/search/' + $query + '/billingCustomer').then(function (response) {
                                           return limitToFilter(response.data, 15);
                                       });
                                   };

                                   $scope.cin.invoiceDetailList = new Array();
                                   if (chkArr.length > 0) {

                                       $scope.cin.invoiceDetailList = chkArr;
                                       CalcTaxAmount();
                                   }

                                   $scope.hdrChkCustInvoice = false;
                                   $scope.toggleCustInvoice = function () {

                                       $scope.hdrChkCustInvoice = !$scope.hdrChkCustInvoice;
                                       angular.forEach($scope.cin.invoiceDetailList, function (item, index) {
                                           item.chk = $scope.hdrChkCustInvoice;
                                       });
                                   };






                                   CustomerInvoiceService.GetLookupData().then(function (d) {

                                       $scope.lookupData = d.data;
                                       $scope.cin.InvoiceType = d.data.invoiceType;
                                   }, function (err) { });


                                   $scope.SelectCurrencyCode = function (curCode) {

                                       CustomerInvoiceService.GetExchangeRate(curCode).then(function (d) {

                                       }, function (err) { });
                                   }

                                   /* datepicker configuration */
                                   $scope.formats = ['dd/MM/yyyy'];
                                   $scope.format = $scope.formats[0];

                                   $scope.today = function () {
                                       $scope.dt = new Date();
                                   };
                                   $scope.today();

                                   $scope.popup = {
                                       InvoiceDate: false,
                                   };

                                   $scope.open = function (id) {
                                       $scope.popup[id] = true;
                                   };
                                   /* datepicker configuration */

                                   function CalcTaxAmount() {
                                       var totalTaxAmount = 0.0;
                                       var totalAmount = 0.0;
                                       if ($scope.cin.invoiceDetailList.length > 0) {
                                           angular.forEach($scope.cin.invoiceDetailList, function (item, index) {
                                               totalTaxAmount += parseFloat(item.TaxAmount);
                                               totalAmount += parseFloat(item.TotalAmount);
                                           });
                                           $scope.cin.TaxAmount = totalTaxAmount.toFixed(2);
                                           $scope.cin.TotalAmount = totalAmount.toFixed(2);
                                       }
                                       else {
                                           $scope.cin.TaxAmount = totalTaxAmount.toFixed(2);
                                           $scope.cin.TotalAmount = totalAmount.toFixed(2);
                                       }

                                   }

                                   CalcTaxAmount();

                               }]);

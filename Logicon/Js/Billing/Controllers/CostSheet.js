﻿app.controller('CostSheetController', ['$scope', '$uibModal', 'CostSheetService', '$routeParams', 'DataTransferService', '$location', 'Utility', 'limitToFilter', '$http', 'UtilityFunc', 'Utility',
                               function ($scope, $uibModal, CostSheetService, $routeParams, DataTransferService, $location, Utility, limitToFilter, $http, UtilityFunc, Utility) {
                                   $scope.tabs = [
                                       { title: 'Details', content: 'Js/Billing/Templates/CostSheet/details.html?v=' + Utility.Version, active: false, disabled: false },
                                   ];
                                   $scope.chkArr = {};
                                   $scope.isChecked = true;
                                   var costSheetIndex = -1;

                                   $scope.CostSheetDetails = function (index) {                                     
                                       costSheetIndex = index;
                                       var modalInstance = $uibModal.open({
                                           animation: true,
                                           templateUrl: 'Js/Billing/Templates/CostSheet/add-cost-sheet.html?v=' + Utility.Version,
                                           controller: 'addEditCostSheetCntrl',
                                           size: 'lg',
                                           resolve: {
                                               costSheetItem: function () {
                                                   return costSheetIndex != -1 ? $scope.costSheetList[costSheetIndex] : { OrderNo: orderNo };
                                               }
                                           }
                                       });

                                       modalInstance.result.then(function (cs) {
                                           if (costSheetIndex == -1) {
                                               if ($scope.costSheetList != null) {
                                                   $scope.costSheetList.push(cs);
                                               }
                                               else {
                                                   $scope.costSheetList = new Array();
                                                   $scope.costSheetList.push(cs);
                                               }
                                           }
                                           else {
                                               $scope.costSheetList[costSheetIndex] = cs;
                                           }

                                       }, function () {

                                       });
                                   };

                                   $scope.costSheetList = new Array();

                                   $scope.hdrCostSheet = false;
                                   $scope.toggleCostSheet = function () {

                                       $scope.hdrCostSheet = !$scope.hdrCostSheet;
                                       angular.forEach($scope.costSheetList, function (item, index) {
                                           item.chk = $scope.hdrCostSheet;
                                       });
                                   };


                                   $scope.DeleteCostSheet = function (index) {
                                       $scope.costSheetList = UtilityFunc.removeArrayElementByKey($scope.costSheetList, 'Index', index);
                                   };

                                   $scope.CloneCostSheet = function () {
                                       var tempArray = new Array();
                                       angular.forEach($scope.costSheetList, function (item, index) {
                                           if (item.chk) {
                                               tempArray.push(index);
                                           }
                                       });

                                       angular.forEach(tempArray, function (item, index) {
                                           var tempObj = {};
                                           angular.copy($scope.costSheetList[item], tempObj);
                                           $scope.costSheetList.push(tempObj);
                                       });

                                       angular.forEach($scope.costSheetList, function (item, index) {
                                           item.Index = index;
                                           item.chk = false;
                                       });
                                   };

                                   $scope.RemoveCostSheet = function () {
                                       var tempArray = new Array();
                                       angular.forEach($scope.costSheetList, function (item, index) {
                                           if (item.chk) {
                                               tempArray.push(index);
                                           }
                                       });

                                       angular.forEach(tempArray, function (item, index) {
                                           $scope.costSheetList = UtilityFunc.removeArrayElementByKey($scope.costSheetList, 'Index', item);
                                       });

                                       angular.forEach($scope.costSheetList, function (item, index) {
                                           item.Index = index;
                                           item.chk = false;
                                       });
                                   };


                                   $scope.AddOtherContainers = function () {

                                       var chargeCode = null;

                                       angular.forEach($scope.costSheetList, function (item, index) {
                                           if (item.chk) {
                                               chargeCode = item.ChargeCodeDesc;
                                           }
                                       });

                                       angular.forEach($scope.costSheetList, function (item, index) {
                                           item.ChargeCodeDesc = chargeCode;
                                       });
                                   }

                                   $scope.ChangeCreditTerm = function () {

                                   }

                                   $scope.ChangeSellingRate = function () {
                                       var sellingPrice = 0;


                                       angular.forEach($scope.costSheetList, function (item, index) {
                                           if (item.chk) {
                                               sellingPrice = item.Price;
                                           }
                                       });

                                       angular.forEach($scope.costSheetList, function (item, index) {
                                           item.Price = sellingPrice;
                                       });
                                   }


                                   $scope.ExistingCustometInvoice = function () {
                                       debugger;
                                       var tempArray = new Array();
                                       angular.forEach($scope.costSheetList, function (item, index) {
                                           if (item.chk) {
                                               tempArray.push(item);
                                           }
                                       });
                                       DataTransferService.SetData(tempArray);
                                       $location.path('/billing/customerinvoice');


                                   }


                                   $scope.isFrmCostsheetValid = false;
                                   $scope.$watch('frmCostsheet.$valid', function (isValid) {
                                       $scope.isFrmCostsheetValid = isValid;
                                   });

                                   $scope.SaveCostSheet = function () {
                                       debugger;
                                       $scope.submitted = true;
                                      // if ($scope.isFrmCostsheetValid) {
                                           
                                           CostSheetService.SaveCostSheet($scope.costSheetList).then(function (d) {

                                           }, function (err) {

                                           });
                                      // } else {
                                      //     growl.error('please enter all mandatory fields', {});
                                      // }
                                   };

                                   $scope.NewCustomerInvoice = function (chkArr) {
                                       var arr = Object.keys(chkArr);
                                       var jsonArray = new Array();
                                       for (var i = 0; i < arr.length; i++) {
                                           if (chkArr[arr[i]]) {
                                               jsonArray.push(arr[i]);
                                           }
                                       }

                                       DataTransferService.SetData(jsonArray);
                                       $location.path('/billing/customercashbill');
                                   };

                                   $scope.isChecked = function () {

                                       var chkArr = $scope.chkArr;
                                       var arr = Object.keys(chkArr);
                                       var flag = true;
                                       for (var i = 0; i < arr.length; i++) {
                                           if (chkArr[arr[i]]) {
                                               flag = false;
                                               break;
                                           }
                                       }
                                       return flag;
                                   }

                                   $scope.MerchantResults = function ($query) {
                                       return $http.get(Utility.ServiceUrl + '/master/MerchantProfile/search/' + $query + '/billingCustomer').then(function (response) {
                                           return limitToFilter(response.data, 15);
                                       });
                                   };

                                   $scope.CustomerSelected = function (item, type) {
                                       $scope.orderHeaderDetails[type] = item.Value;
                                   };

                                   /*
                                   $scope.isChecked = function (chkArr) {
                                       debugger;
                                       var arr = Object.keys(chkArr);
                                       var flag = true;
                                       for (var i = 0; i < arr.length; i++) {
                                           if (chkArr[arr[i]]) {
                                               flag = false;
                                               break;
                                           }
                                       }
                                       return flag;
                                   };
                                   */
                                   /*
                                   $scope.$watch(function () {
                                       debugger;
                                       var arr = Object.keys($scope.chkArr);
                                       if (arr.length <= 0)
                                           return;
                               
                                       
                                       return $scope.chkArr;
                                   }, function (newVal, oldVal) {        
                                       debugger;
                                       if (typeof newVal == 'undefined')
                                           return;
                               
                                       var arr = Object.keys(newVal);
                                       for (var i = 0; i < arr.length; i++) {
                                           if (newVal[arr[i]]) {
                                               $scope.isChecked = false;
                                               break;
                                           }
                                       }
                                   });
                                   */
                                   var orderNo = $routeParams.orderno;
                                   if (typeof orderNo != 'undefined') {
                                       CostSheetService.GetCostSheetListByOrderNo(orderNo).then(function (d) {
                                           $scope.costSheetList = d.data.costSheetList;
                                           $scope.orderHeaderDetails = d.data.orderHeaderObj;
                                       }, function (err) { })
                                   }
                               }]);

app.service('DataTransferService', ['$http', function ($http) {
    this.Data = [];

    this.SetData = function (data) {
        this.Data = data;
    };

    this.GetData = function () {
        return this.Data;
    }
}]);
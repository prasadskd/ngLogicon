﻿app.controller('MerchantProfileController', ['$scope', '$http', '$uibModal', 'MerchantProfileService', '$window',
    '$routeParams', '$location', 'growl', 'limitToFilter', 'Utility', function ($scope, $http, $uibModal, MerchantProfileService,$window,
    $routeParams, $location, growl, limitToFilter, Utility) {

    $scope.search = {
        filter: 'all'
    };
    $scope.AddAddress = function (addressId) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'Js/MasterData/Templates/MerchantProfile/add-address.html?v=' + Utility.Version,
            controller: 'addEditAddressCntrl',            
            windowClass: 'app-modal-window',
            resolve: {
                addressObj: function () {
                    return (addressId != -1 ? $scope.mp.AddressList[addressId] : { AddressId: -1});
                }
            }
        });

        modalInstance.result.then(function (address) {            
            if ($scope.mp.AddressList != null) {
                if (address.AddressId != -1) {
                    for (var i = 0; i < $scope.mp.AddressList.length; i++) {
                        if ($scope.mp.AddressList[i].AddressId == address.AddressId) {                            
                            $scope.mp.AddressList[i] = address;
                            break;
                        }
                    }
                }
                else {
                    $scope.mp.AddressList.push(address);
                }
            }
            else {
                $scope.mp.AddressList = new Array();
                $scope.mp.AddressList.push(address);
            }
        }, function () {

        });        
    };

    $scope.DeleteAddress = function (addressId) {        
        $scope.mp.AddressList[addressId].IsActive = false;
        $scope['tr_' + addressId] = true;
    };

    $scope.isFrmMerchantProfile = false;
    $scope.$watch('frmMerchantProfile.$valid', function (isValid) {
        $scope.isFrmMerchantProfile = isValid;
    });

    $scope.AddMerchantProfile = function (mp) {
        $scope.showLoading = true;
        if ($scope.isFrmMerchantProfile) {
            var fResults = $scope.fResults;
            var yResults = $scope.yResults;
            var tResults = $scope.tResults;

            $scope.mp.MerchantRelationList = new Array();
            if (!angular.isUndefined(fResults)) {
                for (var i = 0; i < fResults.length; i++) {
                    var obj = {
                        RelatedMerchantCode: fResults[i].Value,
                        RelationshipType: fResults[i].RelationshipType,

                    };

                    $scope.mp.MerchantRelationList.push(obj);
                }
            }

            if (!angular.isUndefined(yResults)) {
                for (var i = 0; i < yResults.length; i++) {
                    var obj = {
                        RelatedMerchantCode: yResults[i].Value,
                        RelationshipType: yResults[i].RelationshipType
                    };

                    $scope.mp.MerchantRelationList.push(obj);
                }
            }

            if (!angular.isUndefined(tResults)) {
                for (var i = 0; i < tResults.length; i++) {
                    var obj = {
                        RelatedMerchantCode: tResults[i].Value,
                        RelationshipType: tResults[i].RelationshipType
                    };

                    $scope.mp.MerchantRelationList.push(obj);
                }
            }

            MerchantProfileService.SaveMerchantProfile(mp).then(function (d) {
                $scope.showLoading = false;
                $location.path('/masterdata/merchantlist');
                growl.success(d.data, {});
            }, function (err) {
                growl.error(err.statusText, {});
            });
        } else {
            $scope.showLoading = false;
            growl.error('Please enter all mandatory fields', {});
        }

        
    };

    $scope.showLoading = true;
    $scope.GetMerchantProfileList = function (skip, limit) {        
        var filter = $scope.search.filter;        
        MerchantProfileService.GetMerchantProfileList(skip, limit, filter).then(function (d) {
            $scope.showLoading = false;
            $scope.mp = d.data.mpList;
            $scope.totalItems = d.data.totalItems;
        }, function (err) {            
            growl.error(err.statusText, {});
        });
    };


    $scope.DeleteMerchant = function (code,name)
    {
        if ($window.confirm('Are you sure, you want to delete \'' + name + '\' ?')) {
            MerchantProfileService.DeleteMerchant(code).then(function (d) {
                growl.success("Deleted Successfully.", {});
                $scope.getData();
            }, function (err) { });
        }
    }


    $scope.AddMerchant = function (merchantCode, taxID, regNo) {
        $location.path('/masterdata/merchant/' + merchantCode + '/' + taxID + '/' + regNo);
    }

    
    $scope.getData = function () {
        $scope.showLoading = true;        
        var skip = $scope.currentPage == 1 ? 0 : ($scope.limit * ($scope.currentPage - 1));        
        $scope.GetMerchantProfileList(skip, $scope.limit);
    };

    $scope.print = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'Js/MasterData/Templates/MerchantProfile/print.html?v=' + Utility.Version,
            controller: 'PrintCntrl',
            windowClass: 'app-modal-window',
            resolve: {
                reportObj: function () {
                    return {
                        branchID: 10,
                        poNo: '1606002',
                        Url: '/NetStockDiamondReports/Diamond.PurchaseOrder'
                    }
                }
            }
        });

        modalInstance.result.then(function (d) {
            
        }, function () {

        });
    };
    
    var code = $routeParams.code;
    $scope.currentPage = 1;
    $scope.limit = 10;
    if (typeof code != 'undefined') {
        if (code != 'NEW') {
            var Obj = {
                MerchantCode: code,
                RegNo: $routeParams.regNo,
                TaxID: $routeParams.taxID
            };
            MerchantProfileService.GetMerchantProfile(Obj).then(function (d) {
                $scope.showLoading = false;
                $scope.mp = d.data.merchant;
                $scope.fResults = d.data.fResults;
                $scope.yResults = d.data.yResults;
                $scope.tResults = d.data.tResults;

            }, function (err) {                
                growl.error(err.statusText, {});
            });
        } else {
            $scope.showLoading = false;
        }

        MerchantProfileService.GetLookupData().then(function (d) {            
            $scope.lookupData = d.data;
        }, function (err) {
            growl.error(err.statusText, {});
        });
    }
    else {        
        $scope.getData();
    }

    

    $scope.pageChanged = function () {
        $scope.getData();
    };

    $scope.backClick = function () {
        $location.path('/masterdata/merchantlist');
    };

    $scope.results = function (text) {
        var filter = $scope.search.filter;
        return $http.get(Utility.ServiceUrl + '/master/MerchantProfile/search/' + text + '/' + filter).then(function (response) {
            return limitToFilter(response.data, 15);
        });
    };    

    $scope.filterChanged = function () {
        $scope.getData();
    };
    
    $scope.forwarderResults = function ($query) {
        if ($query != '') {
            return $http.get(Utility.ServiceUrl + '/master/MerchantProfile/search/' + $query + '/freightForwarder', { cache: true }).then(function (response) {
                return response.data;
            });
        }
        else {
            return [];
        }        
    };

    $scope.yardResults = function ($query) {
        if ($query != '') {
            return $http.get(Utility.ServiceUrl + '/master/MerchantProfile/search/' + $query + '/yard', { cache: true }).then(function (response) {
                return response.data;
            });
        }
        else {
            return [];
        }
    };

    $scope.transportResults = function ($query) {
        if ($query != '') {
            return $http.get(Utility.ServiceUrl + '/master/MerchantProfile/search/' + $query + '/transporter', { cache: true }).then(function (response) {
                return response.data;
            });
        }
        else {
            return [];
        }
    };  

    }]);

app.controller('PrintCntrl', ['$scope', 'reportObj', 'Utility', function ($scope, reportObj, Utility) {
    
    $scope.reportPath = Utility.ReportPath
        + '/PurchaseOrderReport?branchID='
        + reportObj.branchID
        + '&poNo=' + reportObj.poNo 
        + '&Url=' + reportObj.Url;
    debugger;
}]);





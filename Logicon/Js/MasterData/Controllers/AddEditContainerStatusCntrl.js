﻿app.controller('AddEditContainerStatusCntrl', ['$scope', '$uibModalInstance', 'Code', 'ContainerStatusService', 'growl', function ($scope, $uibModalInstance, Code, ContainerStatusService, growl) {
    $scope.truefalse = false;
    $scope.cancel = function () {
       
        $uibModalInstance.dismiss('cancel');
        $scope.isfrmAddContainerStatus = false;
    };
   

    $scope.isfrmAddContainerStatus = false;
    $scope.$watch('frmAddContainerStatus.$valid', function (isValid) {
        $scope.isfrmAddContainerStatus = isValid;
    });


    $scope.AddContainerStatus = function (containerstatus) {
      
        if ($scope.isfrmAddContainerStatus) {
            ContainerStatusService.SaveContainerStatus(containerstatus).then(function (d) {
                $uibModalInstance.close();
                growl.success(d.data, {});

            }, function (err) { growl.error(err.statusText, {}); });
        }
        else {
            growl.error('Please enter all mandatory fields..', {});
        }
    };
  

    if (Code != -1) {
            ContainerStatusService.GetContainerStatusByCode(Code).then(function (d) {
                $scope.containerstatus = d.data;
                $scope.truefalse = true;
            }, function (err) { growl.error(err.statusText, {}); });        
    }

}]);
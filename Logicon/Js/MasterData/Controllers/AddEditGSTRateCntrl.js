﻿app.controller('AddEditGSTRateCntrl', ['$scope', '$uibModalInstance', 'gstCode', 'GstRateService', 'growl', function ($scope, $uibModalInstance, gstCode, GstRateService, growl) {
   
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $scope.isfrmAddGstRateMaster = false;
    };

    $scope.isfrmAddGstRateMaster = false;
    $scope.$watch('frmAddGSTRateMaster.$valid', function (isValid) {      
        $scope.isfrmAddGstRateMaster = isValid;
    });

    $scope.AddGSTRateMaster = function (gst) {       
        if ($scope.isfrmAddGstRateMaster) {
            GstRateService.SaveGSTRateCode(gst).then(function (d) {
                $uibModalInstance.close();
                growl.success(d.data, {});

            }, function (err) { growl.error(err.statusText, {}); });
        }
        else {
            growl.error('Please enter all mandatory fields..', {});
        }
    };
    $scope.truefalse = false;

    if (gstCode != -1) {        
        GstRateService.GetRateByGstCode(gstCode).then(function (d) {
            $scope.truefalse = true;
            $scope.gst = d.data;
        }, function (err) { growl.error(err.statusText, {}); });
    }

}]);
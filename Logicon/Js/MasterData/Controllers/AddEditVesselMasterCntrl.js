﻿app.controller('AddEditVesselMasterCntrl', ['$scope', '$uibModalInstance', 'vesselId', 'VesselMasterService', 'growl', function ($scope, $uibModalInstance, vesselId, VesselMasterService, growl) {
   
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $scope.isfrmAddVesselMaster = false;
    };

    $scope.isfrmAddVesselMaster = false;
    $scope.$watch('frmVesselMaster.$valid', function (isValid) {
        $scope.isfrmAddVesselMaster = isValid;
    });

    $scope.AddVesselMaster = function (vessel) {
       
        if ($scope.isfrmAddVesselMaster) {
            VesselMasterService.SaveVesselMaster(vessel).then(function (d) {
                $uibModalInstance.close();
                growl.success(d.data, {});

            }, function (err) { growl.error(err.statusText, {}); });
        }
        else {
            growl.error('Please enter all mandatory fields..', {});
        }
    };

   

    VesselMasterService.GetCountryList().then(function (d) {       
        $scope.countryDataList = d.data;
       
    }, function (err) {
        growl.error(err.statusText, {});
    });

    $scope.truefalse = false;

    if (vesselId != -1) {
       
        VesselMasterService.GetVesselMasterById(vesselId).then(function (d) {         
            
            $scope.vessel = d.data;
            $scope.truefalse = true;
        }, function (err) { growl.error(err.statusText, {}); });
       
    }



}]);
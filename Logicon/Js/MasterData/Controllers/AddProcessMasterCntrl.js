﻿app.controller('AddProcessMasterCntrl', ['$scope', '$uibModalInstance', 'pmCode', 'ProcessMasterService', 'growl', 'CompanyService',
    function ($scope, $uibModalInstance, pmCode, ProcessMasterService, growl, CompanyService) {
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.AddProcessMaster = function (pm) {
        debugger;
        if ($scope.isfrmAddProcessMaster) {
           
            ProcessMasterService.SaveProcess(pm).then(function (d) {
                debugger;
                $uibModalInstance.close();
                growl.success(d.data, {});
            }, function (err) { growl.error(err.statusText, {}); });
        }
        else {
            growl.error('Please enter all mandatory fields..', {});
        }
    };

    $scope.isfrmAddProcessMaster = false;
    $scope.$watch('frmAddProcessMaster.$valid', function (isValid) {       
        $scope.isfrmAddProcessMaster = isValid;
    });

    ProcessMasterService.getLookupData().then(function (d) {
        $scope.lookupData = d.data;
    }, function (err) {
        growl.error(err.statusText, {});
    });

    CompanyService.GetCompanySubscription().then(function (d) {
        $scope.modulesList = d.data;
        debugger;
    }, function (err) { growl.error(err.statusText, {}); });


    $scope.truefalse = false;

    if (pmCode != -1) {
        debugger;
        ProcessMasterService.GetProcess(pmCode).then(function (d) {
            debugger;

            $scope.truefalse = true;
            $scope.pm = d.data;
        }, function (err) { growl.error(err.statusText, {}); });
    }

}]);
﻿app.controller('ChargeCodeController', ['$scope', '$routeParams', 'ChargeCodeService', 'GstRateService', '$location', '$window', 'growl', 'Utility', function ($scope, $routeParams, ChargeCodeService, GstRateService, $location, $window, growl, Utility) {
    $scope.currentPage = 1;
    $scope.limit = 10;

    $scope.getData = function () {
        debugger;
        var skip = $scope.currentPage == 1 ? 0 : ($scope.limit * ($scope.currentPage - 1));
        $scope.showLoading = true;
        ChargeCodeService.GetChargeCodeList(skip, $scope.limit).then(function (d) {
            $scope.showLoading = false;
            $scope.ccList = d.data.chargeCodeList;
            $scope.totalItems = d.data.totalItems;
        }, function () { });
    };

    $scope.pageChanged = function () {
        $scope.getData();
    };

    $scope.tabs = [
        { title: 'Charge Rules', content: 'Js/MasterData/Templates/ChargeCode/chargerules.html?v=' + Utility.Version, active: false, disabled: false },
        { title: 'Tax', content: 'Js/MasterData/Templates/ChargeCode/tax.html?v=' + Utility.Version, active: false, disabled: false },
    ];

    $scope.cc = {};    

    $scope.SaveChargeCode = function (cc) {
        if ($scope.isfrmChargeCodeValid) {
            ChargeCodeService.SaveChargeCode(cc).then(function (d) {
                growl.success(d.data, {});
                $location.path('/masterdata/chargecodelist');
            }, function () { });
        } else {
            growl.error('Please enter all mandatory fields..', {});
        }
    };

    $scope.AddChargeCode = function (chargeCode) {
        $location.path('/masterdata/chargecodemaster/' + chargeCode);
    };

    $scope.navToList = function () {
        $location.path('/masterdata/chargecodelist');
    };

    var chargeCode = $routeParams.chargecode;
    $scope.showLoading = true;
    if (typeof chargeCode != 'undefined') {
        if (chargeCode != 'NEW') {
            ChargeCodeService.GetChargeCode(chargeCode).then(function (d) {                
                $scope.cc = d.data;
                $scope.truefalse = false;
            }, function () { });
        }

        ChargeCodeService.GetLookupData().then(function (d) {
            $scope.showLoading = false;
            $scope.lookupData = d.data;
            $scope.truefalse = true;
        }, function () { });
    }
    else {
        debugger;
        ChargeCodeService.GetChargeCodeList(0, $scope.limit).then(function (d) {
            $scope.showLoading = false;
            $scope.ccList = d.data.chargeCodeList;
            $scope.totalItems = d.data.totalItems;
        }, function () { });
    }

    $scope.isfrmChargeCodeValid = false;
    $scope.$watch('frmChargeCode.$valid', function (isValid) {
        $scope.isfrmChargeCodeValid = isValid;
    });

    $scope.gstChange = function (gstRate, gstCode) {
        GstRateService.GetRateByGstCode($scope.cc[gstCode]).then(function (d) {
            $scope.cc[gstRate] = d.data.Rate.toFixed(2);
        }, function (err) { });
    };
       
    $scope.DeleteChargeCode = function (chargeCode) {
        if ($window.confirm('Are you sure, you want to delete \'' + chargeCode + '\' ?')) {
            ChargeCodeService.DeleteChargeCode(chargeCode).then(function (d) {
                growl.success(d.data, {});
                $scope.getData();
            }, function (err) { });
        }
    };
}]);



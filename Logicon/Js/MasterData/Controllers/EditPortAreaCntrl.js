﻿app.controller('EditPortAreaCntrl', ['$scope', '$uibModalInstance', 'PortAreaService','growl', 'dataObj', function ($scope, $uibModalInstance, PortAreaService, growl, dataObj) {
    debugger;
    $scope.truefalse = false;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    if (dataObj.portCode != '') {
        PortAreaService.portByPortCode(dataObj.portCode).then(function (d) {
            $scope.port = d.data;
            $scope.truefalse = true;
        }, function (err) {

        });
    }
    else {
        $scope.port = {
            CountryCode: dataObj.CountryCode,
            PortType: 1300
        };
    }

    PortAreaService.GetIntegrationBranch().then(function (d) {        
        $scope.branchList = d.data;
    }, function (err) { });

    $scope.isFrmPortAreaValid = false;

    $scope.$watch('frmPortArea.$valid', function (valid) {
        $scope.isFrmPortAreaValid = valid;
    });
    $scope.SavePort = function (port) {
        if ($scope.isFrmPortAreaValid) {
            PortAreaService.SavePort(port).then(function (d) {
                if (d.data == 'Saved Successfully...!') {
                    $uibModalInstance.close(true);
                    growl.success('Saved..!', {});
                }
                else {
                    $uibModalInstance.close(false);
                    growl.error('failed', {});
                }
            }, function (err) {

            });
        } else {
            growl.error('failed', {});
        }
        
    };
}]);
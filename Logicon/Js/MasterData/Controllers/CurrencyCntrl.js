﻿app.controller('CurrencyCntrl', ['$scope', 'CurrencyService', '$uibModal', function ($scope, CurrencyService, $uibModal) {
    $scope.GetCurrencyList = function (skip, take) {
        CurrencyService.GetCurrencyList(skip, take).then(function (d) {            
            $scope.currencyList = d.data.currencyList;
            $scope.totalItems = d.data.totalItems;
        }, function () { });
    };

    $scope.getData = function () {
        var skip = $scope.currentPage == 1 ? 0 : ($scope.limit * ($scope.currentPage - 1));
        $scope.GetCurrencyList(skip, $scope.limit);
    };

    $scope.currentPage = 1;
    $scope.limit = 10;
    $scope.GetCurrencyList(0, $scope.limit);

    $scope.pageChanged = function () {
        $scope.getData();
    };

    $scope.EditCurrency = function (CurrencyCode) {
        debugger;
        if (countryCode == 'NEW')
            $scope.truefalse = false;
        else
            $scope.truefalse = true;

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'Js/MasterData/Templates/currency/add-currency.html?v=' + Utility.Version,
            controller: 'EditCurrencyCntrl',
            size: 'md',
            resolve: {
                CurrencyCode: function () {
                    return CurrencyCode;
                }
            }
        });

        modalInstance.result.then(function () {
            $scope.getData();
        }, function () {

        });
    };

    $scope.DeleteCurrency = function (currencyCode) {
        debugger;
        CurrencyService.DeleteCurrency(currencyCode).then(function (d) {
            $scope.getData();
        }, function () { });
    };
}]);




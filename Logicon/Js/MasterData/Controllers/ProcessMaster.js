﻿app.controller('ProcessMasterController', ['$scope', '$uibModal', 'growl', 'Utility', 'ProcessMasterService', function ($scope, $uibModal, growl, Utility, ProcessMasterService) {
    $scope.currentPage = 1;
    $scope.limit = 10;

    $scope.AddProcessMaster = function (Code) {
        debugger;
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'Js/MasterData/Templates/ProcessMaster/add-process-master.html?v=' + Utility.Version,
            controller: 'AddProcessMasterCntrl',
            size: 'lg',
            resolve: {
                pmCode: function () {
                    return Code;
                }
            }
        });

        modalInstance.result.then(function (pm) {            
            $scope.ProcessList();
        }, function (err) {
            growl.error(err.statusText, {});
        });
    };

    $scope.ProcessList = function () {
        var skip = $scope.currentPage == 1 ? 0 : ($scope.limit * ($scope.currentPage - 1));
        ProcessMasterService.getProcessMasterList(skip, $scope.limit).then(function (d) {
            $scope.processList = d.data.processList;
            $scope.totalItems = d.data.totalItems;
        }, function (err) { growl.error(err.statusText, {}); });
    };    

    $scope.pageChanged = function () {
        $scope.ProcessList();
    };

    $scope.getData = function (skip, take) {
        ProcessMasterService.getProcessMasterList(skip, take).then(function (d) {
            $scope.processList = d.data.processList;
            $scope.totalItems = d.data.totalItems;
        }, function (err) { growl.error(err.statusText, {}); });
    };

    $scope.DeleteProcessMaster = function (code) {
              
        if (confirm('Are you sure, you want to delete \'' + code + '\' ?')) {
            debugger;
            ProcessMasterService.DeleteProcessMaster(code).then(function (d) {
                growl.success(d.data, {});
                $scope.ProcessList();
            }, function (err) { });
        }
    }
    
    $scope.getData(0, $scope.limit);

    $scope.IsDeleted = function (pm) {
       
        return pm.IsActive == false ? 'deleted' : 'ok';
    }

         
}]);




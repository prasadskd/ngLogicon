﻿app.controller('AssociationTarrifCntrl', ['$scope', 'TariffService', '$uibModal', 'Utility', '$location', '$routeParams', 'growl', 'UtilityFunc',
    function ($scope, TariffService, $uibModal, Utility, $location, $routeParams, growl, UtilityFunc) {

        $scope.dateFormat = UtilityFunc.DateFormat();
        $scope.dateTimeFormat = UtilityFunc.DateTimeFormat();
        $scope.dateTimeFormat12 = UtilityFunc.DateTimeFormat12();

        $scope.ErrorMsg = false;
        $scope.isFrmAssociationTariffValid = false;
        $scope.$watch('frmAssociationTariff.$valid', function (isValid) {
            $scope.isFrmAssociationTariffValid = isValid;
        });
        $scope.th = {
            tariffDetails: new Array()
        };

        $scope.isNew = false;
        var tariffno = $routeParams.tariffno;
        if (tariffno == 'NEW')
            $scope.isNew = true;
        else
            $scope.isNew = false;

        $scope.GetLookupData = function (tariffno) {
            $scope.showLoading = true;
            TariffService.GetLookupData(tariffno).then(function (d) {
                $scope.showLoading = false;
                $scope.LookupData = d.data;
            }, function (err) { });
        };

        var tariffDetailIndex = -1;
        $scope.AddDetail = function (inx) {
            tariffDetailIndex = inx;
            debugger;
            $scope.modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/MasterData/Templates/AssociationTariff/tariff-detail.html?v=' + Utility.Version,
                size: 'md',
                controller: 'TariffDetailCntrl',
                resolve: {
                    dataObj: function () {
                        return {
                            tariffDetail: (tariffDetailIndex == -1 ? {} : $scope.th.tariffDetails[tariffDetailIndex]),
                            lookUpData: $scope.LookupData
                        };
                    }
                }
            });

            $scope.modalInstance.result.then(function (tariffDetail) {
                if (tariffDetailIndex != -1) {
                    $scope.th.tariffDetails[tariffDetailIndex] = tariffDetail;
                }
                else {
                    if ($scope.th.tariffDetails != null) {
                        $scope.th.tariffDetails.push(tariffDetail);
                    }
                    else {
                        $scope.th.tariffDetails = new Array();
                        $scope.th.tariffDetails.push(tariffDetail);
                    }
                }
            }, function () {

            });
        };

        $scope.SaveAssociationTariff = function (th) {
            if ($scope.isFrmAssociationTariffValid && $scope.th.tariffDetails != null && $scope.th.tariffDetails.length > 0) {
                TariffService.SaveTariff(th).then(function (d) {
                    $scope.th = d.data;
                    growl.success('Tariff Saved Successfully', {});
                }, function () { });
            } else {
                if ($scope.isFrmAssociationTariffValid)
                    growl.error('Please enter all mandatory fields', {});
                else if ($scope.th.tariffDetails.length == 0)
                    growl.error('Please enter atleast one detail', {});
            }
        };

        $scope.DeleteDetail = function (inx) {
            $scope.th.tariffDetails.splice(inx, 1);
        };

        $scope.addAssociationTariff = function (QuotationNo) {
            if (QuotationNo != 'NEW')
                $location.path('/MasterData/Associationtariff/' + QuotationNo);
            else
                $location.path('/MasterData/Associationtariff/NEW');
        };


        if (angular.isUndefined(tariffno)) {
            TariffService.GetList(26101).then(function (d) {
                $scope.tariffList = d.data;
            }, function (err) { });
        } else {
            if (tariffno != 'NEW') {
                TariffService.GetTariff(tariffno).then(function (d) {
                    $scope.th = d.data;
                }, function (err) { });
            } else {
                $scope.th = {
                    QuotationNo: ''
                };
            }
        }

        $scope.GetLookupData(tariffno);
    }]);
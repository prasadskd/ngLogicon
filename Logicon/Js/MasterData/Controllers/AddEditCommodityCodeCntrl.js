﻿app.controller('AddEditCommodityCodeCntrl', ['$scope', '$uibModalInstance', 'commodityCode', 'CommodityCodeService', 'growl', function ($scope, $uibModalInstance, commodityCode, CommodityCodeService, growl) {
   
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $scope.isfrmAddCommodityCode = false;
    };

    $scope.isfrmAddCommodityCode = false;
    $scope.$watch('frmAddCommodityCode.$valid', function (isValid) {
        $scope.isfrmAddCommodityCode = isValid;
    });

    $scope.AddCommodityCode = function (cmc) {
       
        if ($scope.isfrmAddCommodityCode) {
            CommodityCodeService.SaveCommodityCode(cmc).then(function (d) {
                $uibModalInstance.close();
                growl.success(d.data, {});

            }, function (err) { growl.error(err.statusText, {}); });
        }
        else {
            growl.error('Please enter all mandatory fields..', {});
        }
    };
    $scope.truefalse = false;

    if (commodityCode != -1) {
       
        CommodityCodeService.GetCommodityByCode(commodityCode).then(function (d) {
            $scope.truefalse = true;
            $scope.commoditycodeList = d.data;
        }, function (err) {
            growl.error(err.statusText, {});
        })
    }

}]);
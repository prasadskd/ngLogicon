﻿app.controller('StandardTarrifCntrl', ['$scope', 'TariffService', '$uibModal', 'Utility', 'UtilityFunc', 'growl', function ($scope, TariffService, $uibModal, Utility, UtilityFunc, growl) {

    $scope.dateFormat = UtilityFunc.DateFormat();
    $scope.dateTimeFormat = UtilityFunc.DateTimeFormat();
    $scope.dateTimeFormat12 = UtilityFunc.DateTimeFormat12();

    $scope.ErrorMsg = false;
    $scope.isFrmDiscountTariffValid = false;
    $scope.$watch('frmDiscountTariff.$valid', function (isValid) {
        $scope.isFrmDiscountTariffValid = isValid;
    });
    $scope.th = {
        tariffDetails: new Array(),
        QuotationNo: UtilityFunc.StandardQuotationkey()
    };    
    $scope.GetLookupData = function () {
        TariffService.GetLookupData().then(function (d) {
            $scope.LookupData = d.data;
        }, function (err) { });
    };

    $scope.GetStandardTariff = function () {
        TariffService.GetTariff(UtilityFunc.StandardQuotationkey()).then(function (d) {            
            if (d.data != null) {
                $scope.th = d.data;

                if ($scope.th.ExpiryDate == null)
                    $scope.th.ExpiryDate = undefined;
            }
        }, function (err) { });
    };

    var tariffDetailIndex = -1;
    $scope.AddDetail = function (inx) {
        tariffDetailIndex = inx;        
        $scope.modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'Js/MasterData/Templates/StandardTariff/tariff-detail.html?v=' + Utility.Version,
            size: 'md',
            controller: 'TariffDetailCntrl',
            resolve: {
                dataObj: function () {
                    return {
                        
                        tariffDetail: (tariffDetailIndex == -1 ? {} : $scope.th.tariffDetails[tariffDetailIndex]),
                        tariffDetailArray: $scope.th.tariffDetails,
                        lookUpData: $scope.LookupData
                        
                    };
                }
            }
        });

        $scope.modalInstance.result.then(function (tariffDetail) {
            if (tariffDetailIndex != -1) {
                $scope.th.tariffDetails[tariffDetailIndex] = tariffDetail;
            }
            else {
                if ($scope.th.tariffDetails != null) {
                    $scope.th.tariffDetails.push(tariffDetail);
                }
                else {
                    $scope.th.tariffDetails = new Array();
                    $scope.th.tariffDetails.push(tariffDetail);
                }
            }
        }, function () {

        });
    };

    $scope.SaveDiscountTariff = function (th) {        
        if ($scope.isFrmDiscountTariffValid && $scope.th.tariffDetails.length > 0) {
            $scope.showLoading = true;
            TariffService.SaveTariff(th).then(function (d) {
                $scope.showLoading = false;
                growl.success('Standard Rate Profile Saved Successfully', {});
            }, function () { });
        } else {
            $scope.showLoading = false;
            if ($scope.th.tariffDetails.length == 0)
                growl.error('Please enter atleast one detail object', {});
            else
                growl.error('Please enter all mandatory elements', {});
        }
    };

    $scope.DeleteDetail = function (inx) {
        $scope.th.tariffDetails.splice(inx, 1);
    };



    $scope.GetLookupData();
    $scope.GetStandardTariff();
}]);
﻿app.controller('RebateTarrifCntrl', ['$scope', 'TariffService', '$uibModal', 'Utility', '$routeParams', '$location', 'growl', 'UtilityFunc',
    function ($scope, TariffService, $uibModal, Utility, $routeParams, $location, growl, UtilityFunc) {

        $scope.dateFormat = UtilityFunc.DateFormat();
        $scope.dateTimeFormat = UtilityFunc.DateTimeFormat();
        $scope.dateTimeFormat12 = UtilityFunc.DateTimeFormat12();

        $scope.ErrorMsg = false;
        $scope.isFrmRebateTariffValid = false;
        $scope.$watch('frmRebateTariff.$valid', function (isValid) {
            $scope.isFrmRebateTariffValid = isValid;
        });
        $scope.th = {
            tariffDetails: new Array()
        };
        $scope.GetLookupData = function () {
            TariffService.GetLookupData().then(function (d) {
                $scope.LookupData = d.data;
            }, function (err) { });
        };

        var tariffDetailIndex = -1;
        $scope.AddDetail = function (inx) {
            tariffDetailIndex = inx;
            //debugger;
            $scope.modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/MasterData/Templates/RebateTariff/tariff-detail.html?v=' + Utility.Version,
                size: 'md',
                controller: 'TariffDetailCntrl',
                resolve: {
                    dataObj: function () {
                        return {
                            tariffDetail: (tariffDetailIndex == -1 ? {} : $scope.th.tariffDetails[tariffDetailIndex]),
                            lookUpData: $scope.LookupData
                        };
                    }
                }
            });

            $scope.modalInstance.result.then(function (tariffDetail) {
                if (tariffDetailIndex != -1) {
                    $scope.th.tariffDetails[tariffDetailIndex] = tariffDetail;
                }
                else {
                    if ($scope.th.tariffDetails != null) {
                        $scope.th.tariffDetails.push(tariffDetail);
                    }
                    else {
                        $scope.th.tariffDetails = new Array();
                        $scope.th.tariffDetails.push(tariffDetail);
                    }
                }
            }, function () {

            });
        };

        $scope.SaveRebateTariff = function (th) {
            if ($scope.isFrmRebateTariffValid && $scope.th.tariffDetails.length > 0) {
                TariffService.SaveTariff(th).then(function (d) {
                    growl.success('Tariff Saved Successfully', {});
                }, function () { });
            } else {
                if ($scope.isFrmRebateTariffValid)
                    growl.error('Please enter all mandatory fields', {});
                else if ($scope.th.tariffDetails.length == 0)
                    growl.error('Please enter atleast one detail', {});
            }
        };

        $scope.DeleteDetail = function (inx) {
            $scope.th.tariffDetails.splice(inx, 1);
        };

        $scope.addDiscountTariff = function (QuotationNo) {
            if (QuotationNo != 'NEW')
                $location.path('/MasterData/rebatetariff/' + QuotationNo);
            else
                $location.path('/MasterData/rebatetariff/NEW');
        };

        var tariffno = $routeParams.tariffno;
        if (angular.isUndefined(tariffno)) {
            TariffService.GetList(26101).then(function (d) {
                $scope.tariffList = d.data;
            }, function (err) { });
        } else {
            if (tariffno != 'NEW') {
                TariffService.GetTariff(tariffno).then(function (d) {
                    $scope.th = d.data;
                }, function (err) { });
            } else {
                $scope.th = {
                    QuotationNo: ''
                };
            }
        }

        $scope.GetLookupData();
    }]);
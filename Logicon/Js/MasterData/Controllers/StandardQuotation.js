﻿app.controller('StandardQuotationCntrl', ['$scope', '$uibModal', 'CustomerQuotationService', 'UtilityFunc',
    function ($scope, $uibModal, CustomerQuotationService, UtilityFunc) {

        $scope.dateFormat = UtilityFunc.DateFormat();
        $scope.dateTimeFormat = UtilityFunc.DateTimeFormat();
        $scope.dateTimeFormat12 = UtilityFunc.DateTimeFormat12();

        $scope.IsfrmStandardCntrl = false;
        $scope.cq = {
            QuotationItems: new Array()
        };

        $scope.$watch('frmStandardCntrl.$valid', function (valid) {
            $scope.IsfrmStandardCntrl = valid;
        });

        $scope.AddCQDetails = function (index) {
            var modalInstance = $uibModal.open({
                animation: true,
                template: '<quotation-detail data="data" quotation="customer"></quotation-detail>',
                controller: 'QuotationDetailCntrl',
                windowClass: 'app-modal-window2',
                resolve: {
                    quotationDetailsObj: function () {
                        return (index != -1 ? $scope.cq.QuotationItems[index] : { Index: index });
                    }
                }
            });

            modalInstance.result.then(function (quotationDetails) {
                debugger;
                if ($scope.cq.QuotationItems != null) {
                    if (quotationDetails.Index != -1) {
                        $scope.cq.QuotationItems[quotationDetails.Index] = quotationDetails;
                    }
                    else {
                        $scope.cq.QuotationItems.push(quotationDetails);
                    }
                }
                else {
                    $scope.cq.QuotationItems = new Array();
                    $scope.cq.QuotationItems.push(quotationDetails);
                }
            }, function () {

            });
        };

        $scope.SaveStndQuotation = function (cq) {
            debugger;
            if ($scope.IsfrmStandardCntrl) {
                CustomerQuotationService.SaveCustomerQuotation(cq).then(function (d) {
                    debugger;
                }, function (err) { });
            }
        };

        $scope.getLookUpData = function () {
            CustomerQuotationService.GetLookupData('quotation').then(function (d) {
                $scope.lookupData = d.data;
            }, function (err) { });
        };

        $scope.getLookUpData();
    }]);


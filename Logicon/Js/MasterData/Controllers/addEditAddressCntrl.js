﻿app.controller('addEditAddressCntrl', ['$scope', '$uibModalInstance', 'CountryService', 'growl', 'addressObj', 'limitToFilter', '$http',
                               function ($scope, $uibModalInstance, CountryService, growl, addressObj, limitToFilter, $http) {
    $scope.address = addressObj;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    debugger;
    //CountryService.GetCountriesList().then(function (d) {
    //    $scope.CountriesList = d.data;
    //}, function () { });

    $scope.countryResults = function (text) {
        return CountryService.SearchCountries(text).then(function (d) {            
            return limitToFilter(d.data, 15);
        }, function (err) { });
    };

    $scope.countrySelect = function (item) {
        $scope.address.CountryCode = item.Value;
    };

    $scope.isfrmAddressValid = false;
    $scope.$watch('frmAddress.$valid', function (isValid) {
        $scope.isfrmAddressValid = isValid;
    });

    $scope.AddAddress = function (address) {
        debugger;
        if ($scope.isfrmAddressValid) {
            $uibModalInstance.close(address);
        }
        else {
            growl.error('Please enter all mandatory fields..', {});
        }
    };


}]);

//testing
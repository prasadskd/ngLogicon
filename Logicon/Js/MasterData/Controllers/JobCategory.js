﻿app.controller('JobCategoryCntrl', ['$scope', '$uibModal', 'JobCategoryService', 'growl', '$location', function ($scope, $uibModal, JobCategoryService, growl, $location) {
    $scope.currentPage = 1;
    $scope.limit = 10;

    $scope.AddJobCategory = function () {        
        $location.path('/masterdata/jobcategory/add/NEW');
    };

    $scope.JobCategoryList = function (skip, take) {
        JobCategoryService.getJobCategoryList(skip, take).then(function (d) {
            $scope.jcList = d.data.jcList;
            $scope.totalItems = d.data.totalItems;
        }, function (err) {
            growl.error(err.statusText, {});
        });
    };

    $scope.pageChanged = function () {
        var skip = $scope.currentPage == 1 ? 0 : ($scope.limit * ($scope.currentPage - 1));
        $scope.JobCategoryList(skip, $scope.limit);
    };

    $scope.DeleteJobCategory = function (code) {
        debugger;
        if (confirm('Are you sure, you want to delete \'' + code + '\' ?')) {
           
            JobCategoryService.DeleteJobCategory(code).then(function (d) {
                growl.success(d.data, {});
                $scope.getData();
            }, function (err) { });
        }
    }

    $scope.editCategory = function (categoryCode) {
        $location.path('masterdata/jobcategory/add/' + categoryCode);
    };

    $scope.JobCategoryList(0, $scope.limit);


    $scope.IsDeleted = function (jc) {       
        return jc.IsActive == false ? 'deleted' : 'ok';
    }
}]);
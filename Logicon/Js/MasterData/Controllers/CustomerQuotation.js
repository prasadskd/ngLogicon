﻿app.controller('CustomerQuotationCntrl', ['$scope', '$uibModal', 'CustomerQuotationService', 'limitToFilter', '$routeParams', '$location', 'MerchantProfileService', '$filter', 'growl', 'Utility',
    function ($scope, $uibModal, CustomerQuotationService, limitToFilter, $routeParams, $location, MerchantProfileService, $filter, growl, Utility) {
    $scope.tabs = [
       { title: 'General', content: 'Js/MasterData/Templates/CustomerQuotation/general.html?v=' + Utility.Version, active: true, disabled: false },
       { title: 'Details', content: 'Js/MasterData/Templates/CustomerQuotation/details.html?v=' + Utility.Version, active: false, disabled: false }
    ];

    $scope.cq = {
        QuotationItems: new Array()
    };

    $scope.AddCQDetails = function (index) {        
        var modalInstance = $uibModal.open({
            animation: true,
            template: '<quotation-detail data="data" quotation="customer"></quotation-detail>',
            controller: 'QuotationDetailCntrl',
            windowClass: 'app-modal-window2',
            resolve: {
                quotationDetailsObj: function () {
                    return (index != -1 ? $scope.cq.QuotationItems[index] : { Index: index });
                }
            }
        });

        modalInstance.result.then(function (quotationDetails) {            
            if ($scope.cq.QuotationItems != null) {                
                if (quotationDetails.Index != -1) {                    
                    $scope.cq.QuotationItems[quotationDetails.Index] = quotationDetails;
                }
                else {
                    $scope.cq.QuotationItems.push(quotationDetails);
                }
            }
            else {
                $scope.cq.QuotationItems = new Array();
                $scope.cq.QuotationItems.push(quotationDetails);
            }
        }, function () {

        });
    };
    
    $scope.isFrmCustomerQuotationValid = false;
    $scope.$watch('frmCustomerQuotation.$valid', function (valid) {
        $scope.isFrmCustomerQuotationValid = valid;
    });

    $scope.SaveCustomerQuotation = function (cq) {        
        if ($scope.isFrmCustomerQuotationValid) {
            CustomerQuotationService.SaveCustomerQuotation(cq).then(function (d) {
                growl.success(d.data, {});
                $location.path('/MasterData/customerquotation/list');
            }, function (err) { });
        } else {
            growl.error('Please enter all mandatory fields..', {});
        }
    };

    $scope.lookupData = {};
    $scope.getLookUpData = function () {
        CustomerQuotationService.GetLookupData('quotation').then(function (d) {
            $scope.lookupData = d.data;
        }, function (err) { });
    };    

    $scope.getLookUpData();    

    $scope.CustomerResults = function (text, filter) {
        return MerchantProfileService.SearchMerchantResults(text, filter).then(function (response) {
            return limitToFilter(response.data, 15);
        }, function (err) { });
    };

    $scope.AgentResults = function (text, filter) {
        return MerchantProfileService.SearchMerchantResults(text, filter).then(function (response) {
            return limitToFilter(response.data, 15);
        }, function (err) { });
    };

    $scope.ForwarderResults = function (text, filter) {
        return MerchantProfileService.SearchMerchantResults(text, filter).then(function (response) {
            return limitToFilter(response.data, 15);
        }, function (err) { });
    };

    $scope.CustomerQuotationList = function () {
        CustomerQuotationService.getCustomerQuotationListByType(3861).then(function (d) {
            $scope.quotationlist = d.data;            
        }, function () { });
    };

    var quotationNo = $routeParams.quotation;    
    if (typeof quotationNo != 'undefined' && quotationNo != 'NEW') {
        CustomerQuotationService.getCustomerQuotation(quotationNo).then(function (d) {
            $scope.cq = d.data;

            if ($scope.cq.QuotationDate != null) {
                $scope.cq.QuotationDate = new Date(moment($scope.cq.QuotationDate));
            }

            if ($scope.cq.EffectiveDate != null)
                $scope.cq.EffectiveDate = new Date(moment($scope.cq.EffectiveDate));

            if ($scope.cq.ExpiryDate != null)
                $scope.cq.ExpiryDate = new Date(moment($scope.cq.ExpiryDate));
        }, function (err) { });
    }
    else {
        $scope.CustomerQuotationList();
    }    

    $scope.backToList = function () {
        $location.path('/MasterData/customerquotation/list');
    };
    
    $scope.searchClick = function (type) {
        $routeParams.code = $scope.cq[type];
    };

    $scope.AddQuotation = function (quotationNo)
    {
        $location.path('/MasterData/customerquotation/' +quotationNo);
    }


    $scope.DeleteQuotation = function (quotationNo) {
        CustomerQuotationService.DeleteQuotation(quotationNo).then(function (d) {
            debugger;
            growl.success(d.data, {});
            $scope.CustomerQuotationList();
        }, function (err) { });
    };
}]);




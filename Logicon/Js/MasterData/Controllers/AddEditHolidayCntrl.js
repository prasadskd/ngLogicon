﻿app.controller('AddEditHolidayCntrl', ['$scope', '$uibModalInstance', 'holiday', 'HolidayService', 'growl', function ($scope, $uibModalInstance, holiday, HolidayService, growl) {

    $scope.cancel = function () {       
        $uibModalInstance.dismiss('cancel');
        $scope.isfrmAddHoliday = false;
    };
   
    $scope.isfrmAddHoliday = false;
    $scope.$watch('frmAddHoliday.$valid', function (isValid) {        
        $scope.isfrmAddHoliday = isValid;
    });

    $scope.AddHoliday = function (holiday) {       
        if ($scope.isfrmAddHoliday) {
            HolidayService.SaveHoliday(holiday).then(function (d) {
                $uibModalInstance.close();
                growl.success(d.data, {});

            }, function (err) { growl.error(err.statusText, {}); });
        }
        else {
            growl.error('Please enter all mandatory fields..', {});
        }
    };

  

    HolidayService.GetCountryList().then(function (d) {
        $scope.countryDataList = d.data;       
    }, function (err) {
        growl.error(err.statusText, {});
    });

    
    if (holiday != -1) {
        if ($scope.isfrmAddHoliday) {
            HolidayService.GetHolidayByDate(holiday).then(function (d) {
                $scope.holidayList = d.data;
            }, function (err) { growl.error(err.statusText, {}); });
        }
        else {
            growl.error('Please enter all mandatory fields..', {});
        }
    }

}]);
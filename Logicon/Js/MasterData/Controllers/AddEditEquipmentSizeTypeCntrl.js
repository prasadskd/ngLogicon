﻿app.controller('AddEditEquipmentSizeTypeCntrl', ['$scope', '$uibModalInstance', 'stCode', 'EquipmentSizeTypeService', 'growl', function ($scope, $uibModalInstance, stCode, EquipmentSizeTypeService, growl) {
    $scope.truefalse = false;
    $scope.cancel = function () {
        
        $uibModalInstance.dismiss('cancel');
        $scope.isfrmAddEquipmentSizeType = false;
    };
   
    $scope.isfrmAddEquipmentSizeType = false;
    $scope.$watch('frmAddEquipmentSizeType.$valid', function (isValid) {
        $scope.isfrmAddEquipmentSizeType = isValid;
    });

    $scope.AddEquipmentSizeType = function (equipmentsizetype) {
       
        if ($scope.isfrmAddEquipmentSizeType) {
            EquipmentSizeTypeService.SaveEquipmentSizeType(equipmentsizetype).then(function (d) {
                $uibModalInstance.close();
                growl.success(d.data, {});

            }, function (err) { growl.error(err.statusText, {}); });
        }
        else {
            growl.error('Please enter all mandatory fields..', {});
        }
    };

    EquipmentSizeTypeService.GetEquipmentType().then(function (d) {       
        $scope.equipmenttypeList = d.data;
    });

  
    
    if (stCode != -1) {
              
        EquipmentSizeTypeService.GetEquipmentSizeTypeByCode(stCode).then(function (d) {           
            $scope.equipmentsizetype = d.data;
            $scope.truefalse = true;
            }, function (err) { growl.error(err.statusText, {}); });       
        }
    

}]);
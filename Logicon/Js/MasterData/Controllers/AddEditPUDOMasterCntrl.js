﻿app.controller('AddEditPUDOMasterCntrl', ['$scope', '$uibModalInstance', 'pudomaster', 'PUDOMasterService', 'growl', function ($scope, $uibModalInstance, pudomaster, PUDOMasterService, growl) {
   
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $scope.isfrmAddPUDOMaster = false;
    };

    $scope.isfrmAddPUDOMaster = false;
    $scope.$watch('frmAddPUDOMaster.$valid', function (isValid) {
        $scope.isfrmAddPUDOMaster = isValid;
    });


    $scope.GetPUDOCodes = function (BookingTypeId) {
       
        var bookingTypeDesc = $.grep($scope.bookingtypeList, function (BookingType) {
           
            return BookingType.Value == BookingTypeId;
        })[0].Text;
       
        PUDOMasterService.GetPUDOModeList(bookingTypeDesc).then(function (d) {            
            $scope.pudomodeList = d.data;
        });
    };

    $scope.GetMovementCodes = function (orderType) {
        debugger;      
        PUDOMasterService.GetMovementCodeList(orderType).then(function (d) {
            $scope.movementcodeList = d.data;
        });
    };
    
    $scope.AddPUDOMaster = function (pudomaster) {

        if ($scope.isfrmAddPUDOMaster) {
          
            PUDOMasterService.SavePUDOMaster(pudomaster).then(function (d) {
                $uibModalInstance.close();
                growl.success(d.data, {});

            }, function (err) { growl.error(err.statusText, {}); });
        }
        else {
            growl.error('Please enter all mandatory fields..', {});
        }
    };

    if (pudomaster != -1) {
        debugger;
        PUDOMasterService.GetPUDOMaster(pudomaster).then(function (d) {
            debugger;
            $scope.pudomodeList = $scope.GetPUDOCodes(pudomaster.JobType);
            $scope.movementcodeList = $scope.GetMovementCodes(pudomaster.OrderType);
            $scope.pudomaster = d.data;
        }, function (err) { growl.error(err.statusText, {}); });

    }

    PUDOMasterService.GetBookingTypeList().then(function (d) {
        $scope.bookingtypeList = d.data;
    });

    //PUDOMasterService.GetPUDOModeList().then(function (d) {
    //    debugger;
    //    $scope.pudomodeList = d.data;
    //});

    PUDOMasterService.GetOrderTypeList().then(function (d) {
        
        $scope.ordertypeList = d.data;
    });


    //$scope.ordertypeList = [
    //{ Value: "2001", Text: "1" },
    // { Value: "2002", Text: "2" },
    //];

    //$scope.movementcodeList = [
    //{ Value: "9001", Text: "1" },
    // { Value: "9002", Text: "2" },
    //];

    //PUDOMasterService.GetOrderTypeList().then(function (d) {
    //    debugger;
    //    $scope.ordertypeList = d.data;
    //});

    //PUDOMasterService.GetMovementCodeList().then(function (d) {
    //    $scope.movementcodeList = d.data;
    //});

/*
    if (pudomaster != -1) {
        PUDOMasterService.GetPUDOMaster(pudomaster).then(function (d) {
            $scope.pudomaster = d.data;
        }, function (err) { growl.error(err.statusText, {}); });

    }*/

}]);
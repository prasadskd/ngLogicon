﻿app.controller('EditIMOCodeCntrl', ['$scope', '$uibModalInstance', 'imoCode', 'IMOCodeService', 'growl',
    function ($scope, $uibModalInstance, imoCode, IMOCodeService, growl) {

        $scope.isFrmIMOCodeValid = false;
        $scope.$watch('frmIMOCode.$valid', function (valid) {
            $scope.isFrmIMOCodeValid = valid;
        });

        if (imoCode != 'NEW') {
            IMOCodeService.GetIMOCode(imoCode).then(function (d) {
                $scope.imo = d.data;
            }, function (err) { });
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.SaveIMOCode = function (imo) {
            if ($scope.isFrmIMOCodeValid) {
                IMOCodeService.SaveIMOCode(imo).then(function (d) {
                    $uibModalInstance.close();
                }, function (err) { });
            }
        };
}]);

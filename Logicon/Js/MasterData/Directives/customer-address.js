﻿app.directive('customerAddress', function () {
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            code: '=merchant'
        },
        templateUrl: 'Js/MasterData/Directives/logicon-address.html',
        controller: ['$scope', '$q', 'AddressService','$routeParams',
            function ($scope, $q, AddressService, $routeParams) {
                var code = $routeParams.code;                
                AddressService.GetAddress(code).then(function (d) {
                    $scope.address = d.data;
                }, function (err) { });
            }]
    };
});
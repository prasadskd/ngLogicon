﻿app.directive('quotationDetail', [function () {
    debugger;
    return {
        restrict: 'E',
        replace: true,
        scope: {
            details: '=data'
        },
        templateUrl: 'Js/MasterData/Directives/quotation-detail.html',
        controller: ['$scope', 'CustomerQuotationService', 'ChargeCodeService', 'OrderEntryService', 'limitToFilter','$q',
            function ($scope, CustomerQuotationService, ChargeCodeService, OrderEntryService, limitToFilter, $q) {
            $scope.isfrmQuotationDetailsValid = false;
            $scope.$watch('frmQuotationDetails.$valid', function (isValid) {
                $scope.isfrmQuotationDetailsValid = isValid;
            });            

            $scope.lookupData = {};
            $scope.getLookUpData = function () {
                
                var lookupPromise = CustomerQuotationService.GetLookupData('quotationdetail');
                if ($scope.details.Size == null)
                    $scope.details.Size = "20";
                var sizeTypePromise = OrderEntryService.GetSizeType($scope.details.Size);
               
                $q.all([lookupPromise, sizeTypePromise]).then(function (d) {                    
                    $scope.lookupData = d[0].data;
                    $scope.lookupData.TypeList = d[1].data;
                }, function (err) { })
            };

            $scope.ChargeCodeResults = function (text) {
                return ChargeCodeService.GetChargeCodeSearch(text).then(function (response) {
                    return limitToFilter(response.data, 15);
                }, function (err) { });
            };
            
            $scope.SaveQuotationDetail = function (details) {
                if ($scope.isfrmQuotationDetailsValid) {
                    $scope.$parent.addDetails(details);
                } else {

                }
            };

            $scope.cancel = function () {
                $scope.$parent.cancel();
            };

            $scope.sizeChanged = function () {
                OrderEntryService.GetSizeType($scope.details.Size).then(function (d) {
                    debugger;
                    $scope.lookupData.TypeList = d.data;
                }, function () { })
            };

            $scope.getLookUpData();
        }],
        link: function ($scope, element, attrs) {            
            //$scope.isextra = attrs.quotation == 'standard' ? true : false;
        }
    };
}]);

/*
app.directive('quotationDetail', function ($uibModalInstance) {
    debugger;
    return {
        restrict: 'E',
        replace: true,
        scope: {
            
        },
        templateUrl: 'Js/Tms/Directives/quotation-detail.html',
        controller: ['$scope', 'quotationDetailsObj', function ($scope, quotationDetailsObj) {
            debugger;
        }],
        link: function ($scope, element, attrs) {
            debugger;
        }
    };
});
*/

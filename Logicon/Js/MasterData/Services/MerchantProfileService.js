﻿app.service('MerchantProfileService', ['$http', '$q', 'Utility', 'limitToFilter', function ($http, $q, Utility, limitToFilter) {
    this.GetLookupData = function () {
        var deferred = $q.defer();
        $http.get(Utility.ServiceUrl + '/master/MerchantProfile/lookup').then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.GetMerchantProfileList = function (skip, limit, filter) {
        var deferred = $q.defer();
        $http.get(Utility.ServiceUrl + '/master/MerchantProfile/list/' + skip + '/' + limit + '/' + filter).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.GetMerchantProfile = function (obj) {      
        var deferred = $q.defer();
        $http.post(Utility.ServiceUrl + '/master/MerchantProfile/details', JSON.stringify(obj)).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.SaveMerchantProfile = function (obj) {       
        var deferred = $q.defer();
        $http.post(Utility.ServiceUrl + '/master/MerchantProfile/save', JSON.stringify(obj)).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.GetMerchantResults = function (text, filter) {
        var deferred = $q.defer();
        $http.get(Utility.ServiceUrl + '/master/MerchantProfile/' + code).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.SearchMerchantResults = function (text, filter) {
        var deferred = $q.defer();
        $http.get(Utility.ServiceUrl + '/Master/MerchantProfile/search/' + text + '/' + filter).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };



    this.DeleteMerchant = function (code) {        
        var deferred = $q.defer();
        $http.delete(Utility.ServiceUrl + '/master/MerchantProfile/' + code).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };


   
    /*
    return $http.get(Utility.ServiceUrl + '/master/MerchantProfile/search/' + text + '/' + filter).then(function (response) {
        return limitToFilter(response.data, 15);
    });
    */
}]);
﻿app.service('HSCodeService', ['$http', '$q', 'Utility', function ($http, $q, Utility) {
    this.Search = function (text) {
        var deferred = $q.defer();
        $http.get(Utility.ServiceUrl + '/master/hscode/search/' + text).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    this.AutoComplete = function (text) {
        var deferred = $q.defer();
        $http.get(Utility.ServiceUrl + '/master/hscode/autocomplete/' + text).then(function (res) {
            deferred.resolve(res);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    };
}]);
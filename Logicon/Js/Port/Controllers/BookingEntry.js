﻿app.controller('BookingEntryCntrl', ['$scope', '$uibModal', 'BookingEntryService', 'MerchantProfileService', 'JobCategoryChargesService', 'VesselMasterService', 'VesselScheduleService', 'limitToFilter', '$routeParams', 'growl', '$filter', 'UtilityFunc', 'Utility', 'PortAreaService', '$route', '$location', '$window',
    function ($scope, $uibModal, BookingEntryService, MerchantProfileService, JobCategoryChargesService, VesselMasterService, VesselScheduleService, limitToFilter, $routeParams, growl, $filter, UtilityFunc, Utility, PortAreaService, $route, $location, $window) {

        var defaultCurrency = UtilityFunc.DefaultCurrency();
        $scope.dateFormat = UtilityFunc.DateFormat();
        $scope.dateTimeFormat = UtilityFunc.DateTimeFormat();
        $scope.dateTimeFormat12 = UtilityFunc.DateTimeFormat12();

        $scope.currentPage = 1;
        $scope.limit = 10;
        $scope.showLoading = true;
        $scope.dtmax = new Date();
        $scope.bookingHeader = {
            BookingContainers: new Array(),
            TransportType: 1021,
            OrderDate: moment()
        };

        $scope.tabs = [
            { title: 'General Info', content: 'Js/Port/Templates/BookingEntry/generalinfo.html?v=' + Utility.Version, active: true, disabled: false, tooltip: '' },
            { title: 'Container Details', content: 'Js/Port/Templates/BookingEntry/containerdetails.html?v=' + Utility.Version, active: false, disabled: true, tooltip: 'Please select booking type to enable the tab' }
        ];
        $scope.IsfrmBookingEntryValid = false;
        $scope.bookingHeader.BookingMovements = new Array();
        $scope.ChargeVasList = new Array();

        $scope.$watch('frmBookingEntry.$valid', function (valid) {
            $scope.IsfrmBookingEntryValid = valid;
        });

        $scope.back = function () {
            $location.path('/Port/bookingentrylist');
        };

        $scope.clear = function () {
            $route.reload('/port/bookingentry/NEW');
        };

        $scope.AddContainerDetails = function (index) {
            var t = (index == -1 ? $scope.bookingHeader.BookingContainers[index] : { index: -1 });
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'Js/Port/Templates/BookingEntry/addcontainerdetails.html?v=' + Utility.Version,
                controller: 'BookingContainerInfoCntrl',
                size: 'lg',
                resolve: {
                    dataObj: function () {
                        return {
                            lookUpData: $scope.lookUpData,
                            bookingContainer: (index != -1 ? $scope.bookingHeader.BookingContainers[index] : { index: -1 })
                        }
                    }
                }
            });
            modalInstance.result.then(function (bc) {
                var ContainerKey = '';
                if (bc.index == -1) {
                    bc.ContainerKey = GetPaddingNo($scope.bookingHeader.BookingContainers.length + 1);
                    if (typeof ($scope.bookingHeader.BookingContainers) != 'undefined') {
                        bc.index = $scope.bookingHeader.BookingContainers.length;
                        $scope.bookingHeader.BookingContainers.push(bc);
                    }
                    else {
                        $scope.bookingHeader.BookingContainers = new Array();
                        bc.index = 0;
                        $scope.bookingHeader.BookingContainers.push(bc);
                    }

                    $scope.tblSearch = { ContainerKey: bc.ContainerKey };

                    JobCategoryChargesService.MovementsByCategoryCode($scope.bookingHeader.OrderType, bc.ContainerKey).then(function (d) {
                        if ($scope.bookingHeader.BookingMovements.length > 0)

                            $scope.bookingHeader.BookingMovements = $scope.bookingHeader.BookingMovements.concat(d.data);
                        else
                            $scope.bookingHeader.BookingMovements = d.data;
                    }, function (err) { });
                }
                else {
                    $scope.bookingHeader.BookingContainers[bc.index] = bc;
                    JobCategoryChargesService.MovementsByCategoryCode($scope.bookingHeader.OrderType, bc.ContainerKey).then(function (d) {
                        if ($scope.bookingHeader.BookingMovements.length > 0) {

                            $scope.bookingHeader.BookingMovements = UtilityFunc.removeArrayElementByKey($scope.bookingHeader.BookingMovements, 'ContainerKey', bc.ContainerKey);
                            $scope.bookingHeader.BookingMovements = $scope.bookingHeader.BookingMovements.concat(d.data);
                        }
                        else
                            $scope.bookingHeader.BookingMovements = d.data;
                    }, function (err) { });
                }

            }, function (err) {
            });
        };

        function GetPaddingNo(num) {
            var str = "" + num
            var pad = "000"
            var result = pad.substring(0, pad.length - str.length) + str

            return result;
        }

        $scope.lookUp = function () {
            BookingEntryService.getLookupData().then(function (d) {
                $scope.lookUpData = d.data;
                $scope.showLoading = false;
            }, function (err) { });
        };

        $scope.bookingTypeChange = function () {
            debugger;
            BookingEntryService.getOrderTypeData($scope.bookingHeader.BookingType)
                .then(function (d) {
                    $scope.lookUpData.orderTypeList = d.data.orderTypeList;
                    $scope.lookUpData.puModeList = d.data.puModeList;
                    $scope.tabs[1].disabled = false;
                    $scope.tabs[1].tooltip = '';
                }, function (err) { });
        };

        $scope.GenericMerchantResults = function (text, filter) {
            return MerchantProfileService.SearchMerchantResults(text, filter).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.portResults = function (text) {
            return PortAreaService.PortAutoComplete(text).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.PortSelected = function (item, type) {
            debugger;
            $scope.bookingHeader[type] = item.PortCode;
        };

        $scope.VesselCodeLoadingResults = function ($query) {
            debugger;
            return VesselMasterService.GetVesselByVesselName($query).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.VoyageSearchSelect = function (item) {
            debugger;
            $scope.bookingHeader.VesselScheduleID = item.VesselScheduleID;
            $scope.bookingHeader.PortCutOffDry = item.ClosingDate;
            $scope.bookingHeader.PortCutOffReefer = item.ClosingDateRF;
            $scope.bookingHeader.ETA = item.ETA;
            $scope.bookingHeader.ETD = item.ETD;
            $scope.bookingHeader.ShipCallNo = item.ShipCallNo;
            $scope.bookingHeader.CallSignNo = item.CallSignNo;
            if ($scope.bookingHeader.BookingType == 1061) {
                $scope.bookingHeader.VoyageNo = item.VoyageNoOutWard;
            }
            else {
                $scope.bookingHeader.VoyageNo = item.VoyageNoInWard;
            }
            //console.log(JSON.stringify(item));
        };

        $scope.VoyageLoadingResults = function ($query) {
            var obj = {
                VesselID: $scope.bookingHeader.VesselID,
                AgentCode: $scope.bookingHeader.AgentCode,
                JobType: $scope.bookingHeader.BookingType,
                VoyageNo: $query
            }

            return VesselScheduleService.VoyageSearchByObj(obj).then(function (d) {

                return limitToFilter(d.data, 15);
            }, function (err) {
                // alert(err.statusText);
            });
        };

        $scope.CustomerSelected = function (item, type) {
            $scope.bookingHeader[type] = item.Value;
        };

        $scope.PortSelected = function (item, type) {
            $scope.bookingHeader[type] = item.PortCode;
        };

        $scope.ValidatePorts = function () {
            if ($scope.bookingHeader.TransportType == 1021) {
                if ($scope.bookingHeader.LoadingPort == $scope.bookingHeader.DischargePort) {
                    growl.error('Loading Port and Discharge Port can not be the same', {});
                    return false;
                }
                else if ($scope.bookingHeader.LoadingPort == $scope.bookingHeader.DestinationPort) {
                    growl.error('Loading Port and Destination Port can not be the same', {});
                    return false;
                }
                else
                    return true;
            }
            else
                return true;
        };

        $scope.SaveBookingEntry = function (bookingHeader) {
            if ($scope.IsfrmBookingEntryValid) {
                //$scope.showLoading = true;
                if ($scope.ValidatePorts()) {
                    BookingEntryService.SaveBookingHeader(bookingHeader).then(function (d) {
                        //$scope.showLoading = false;
                        growl.success('Booking Entry Saved Successfully', {});
                        $route.reload('Port/bookingentry/NEW');
                        debugger;
                    }, function (err) { })
                }
            }
            else {
                growl.error('Please enter all mandatory fields', {});
            }
        };

        $scope.DeleteBookingEntry = function () {
            debugger;
            if ($window.confirm('Are you sure, you want to delete \'' + $scope.bookingHeader.OrderNo + '\' ?')) {
                BookingEntryService.deleteBookingEntry($scope.bookingHeader.OrderNo).then(function (d) {
                    if (d.data) {
                        debugger;
                        growl.success('Booking Entry Deleted Successfully', {});
                        $location.path('/Port/bookingentrylist');
                    }
                });
            }
        };

        $scope.getData = function (skip, take) {
            BookingEntryService.getBookingHeaderList(skip, take).then(function (d) {
                $scope.bookingHeaderList = d.data.bookingHeaderList;
                $scope.totalItems = d.data.totalItems;
            }, function (err) { growl.error(err.statusText, {}); });
        };

        $scope.pageChanged = function () {
            var skip = $scope.currentPage == 1 ? 0 : ($scope.limit * ($scope.currentPage - 1));
            $scope.getData(skip, $scope.limit);
        };

        $scope.PortResults = function ($query) {
            return BookingEntryService.SearchPorts($query).then(function (d) {
                return limitToFilter(d.data, 15);
            }, function (err) { });
        };

        $scope.OnBlur = function (name, id) {
            if (typeof $scope.bookingHeader[name] == 'undefined' || $scope.bookingHeader[name] == null || $scope.bookingHeader[name] == '') {
                $scope.bookingHeader[id] = null;
            }
        }

        $scope.containerDetailRowClick = function (containerKey) {
            debugger;
            $scope.tblSearch = { ContainerKey: containerKey };
        };

        $scope.transportTypeChanged = function () {
            // $scope.bookingHeader.TransportType=== 1022;
        };

        var orderno = $routeParams.orderNo;
        $scope.IsNew = false;
        if (typeof orderno != 'undefined') {
            $scope.lookUp();
            if (orderno != 'NEW') {
                BookingEntryService.getBookingHeader(orderno).then(function (d) {
                    debugger;
                    $scope.bookingHeader = d.data;
                    if (!angular.isUndefined($scope.bookingHeader.BookingContainers) && $scope.bookingHeader.BookingContainers != null && $scope.bookingHeader.BookingContainers.length > 0) {
                        $scope.containerDetailRowClick($scope.bookingHeader.BookingContainers[0].ContainerKey);
                    }

                    if ($scope.bookingHeader.BookingType != null) {
                        $scope.bookingTypeChange();
                    }
                }, function (err) { });
            }
            else {
                $scope.IsNew = true;
            }
        }
        else {
            $scope.IsNew = true;
            $scope.getData(0, $scope.limit);
        }
    }]);
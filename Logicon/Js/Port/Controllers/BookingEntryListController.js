﻿app.controller('BookingEntryListCntrl', ['$scope', 'BookingEntryListService', '$q', '$location', '$routeParams', 'UtilityFunc',
function ($scope, BookingEntryListService, $q, $location, $routeParams, UtilityFunc) {
    $scope.currentPage = 1;
    $scope.limit = 10;
    $scope.Search = {};
    $scope.dateFormat = UtilityFunc.DateFormat();
    $scope.dateTimeFormat = UtilityFunc.DateTimeFormat();
    $scope.dataGridNorecords = UtilityFunc.DataGridNorecords();

    $scope.Search.DateFrom = UtilityFunc.FirstDateOfMonth(); //new Date(d.getFullYear(), d.getMonth(), 1);
    $scope.Search.DateTo = moment();

    $scope.GetBookingEntries = function (obj) {
        debugger;
        BookingEntryListService.SearchBookingEntries(obj).then(function (d) {
            $scope.bookingentries = d.data.bookingentries;
            $scope.totalItems = d.data.recordsCount;
        }, function (err) { });

    };

    $scope.isFrmSearchValid = true;
    //$scope.$watch('frmSearch.$valid', function (isValid) {
    //    $scope.isFrmSearchValid = isValid;
    //    debugger;
    //});

    $scope.SearchBookingEntries = function () {
        var obj = {
            branchID: $routeParams.branchID == undefined ? null : $routeParams.branchID,
            orderNo: $scope.Search.OrderNo == undefined ? null : $scope.Search.OrderNo,
            VoyageNo: $scope.Search.VoyageNo == undefined ? null : $scope.Search.VoyageNo,
            DateFrom: $scope.Search.DateFrom == undefined ? null : $scope.Search.DateFrom,
            DateTo: $scope.Search.DateTo == undefined ? null : $scope.Search.DateTo,
            bookingNo: $scope.Search.BookingNo == undefined ? null : $scope.Search.BookingNo,
            CustomerName: $scope.Search.CustomerName == undefined ? null : $scope.Search.CustomerName,
            vesselName: $scope.Search.VesselName == undefined ? null : $scope.Search.VesselName,
            IsDefault: false,
            Skip: ($scope.currentPage == 1 ? 0 : ($scope.limit * ($scope.currentPage - 1))),
            Limit: $scope.limit
        };
        debugger;
        if ($scope.isFrmSearchValid) {
            BookingEntryListService.SearchBookingEntries(obj).then(function (d) {
                $scope.bookingentries = d.data.bookingentries;
                $scope.totalItems = d.data.recordsCount;
            }, function (err) { });
        }
    };

    $scope.pageChanged = function () {
        $scope.SearchBookingEntries();
    };
    $scope.GetBookingEntries({
        branchID: $routeParams.branchID == undefined ? null : $routeParams.branchID,
        orderNo: null,
        VoyageNo: null,
        DateFrom: null,
        DateTo: null,
        bookingNo: null,
        CustomerName: null,
        vesselName: null,
        IsDefault: true,
        Skip: 0,
        Limit: 0
    });
    $scope.viewInfo = function (orderno) {
        $location.path('/Port/bookingentry/' + orderno);
    };

    $scope.viewInfo2 = function (orderno) {        
        $location.path('/Port/bookingentry/' + orderno);
    };
}]);
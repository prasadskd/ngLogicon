﻿app.controller('BookingContainerInfoCntrl', ['$scope', '$uibModalInstance', 'dataObj', 'OrderEntryService', 'growl', 'UtilityFunc',
    function ($scope, $uibModalInstance, dataObj, OrderEntryService, growl, UtilityFunc) {

        $scope.dateFormat = UtilityFunc.DateFormat();
        $scope.dateTimeFormat = UtilityFunc.DateTimeFormat();
        $scope.dateTimeFormat12 = UtilityFunc.DateTimeFormat12();

        debugger;

        $scope.bc = dataObj.bookingContainer;
        $scope.lookUpData = dataObj.lookUpData;
        $scope.isFrmBookingConInfoValid = false;
        if ($scope.bc.index == -1)
            $scope.bc.Status = 1041;
        $scope.bc.PickupDate = moment();
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.sizeChanged = function () {
            OrderEntryService.GetSizeType($scope.bc.Size).then(function (d) {
                $scope.lookUpData.TypeList = d.data;
            }, function () { })
        };

        if ($scope.bc.Size != null) {
            $scope.sizeChanged();

        }

        $scope.isFrmBookingConInfoValid = false;
        $scope.$watch('frmBookingConInfo.$valid', function (valid) {
            $scope.isFrmBookingConInfoValid = valid;
        });

        $scope.SaveBookingContainer = function (bc) {
            if ($scope.isFrmBookingConInfoValid) {
                $uibModalInstance.close(bc);
            }
            else {
                growl.error('Please enter all mandatory fields', {});
            }
        };
    }]);
﻿app.controller('EmailVerificationCntrl', ['$scope', '$routeParams', '$location', 'VerificationService', 'growl', '$timeout', function ($scope, $routeParams, $location, VerificationService, growl, $timeout) {
    $scope.showAnimation = false;
    $scope.Failed = false;
    $scope.showLoading = true;
    if (angular.isUndefined($routeParams.uniqueid)) {
        $scope.showLoading = false;
        growl.warning('This link is expired.', {});

        $timeout(function () {
            //$location.path('/login')
            location.href = 'default.html';
        }, 10000);
    } else {
        var uniqueid = $routeParams.uniqueid;
        VerificationService.EmailVerification(uniqueid).then(function (d) {
            $scope.showLoading = false;
            $scope.showAnimation = true;
            growl.success('Your email has verified.', {});

            $timeout(function () {
                //$location.path('/login')
                location.href = 'default.html';
            }, 10000);
        }, function (err) {
            $scope.showLoading = false;
            $scope.Failed = true;
            growl.error(err.data.ExceptionMessage, {});
        });        
    }
}]);
﻿app.controller('ContainerRequestCntrl', ['$scope', '$uibModal', 'Utility', function ($scope, $uibModal, Utility) {

    $scope.AddContainerRequest = function () {
        $scope.modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'Js/Freight/Templates/ContainerRequest/add-container-request.html?v=' + Utility.Version,
            //size: 'lg',
            windowClass: 'app-modal-window'
        });

        $scope.modalInstance.result.then(function () {

        }, function () {

        });
    };
}]);